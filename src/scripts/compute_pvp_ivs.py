import itertools
import time

from sql import session_scope
from sql.table import PVPStats, Pokedex
from util.combat import combat, ads, stat_product, atk_product


def insert_stats_dict(league, max_level, metric, max_rank):
    if league == "great":
        cp_target = 1500
    elif league == "ultra":
        cp_target = 2500
    elif league == "little":
        cp_target = 500
    else:
        return

    dex_list = list(filter(lambda x: x.form not in ("Normal", "Shadow", "Purified") and x.base_atk is not None,
                           set(Pokedex.values())))
    # dex_list = list(filter(lambda x: x.dex_no != 201 or x.form == "", dex_list))
    dex_list.sort(key=lambda x: (x.dex_no, x.form))

    return_rows = []
    for dex in dex_list:
        level = 1
        while level <= max_level and combat(dex, level, 15, 15, 15) <= cp_target:
            level += 0.5
        lower_level_start = level - 0.5

        level = 1
        while level <= max_level and combat(dex, level, 0, 0, 0) <= cp_target:
            level += 0.5
        upper_level_start = level - 0.5

        if upper_level_start - lower_level_start <= 0.5:
            level_start = lower_level_start
        else:
            level_start = int((upper_level_start + lower_level_start) / 2)
        assert lower_level_start <= level_start <= upper_level_start

        stats = []
        iv_range = range(16) if lower_level_start < max_level else range(max(16 - max_rank, 0), 16)
        for aiv, div, siv in itertools.product(iv_range, repeat=3):
            lower_level, upper_level, level = lower_level_start, upper_level_start, level_start
            while True:
                # todo: possibly reorder
                if combat(dex, level, aiv, div, siv) <= cp_target:
                    lower_level = level
                else:
                    upper_level = level - 0.5
                if lower_level == upper_level:
                    break
                else:
                    level = int(lower_level + upper_level + 1) / 2
            stats.append([upper_level, aiv, div, siv])

        insertions = []
        stats.sort(key=lambda x: metric(dex, *x), reverse=True)
        r = 1
        for i in range(len(stats)):
            if i == 0:
                pass
            elif ads(dex, *stats[i - 1]) != ads(dex, *stats[i]):
                r += 1
            if r > max_rank:
                break
            insertions.append(dict(dex_no=dex.dex_no, form=dex.form, atk_iv=stats[i][1], def_iv=stats[i][2],
                                   sta_iv=stats[i][3], target_dex_no=dex.dex_no, target_dex_form=dex.form,
                                   max_level=max_level, metric=metric.__name__, rank=r,
                                   little_league=stats[i][0] if league.lower() == "little" else 0,
                                   great_league=stats[i][0] if league.lower() == "great" else 0,
                                   ultra_league=stats[i][0] if league.lower() == "ultra" else 0))

        more_insertions = []
        for insert in insertions:
            if prior_dex := dex.prior_dex:
                more_insertions.append(dict(dex_no=prior_dex.dex_no, form=prior_dex.form, atk_iv=insert["atk_iv"],
                                            def_iv=insert["def_iv"], sta_iv=insert["sta_iv"],
                                            target_dex_no=dex.dex_no, target_dex_form=dex.form,
                                            max_level=insert["max_level"], metric=insert["metric"], rank=insert["rank"],
                                            little_league=insert["little_league"],
                                            great_league=insert["great_league"],
                                            ultra_league=insert["ultra_league"]))

                if prior_dex := prior_dex.prior_dex:
                    more_insertions.append(dict(dex_no=prior_dex.dex_no, form=prior_dex.form,
                                                atk_iv=insert["atk_iv"], def_iv=insert["def_iv"],
                                                sta_iv=insert["sta_iv"], target_dex_no=dex.dex_no,
                                                target_dex_form=dex.form, max_level=insert["max_level"],
                                                metric=insert["metric"], rank=insert["rank"],
                                                little_league=insert["little_league"],
                                                great_league=insert["great_league"],
                                                ultra_league=insert["ultra_league"]))

                    if prior_dex := prior_dex.prior_dex:
                        more_insertions.append(dict(dex_no=prior_dex.dex_no, form=prior_dex.form,
                                                    atk_iv=insert["atk_iv"], def_iv=insert["def_iv"],
                                                    sta_iv=insert["sta_iv"], target_dex_no=dex.dex_no,
                                                    target_dex_form=dex.form, max_level=insert["max_level"],
                                                    metric=insert["metric"], rank=insert["rank"],
                                                    little_league=insert["little_league"],
                                                    great_league=insert["great_league"],
                                                    ultra_league=insert["ultra_league"]))

        # print(league, max_level, metric.__name__, dex)
        return_rows += insertions + more_insertions
    return return_rows


def main():
    max_rank = 5

    with session_scope() as s:
        s.query(PVPStats).delete()

    for league in ("great", "ultra", "little"):
        for metric in (stat_product, atk_product):
            for max_level in (41, 51):
                t0 = time.monotonic()
                insertions = insert_stats_dict(league, max_level, metric, max_rank)
                print(league, max_level, metric.__name__, time.monotonic() - t0, len(insertions))
                with session_scope() as sql:
                    sql.bulk_insert_mappings(PVPStats, insertions)


if __name__ == "__main__":
    main()
