from typing import Optional

import sqlalchemy
import sqlalchemy.ext.declarative as decl

from sql import session_scope
from util import split_once_at


class CachedMeta(decl.DeclarativeMeta):

    additional_keys = ()

    def __init__(cls, classname, bases, dict_):
        cls._cache = None
        super().__init__(classname, bases, dict_)

    @property
    def cache(self):
        if self._cache is None:
            self._build_cache()
        return self._cache

    def _build_cache(self):
        self._cache = {}
        pk = tuple(getattr(c, "name") for c in self.__table__.primary_key)
        if len(pk) == 1:
            with session_scope(expire_on_commit=False) as _sql:
                for r in _sql.query(self):
                    self._cache[getattr(r, pk[0])] = r
                    for k in self.additional_keys:
                        self._cache[getattr(r, k)] = r
                _sql.expunge_all()
        else:
            with session_scope(expire_on_commit=False) as _sql:
                for r in _sql.query(self):
                    self._cache[tuple(getattr(r, k) for k in pk)] = r
                _sql.expunge_all()

    def set_cache_dirty(self):
        self._cache = None

    def get(self, item):
        return self.cache[item]

    def get_or_none(self, item):
        return self.cache.get(item, None)

    def len(self):
        return len(self.cache)

    def contains(self, item):
        return item in self.cache

    def iter(self):
        return iter(self.cache)

    def keys(self):
        return self.cache.keys()

    def values(self):
        return self.cache.values()

    def items(self):
        return self.cache.items()

    def copy_cache(self):
        # note: this is not a deep copy
        return self.cache.copy()


class CachedMetaWithNames(CachedMeta):
    additional_keys = ("name",)


class PokedexCachedMeta(CachedMeta):

    def __init__(cls, classname, bases, dict_):
        cls._name_corrections = {}
        cls._form_corrections = {}
        super().__init__(classname, bases, dict_)

    def _build_cache(self):
        self._cache = {}
        special_chars = set(" .-:'")
        with session_scope(expire_on_commit=False) as _sql:
            for r in _sql.query(self):
                self._cache[r.dex_no, r.form] = r
                self._cache[r.name, r.form] = r
                if r.form_id != 0:
                    self._cache[r.form_id] = r

                if set(r.name) & special_chars:
                    name = r.name.capitalize()
                    self._name_corrections[name] = r.name
                    self._name_corrections[name.replace(' ', '')] = r.name
                    name = name.replace('.', '').replace("'", '').replace('-', '').replace(':', '')
                    self._name_corrections[name] = r.name
                    self._name_corrections[name.replace(' ', '')] = r.name

                self._form_corrections[r.form.capitalize()] = r.form
            _sql.expunge_all()

    def search_str(cls, s: str):

        def _try(_arg1, _arg2):
            if _arg1 in cls._name_corrections:
                _arg1 = cls._name_corrections[_arg1]
            if _arg2 in cls._name_corrections:
                _arg2 = cls._name_corrections[_arg2]
            if _arg1 in cls._form_corrections:
                _arg1 = cls._form_corrections[_arg1]
            if _arg2 in cls._form_corrections:
                _arg2 = cls._form_corrections[_arg2]

            if (_arg1, _arg2) in cls.cache:
                return cls.cache[_arg1, _arg2]
            if (_arg2, _arg1) in cls.cache:
                return cls.cache[_arg2, _arg1]

        for i in range(s.strip().count(' ') + 1):
            arg1, arg2 = split_once_at(s, ' ', i)
            if result := _try(arg1.capitalize(), arg2.capitalize()):
                return result

        raise KeyError

    def search(self, name: Optional[str] = None, dex_no: Optional[int] = None,
               form: Optional[str] = None, form_id: Optional[int] = None):
        if name is None and dex_no is None:
            raise Exception("cannot get pokedex entry without name or dex number")
        elif name is not None and dex_no is not None:
            raise Exception("provide only one of either name or dex_no")

        if name is not None:
            name = name.capitalize()

        # form_id supersedes form
        if form_id is not None:
            if 776 <= form_id <= 778:
                # Nidorans have the same form numbers, use dex instead
                pass
            elif form_id != 0:
                return self.cache[form_id]

        if form is None:
            return self.cache[name or dex_no, ""]
        elif form == "Normal":
            try:
                return self.cache[name or dex_no, form]
            except KeyError:
                return self.cache[name or dex_no, ""]
        else:
            return self.cache[name or dex_no, form]


metadata = sqlalchemy.MetaData()
Base = decl.declarative_base(metadata=metadata)
CachedBase = decl.declarative_base(metadata=metadata, metaclass=CachedMeta)
CachedBaseWithNames = decl.declarative_base(metadata=metadata, metaclass=CachedMetaWithNames)
