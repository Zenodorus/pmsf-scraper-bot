import datetime
import logging
import time
from typing import Optional, Union

import discord
import s2sphere as s2sphere
import sqlalchemy.ext.declarative as decl

from sqlalchemy import select, func, desc
from sqlalchemy import Column, String, Integer, ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import relationship

from sql import engine, session_scope
from sql.meta import metadata, PokedexCachedMeta, Base, CachedBase, CachedBaseWithNames
from util import MAPS_URL
from util.math import midnight
from util.s2sphere import same_s2_cell

log = logging.getLogger(__name__)


# Very unlikely to change

class Types(CachedBaseWithNames):
    __tablename__ = "Types"

    type_id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    color = Column(Integer, nullable=False)


# Game Master updates

class Pokedex(decl.declarative_base(metadata=metadata, metaclass=PokedexCachedMeta)):
    __tablename__ = "Pokedex"

    dex_no = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    form = Column(String, primary_key=True)
    form_id = Column(Integer, nullable=False)
    type_1 = Column(Integer, nullable=False)
    type_2 = Column(Integer)
    family = Column(String, nullable=False)
    prior_evolution = Column(String)
    base_atk = Column(Integer)
    base_def = Column(Integer)
    base_sta = Column(Integer)
    buddy_distance = Column(Integer)
    height = Column(String)
    weight = Column(String)
    height_std_dev = Column(String)
    weight_std_dev = Column(String)
    is_gym_deployable = Column(Integer)
    is_transferable = Column(Integer)
    rarity = Column(String)
    quick_moves = Column(String)
    charge_moves = Column(String)
    third_move_stardust = Column(Integer)
    third_move_candy = Column(Integer)
    purify_stardust = Column(Integer)
    purify_candy = Column(Integer)
    thumbnail = Column(String)
    bit_icon = Column(String)
    can_evolve = Column(Integer, nullable=False, default=0)

    @property
    def prior_dex(self) -> Optional["Pokedex"]:
        try:
            if self.prior_evolution is None:
                return None
            elif 862 <= self.dex_no <= 867:
                return Pokedex.search(name=self.prior_evolution, form="Galarian")
            elif self.form.startswith("Mega"):
                return Pokedex.search(name=self.prior_evolution, form="")
            else:
                return Pokedex.search(name=self.prior_evolution, form=self.form)
        except KeyError:
            return None

    def __eq__(self, other):
        if isinstance(other, Pokedex):
            return self.dex_no == other.dex_no and self.form == other.form
        else:
            return super().__eq__(other)

    def __hash__(self):
        return hash((self.dex_no, self.form))

    def __str__(self):
        return ((self.form + ' ') if self.form not in ("Normal", "") else "") + self.name


class Moves(CachedBaseWithNames):
    __tablename__ = "Moves"

    move_id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    move_type = Column(Integer, nullable=False)
    power = Column(Integer, nullable=False)
    pokemon_type = Column(Integer, nullable=False)
    energy_delta = Column(Integer, nullable=False)
    critical_chance = Column(Integer, nullable=False)
    stamina_loss_scalar = Column(Integer, nullable=False)
    heal_scalar = Column(Integer, nullable=False)
    damage_window_start = Column(Integer, nullable=False)
    damage_window_end = Column(Integer, nullable=False)
    duration = Column(Integer, nullable=False)


# Full Cache

class PokemonFilter(Base):
    __tablename__ = "PokemonFilter"

    pokemon_id = Column(ForeignKey(Pokedex.dex_no), primary_key=True)
    perfect = Column(Integer, nullable=False)
    atk = Column(Integer, nullable=False)
    always = Column(Integer, nullable=False)
    on_campus = Column(Integer, nullable=False)
    verified_end_time = Column(Integer, default=0)


class _PokemonFilter(CachedBase):
    __tablename__ = "_PokemonFilter"

    pokemon_id = Column(Integer, primary_key=True)
    form = Column(String, primary_key=True)
    always = Column(Integer, nullable=False)
    perfect = Column(Integer, nullable=False)
    verified_end_time = Column(Integer, default=0)

    __table_args__ = (ForeignKeyConstraint((pokemon_id, form), (Pokedex.dex_no, Pokedex.form)),)


class Spawnpoints(Base):
    __tablename__ = "Spawnpoints"

    spawnpoint_id = Column(Integer, primary_key=True)
    latitude = Column(String, nullable=False)
    longitude = Column(String, nullable=False)
    despawn_time = Column(Integer)
    duration = Column(Integer)
    s2_cellid = Column(String, nullable=False)
    nest_id = Column(Integer)

    @classmethod
    def from_form_data(cls, sp: dict) -> Optional["Spawnpoints"]:
        pass

    def __repr__(self):
        return (f"<Spawnpoints id={self.spawnpoint_id} lat={self.latitude} lng={self.longitude} "
                f"time={self.despawn_time} s2_cell={self.s2_cellid}>")


class Gyms(CachedBase):
    __tablename__ = "Gyms"

    gym_id = Column(String, primary_key=True)
    latitude = Column(String, nullable=False)
    longitude = Column(String, nullable=False)
    s2_cellid = Column(String, nullable=False)
    name = Column(String)
    url = Column(String)
    last_modified = Column(Integer, nullable=False)
    team_id = Column(Integer, nullable=False)
    slots_available = Column(Integer, nullable=False)
    last_scanned = Column(Integer, nullable=False)
    ex_gym = Column(Integer, nullable=False)
    sponsor = Column(String)

    @classmethod
    def embed(cls, key) -> Optional[discord.Embed]:
        gym = cls.get_or_none(key)
        if gym is None:
            return

        e = discord.Embed(title=gym.name, timestamp=datetime.datetime.utcnow(),
                          description=MAPS_URL.format(lat=gym.latitude, lng=gym.longitude))
        e.set_thumbnail(url=gym.url)
        e.set_footer(text=gym.gym_id)

        e.colour = {0: e.color, 1: 0x3D9FFF, 2: 0xEE5B5B, 3: 0xFFC432}[gym.team_id]
        e.add_field(name="Current Occupants", inline=False,
                    value={0: "Unoccupied", 1: "Mystic", 2: "Valor", 3: "Instinct"}[gym.team_id])

        query = select([Raids]).where(Raids.end_time > midnight(time.time())).where(Raids.gym_id == gym.gym_id). \
            order_by(desc(Raids.end_time))
        raids = [r for r in engine.execute(query)]
        if not raids or raids[0].end_time < time.time():
            e.add_field(name="Raids", value="No raids found today.", inline=False)
        else:
            if raids[0].end_time > time.time():
                value = "{form} {pokemon} is attacking the gym with {move_1} and {move_2}, and it flees at {end}."
            e.add_field(name="Raids", value="There is an active raid right now.", inline=False)

        pokemon = PokemonEncounters.nearby(lat=float(gym.latitude), lng=float(gym.longitude))
        # value = '\n'.join("{name} ({d} m)".format(name=p[1].name, d=p[0]) for p in pokemon)
        value = None
        e.add_field(name="Nearby Pokemon", value=value if value else "No pokemon nearby.", inline=False)

        e.add_field(name="Nearby Pokestops", value="Soon TM", inline=False)

        gyms = Gyms.nearby_gyms(lat=float(gym.latitude), lng=float(gym.longitude))
        value = '\n'.join("{name} ({d} m)".format(name=g[1].name, d=g[0]) for g in gyms)
        e.add_field(name="Nearby Gyms", value=value if value else "No gyms nearby.", inline=False)

        return e

    @classmethod
    def search(cls, args):
        return [gym for (gid, gym) in cls.items()
                if args.lower() in gid.lower() or (gym.name is not None and args.lower() in gym.name.lower())]

    @classmethod
    def select_ex_gyms(cls):
        return select([Gyms]).where(Gyms.ex_gym == 1)

    @classmethod
    def select_current_exgyms(cls):
        end = midnight(time.time()) + 86400
        sub_query = select([Raids.gym_id.label("raid_gym_id"), func.max(Raids.end_time).label("end")]). \
            where(end > Raids.end_time). \
            where(Raids.end_time > end - 86400). \
            group_by(Raids.gym_id)
        return select([Gyms.name]).select_from(
            Gyms.__table__.outerjoin(sub_query, Gyms.gym_id == sub_query.c.raid_gym_id)). \
            where(sub_query.c.end == None).where(Gyms.ex_gym == 1)

    @classmethod
    def select_gym_count_by_team(cls):
        return select([Gyms.team_id, func.count(Gyms.team_id).label("count")]).group_by(Gyms.team_id)

    @staticmethod
    def nearby_gyms(lat: Optional[float] = None, lng: Optional[float] = None,
                    s2_cellid: Union[s2sphere.CellId, int, None] = None):
        if lat is not None and lng is not None:
            latlng = s2sphere.LatLng.from_degrees(lat=lat, lng=lng)
            s2_cellid = s2sphere.CellId.from_lat_lng(latlng).parent(17)
        elif s2_cellid is not None:
            if isinstance(s2_cellid, int):
                s2_cellid = s2sphere.CellId(s2_cellid)
            if s2_cellid.level() < 17:
                raise Exception("")
            s2_cellid = s2_cellid.parent(17)
            latlng = s2_cellid.to_lat_lng()
            lat = latlng.lat().degrees
            lng = latlng.lng().degrees
        else:
            raise Exception("")

        with session_scope() as s:
            gyms = {x.gym_id: x for x in s.execute(select([Gyms]))}

        s2_distances = []
        for gym in gyms.values():
            s2_distances.append((abs(lat - float(gym.latitude)) ** 2 + abs(lng - float(gym.longitude)) ** 2, gym))
        s2_distances.sort()
        s2_distances = s2_distances[:20]
        for i in range(len(s2_distances)):
            _, gym = s2_distances[i]
            latlng2 = s2sphere.LatLng.from_degrees(lat=float(gym.latitude), lng=float(gym.longitude))
            s2_distances[i] = round(latlng.get_distance(latlng2).radians * 6371000, 0), gym
        return tuple(filter(lambda x: x[0] < 1000, s2_distances))


class Pokestops(CachedBase):
    __tablename__ = "Pokestops"

    pokestop_id = Column(String, primary_key=True)
    latitude = Column(String, nullable=False)
    longitude = Column(String, nullable=False)
    s2_cellid = Column(String, nullable=False)
    name = Column(String)
    url = Column(String)
    last_scanned = Column(Integer, default=0)

    @classmethod
    def nearest(cls, lat: Optional[float] = None, lng: Optional[float] = None,
                s2_cellid: Union[s2sphere.CellId, int, None] = None):
        if lat is not None and lng is not None:
            latlng = s2sphere.LatLng.from_degrees(lat=lat, lng=lng)
            s2_cellid = s2sphere.CellId.from_lat_lng(latlng).parent(17)
        elif s2_cellid is not None:
            if isinstance(s2_cellid, int):
                s2_cellid = s2sphere.CellId(s2_cellid)
            if s2_cellid.level() < 17:
                raise Exception("")
            s2_cellid = s2_cellid.parent(17)
            latlng = s2_cellid.to_lat_lng()
            lat = latlng.lat().degrees
            lng = latlng.lng().degrees
        else:
            raise Exception("")

        s2_distances = []
        for stop in cls.values():
            s2_distances.append((abs(lat - float(stop.latitude)) ** 2 + abs(lng - float(stop.longitude)) ** 2, stop))
        s2_distances.sort()
        s2_distances = s2_distances[:20]
        minimum = 0xFFFFFFFF
        min_stop = None
        for (_, stop) in s2_distances:
            latlng2 = s2sphere.LatLng.from_degrees(lat=float(stop.latitude), lng=float(stop.longitude))
            distance = latlng.get_distance(latlng2).radians
            if distance < minimum:
                minimum = distance
                min_stop = stop
        return minimum * 6371000, min_stop

    @classmethod
    def embed(cls, key) -> Optional[discord.Embed]:
        stop = cls.get_or_none(key)
        if stop is None:
            return

        e = discord.Embed(title=stop.name, timestamp=datetime.datetime.utcnow(),
                          description=MAPS_URL.format(lat=stop.latitude, lng=stop.longitude))
        e.set_thumbnail(url=stop.url)
        e.set_footer(text=stop.pokestop_id)

        e.add_field(name="Today's Research", value="Soon TM", inline=False)

        incident = None
        for r in engine.execute(Incidents.latest()):
            if r.pokestop_id == stop.pokestop_id:
                incident = r
        if incident is None:
            e.add_field(name="Team Rocket", value="All is safe around this Pokéstop.", inline=False)
        else:
            team_id = incident.team_id
            team = engine.execute(select([RocketTeams]).where(RocketTeams.team_id == team_id)).fetchone()
            value = "A {gender} Team GO Rocket Grunt with {type} type Pokemon was spotted!"
            value = value.format(gender="female" if team.gender else "male", type=team.type)
            e.add_field(name="Team Rocket", value=value, inline=False)

        pokemon = PokemonEncounters.nearby(lat=float(stop.latitude), lng=float(stop.longitude))
        value = '\n'.join("{name} ({d} m)".format(name=Pokedex.search(dex_no=p[1].dex_no, form=p[1].form).name, d=p[0])
                          for p in pokemon)
        e.add_field(name="Nearby Pokemon", value=value if value else "No pokemon nearby.", inline=False)
        e.add_field(name="Nearby Pokestops", value="Soon TM", inline=False)

        gyms = Gyms.nearby_gyms(lat=float(stop.latitude), lng=float(stop.longitude))
        value = '\n'.join("{name} ({d} m)".format(
            name=g[1].name if g[1].name is not None else g[1].gym_id, d=g[0]) for g in gyms)
        e.add_field(name="Nearby Gyms", value=value if value else "No gyms nearby.", inline=False)

        return e

    def __repr__(self):
        return f"<Pokestops name={self.name} id={self.pokestop_id} lat={self.latitude} lng={self.longitude}>"


class RocketTeams(Base):
    __tablename__ = "RocketTeams"

    team_id = Column(Integer, primary_key=True)
    type = Column(String, nullable=False)
    gender = Column(Integer, nullable=False)
    pokemon_first = Column(String, nullable=False)
    pokemon_second = Column(String, nullable=False)
    pokemon_third = Column(String, nullable=False)
    second_reward = Column(Integer, nullable=False)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def __eq__(self, other):
        if isinstance(other, RocketTeams):
            return self.team_id == other.team_id
        else:
            return super().__eq__(other)

    def __hash__(self):
        return hash(self.team_id)

    @classmethod
    def get(cls, item):
        result = engine.execute(select([cls]).where(cls.team_id == item)).fetchone()
        return None if result is None else cls(**result)


class Items(CachedBaseWithNames):
    __tablename__ = "Items"

    item_id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    icon = Column(String, nullable=False)

    def __eq__(self, other):
        if isinstance(other, Items):
            return self.item_id == other.item_id
        else:
            return super().__eq__(other)

    def __hash__(self):
        return hash(self.item_id)


class QuestTypes(Base):
    __tablename__ = "QuestTypes"

    type_id = Column(String, primary_key=True)
    quest_text = Column(String, nullable=False)

    @classmethod
    def select(cls, item):
        return select([cls]).where(cls.type_id == item)


# Active Cache

class PokemonEncounters(Base):
    __tablename__ = "PokemonEncounters"

    encounter_id = Column(String, primary_key=True)
    disappear_time = Column(Integer, nullable=False)
    discovered_time = Column(Integer, nullable=False)
    last_updated_time = Column(Integer, nullable=False)

    dex_no = Column(Integer, nullable=False)
    form = Column(String, default="", nullable=False)
    form_id = Column(Integer, default=0)
    costume = Column(Integer, default=0)
    gender = Column(Integer)
    weather_boosted = Column(Integer)

    expire_timestamp_verified = Column(Integer)
    spawn_id = Column(Integer, ForeignKey(Spawnpoints.spawnpoint_id))
    latitude = Column(String, nullable=False)
    longitude = Column(String, nullable=False)

    weight = Column(String)
    height = Column(String)
    atk_iv = Column(Integer)
    def_iv = Column(Integer)
    sta_iv = Column(Integer)
    move_1 = Column(Integer, ForeignKey(Moves.move_id))
    move_2 = Column(Integer, ForeignKey(Moves.move_id))
    cp = Column(Integer)
    level = Column(Integer)
    nearby_pokestop_id = Column(String, ForeignKey(Pokestops.pokestop_id))

    dex = relationship(Pokedex)
    spawnpoint = relationship(Spawnpoints)
    nearby_pokestop = relationship(Pokestops)

    __table_args__ = (ForeignKeyConstraint((dex_no, form), (Pokedex.dex_no, Pokedex.form)),)

    STAT_KEYS = ("weight", "height", "atk_iv", "def_iv", "sta_iv", "move_1", "move_2", "cp", "level")

    @classmethod
    def from_form_data(cls, enc) -> Optional["PokemonEncounters"]:
        current_time = int(time.time())

        try:
            dex = Pokedex.cache[enc["pokemon_id"], ""] if enc["form"] == 0 else Pokedex.cache[enc["form"]]
        except KeyError:
            log.error(f'Unable to find form id {enc["form"]} for #{enc["pokemon_id"]}')
            return

        try:
            p = cls(encounter_id=enc["encounter_id"],
                    disappear_time=enc["disappear_time"] // 1000,
                    discovered_time=enc["first_seen_timestamp"] // 1000,
                    last_updated_time=current_time,
                    dex=dex,
                    form_id=enc["form"],
                    costume=enc["costume"],
                    gender=enc["gender"],
                    weather_boosted=enc["weather_boosted_condition"],
                    expire_timestamp_verified=enc["expire_timestamp_verified"],
                    spawn_id=None if enc["spawn_id"] is None else int(enc["spawn_id"]),
                    latitude=str(enc["latitude"]),
                    longitude=str(enc["longitude"]),
                    weight=None if enc["weight"] is None else str(enc["weight"]),
                    height=None if enc["height"] is None else str(enc["height"]),
                    atk_iv=enc["individual_attack"],
                    def_iv=enc["individual_defense"],
                    sta_iv=enc["individual_stamina"],
                    move_1=None if enc["move_1"] is None else int(enc["move_1"]),
                    move_2=None if enc["move_2"] is None else int(enc["move_2"]),
                    cp=None if enc["cp"] is None else int(enc["cp"]),
                    level=None if enc["level"] is None else int(enc["level"]))
        except KeyError:
            log.error(f'PokemonEncounter {enc["encounter_id"]} is missing keys')
            return None

        if p.spawn_id is None and p.expire_timestamp_verified == 1:
            log.warning(f"PokemonEncounter {p.encounter_id} has spawn_id and expire_timestamp_verified in conflict")
        if p.spawn_id is None and p.level is not None:
            log.warning(f"PokemonEncounter {p.encounter_id} has IV data but no spawnpoint id")

        if all(getattr(p, k) is None for k in PokemonEncounters.STAT_KEYS):
            return p
        elif all(getattr(p, k) is not None for k in PokemonEncounters.STAT_KEYS):
            return p
        else:
            log.warning(f"PokemonEncounter {p.encounter_id} has bad data")
            return None

    def same_s2_cell(self, other) -> bool:
        if isinstance(other, PokemonEncounters):
            if self.latitude == other.latitude and self.longitude == other.longitude:
                return True
            else:
                return same_s2_cell(float(self.latitude), float(self.longitude),
                                    float(other.latitude), float(other.longitude), 20)
        return False

    @classmethod
    def select_after(cls, after: Union[int, float, None] = None):
        if after is None:
            after = int(time.time())
        return select([cls]).where(cls.disappear_time >= after)

    @classmethod
    def nearby(cls, lat: Optional[float] = None, lng: Optional[float] = None,
                    s2_cellid: Union[s2sphere.CellId, int, None] = None):
        if lat is not None and lng is not None:
            latlng = s2sphere.LatLng.from_degrees(lat=lat, lng=lng)
            s2_cellid = s2sphere.CellId.from_lat_lng(latlng).parent(17)
        elif s2_cellid is not None:
            if isinstance(s2_cellid, int):
                s2_cellid = s2sphere.CellId(s2_cellid)
            if s2_cellid.level() < 17:
                raise Exception("")
            s2_cellid = s2_cellid.parent(17)
            latlng = s2_cellid.to_lat_lng()
            lat = latlng.lat().degrees
            lng = latlng.lng().degrees
        else:
            raise Exception("")

        query = select([PokemonEncounters]).where(PokemonEncounters.disappear_time >= int(time.time()))
        pokemon = {x.encounter_id: x for x in engine.execute(query)}

        s2_distances = []
        for p in pokemon.values():
            s2_distances.append((abs(lat - float(p.latitude)) ** 2 + abs(lng - float(p.longitude)) ** 2, p))
        s2_distances.sort()
        s2_distances = s2_distances[:25]
        for i in range(len(s2_distances)):
            _, p = s2_distances[i]
            latlng2 = s2sphere.LatLng.from_degrees(lat=float(p.latitude), lng=float(p.longitude))
            s2_distances[i] = round(latlng.get_distance(latlng2).radians * 6371000, 0), p
        return tuple(filter(lambda x: x[0] < 100, s2_distances))

    def __repr__(self):
        return f"<PokemonEncounter id={self.encounter_id} dex={self.dex} lat={self.latitude} lng={self.longitude}>"


class Raids(Base):
    __tablename__ = "Raids"

    rowid = Column(Integer, primary_key=True)
    gym_id = Column(String, ForeignKey(Gyms.gym_id), nullable=False)
    start_time = Column(Integer, nullable=False)
    end_time = Column(Integer, nullable=False)
    pokemon_id = Column(Integer)
    pokemon_form = Column(String)
    level = Column(Integer, nullable=False)
    pokemon_move_1 = Column(Integer, ForeignKey(Moves.move_id))
    pokemon_move_2 = Column(Integer, ForeignKey(Moves.move_id))
    pokemon_gender = Column(Integer)

    gym = relationship(Gyms)
    dex = relationship(Pokedex)
    move_1 = relationship(Moves, foreign_keys=pokemon_move_1)
    move_2 = relationship(Moves, foreign_keys=pokemon_move_2)

    __table_args__ = (ForeignKeyConstraint((pokemon_id, pokemon_form), (Pokedex.dex_no, Pokedex.form)),)

    @classmethod
    def latest(cls):
        return select([cls, func.max(cls.start_time).label("latest")]).group_by(cls.gym_id)


class Incidents(Base):
    __tablename__ = "Incidents"

    rowid = Column(Integer, primary_key=True)
    pokestop_id = Column(String, ForeignKey(Pokestops.pokestop_id), nullable=False)
    end_time = Column(Integer, nullable=False)
    team_id = Column(Integer, ForeignKey(RocketTeams.team_id), nullable=False)

    pokestop = relationship(Pokestops)
    rocket_team = relationship(RocketTeams)

    @classmethod
    def latest(cls):
        return select([cls, func.max(cls.end_time).label("latest")]).group_by(cls.pokestop_id)


class Quests(Base):
    __tablename__ = "Quests"

    rowid = Column(Integer, primary_key=True)
    pokestop_id = Column(String, ForeignKey(Pokestops.pokestop_id), nullable=False)
    timestamp = Column(Integer, nullable=False)
    quest_type_id = Column(String, ForeignKey(QuestTypes.type_id), nullable=False)
    quantity = Column(Integer, nullable=False)
    args = Column(String)
    reward_stardust = Column(Integer)
    reward_item_id = Column(Integer, ForeignKey(Items.item_id))
    reward_item_quantity = Column(Integer)
    reward_pokemon_id = Column(Integer)
    reward_pokemon_form = Column(String)
    reward_energy_amount = Column(Integer)

    pokestop = relationship(Pokestops)
    quest_type = relationship(QuestTypes)
    reward_item = relationship(Items)
    reward_dex = relationship(Pokedex)

    __table_args__ = (ForeignKeyConstraint((reward_pokemon_id, reward_pokemon_form), (Pokedex.dex_no, Pokedex.form)),)

    @classmethod
    def latest(cls):
        return select([cls, func.max(cls.timestamp).label("latest")]).group_by(cls.pokestop_id)


class PVPStats(Base):
    __tablename__ = "PVPStats"

    rowid = Column(Integer, primary_key=True)
    dex_no = Column(Integer, nullable=False)
    form = Column(String, nullable=False)
    atk_iv = Column(Integer, nullable=False)
    def_iv = Column(Integer, nullable=False)
    sta_iv = Column(Integer, nullable=False)
    target_dex_no = Column(Integer, nullable=False)
    target_dex_form = Column(String, nullable=False)
    max_level = Column(Integer, nullable=False)
    little_league = Column(Integer, nullable=False)
    great_league = Column(Integer, nullable=False)
    ultra_league = Column(Integer, nullable=False)
    metric = Column(String, nullable=False)
    rank = Column(Integer, nullable=False)

    dex = relationship(Pokedex, foreign_keys=[dex_no, form])
    target_dex = relationship(Pokedex, foreign_keys=[target_dex_no, target_dex_form])

    __table_args__ = (ForeignKeyConstraint((dex_no, form), (Pokedex.dex_no, Pokedex.form)),
                      ForeignKeyConstraint((target_dex_no, target_dex_form), (Pokedex.dex_no, Pokedex.form)))

    def __str__(self):
        level = self.little_league or self.great_league or self.ultra_league
        league = "L" if self.little_league else ("G" if self.great_league else "U")
        return (f"{self.target_dex} {league}L {self.atk_iv}/{self.def_iv}/{self.sta_iv} Level {level} "
                f"R{self.rank}")
