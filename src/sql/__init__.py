import contextlib
import logging

import sqlalchemy.orm

log = logging.getLogger(__name__)


engine = sqlalchemy.create_engine("sqlite:///resources/.nosync.store.db", echo=False)
Session = sqlalchemy.orm.sessionmaker(bind=engine)


@contextlib.contextmanager
def session_scope(**kwargs):
    """Provide a transactional scope around a series of operations."""
    session = Session(**kwargs)
    try:
        yield session
        session.commit()
    except Exception as e:
        log.error(str(e))
        session.rollback()
        raise
    finally:
        session.close()
