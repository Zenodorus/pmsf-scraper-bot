import datetime
import random
import time
from typing import List, Iterable, Optional, Union

import discord
from discord.ext import commands
from discord.ext.commands import UserInputError
from sqlalchemy.orm import joinedload

from sql import session_scope
from sql.table import RocketTeams, Incidents, Pokedex
from util.notifier import EmbedSet, Notifier
from util.display import or_list, monospace_tabular

TYPE_COLORS = {"Normal": 0xA8A878, "Fire": 0xF08030, "Fighting": 0xC03028, "Water": 0x6890F0, "Flying": 0xA890F0,
               "Grass": 0x78C850, "Poison": 0xA040A0, "Electric": 0xF8D030, "Ground": 0xE0C068,
               "Psychic": 0xF85888, "Rock": 0xB8A038, "Ice": 0x98D8D8, "Bug": 0xA8B820, "Dragon": 0x7038F8,
               "Ghost": 0x705898, "Dark": 0x705848, "Steel": 0xB8B8D0, "Fairy": 0xEE99AC, 0: 0xA8A878,
               9: 0xF08030, 1: 0xC03028, 10: 0x6890F0, 2: 0xA890F0,
               11: 0x78C850, 3: 0xA040A0, 12: 0xF8D030, 4: 0xE0C068,
               13: 0xF85888, 5: 0xB8A038, 14: 0x98D8D8, 6: 0xA8B820, 15: 0x7038F8,
               7: 0x705898, 16: 0x705848, 8: 0xB8B8D0, 17: 0xEE99AC, "Kanto": 0xFFFFFF, "Snorlax": 0xA8A878,
               "Cliff": 0x3D9FFF, "Arlo": 0xEE5B5B, "Sierra": 0xFFC432, "Giovanni": 0xA8A878}


class IncidentEmbedSet(EmbedSet):

    def __init__(self, rocket_team: RocketTeams):
        super().__init__(rocket_team)
        self._data: List[Incidents]
        self._key: RocketTeams

    @property
    def expired(self):
        return False

    @property
    def _embed_title(self):
        if not self._data:
            if self._key.type == "Giovanni":
                title = "Giovanni hasn't been seen."
            elif self._key.type in ("Cliff", "Arlo", "Sierra"):
                title = "Executive {type} hasn't been seen."
            else:
                title = "A {gender} Team GO Rocket Grunt with {type} type Pokemon hasn't been seen."
        else:
            if self._key.type == "Giovanni":
                title = "Giovanni was spotted!"
            elif self._key.type in ("Cliff", "Arlo", "Sierra"):
                title = "Executive {type} was spotted!"
            else:
                title = "A {gender} Team GO Rocket Grunt with {type} type Pokemon was spotted!"
        return title.format(gender="female" if self._key.gender else "male", type=self._key.type)

    @property
    def _embed_description(self):
        if not self._data:
            return discord.embeds.EmptyEmbed

        first = tuple(map(int, self._key.pokemon_first.split(','))) if self._key.pokemon_first else ""
        second = tuple(map(int, self._key.pokemon_second.split(','))) if self._key.pokemon_second else ""
        third = tuple(map(int, self._key.pokemon_third.split(','))) if self._key.pokemon_third else ""
        roster = []
        for i in range(3):
            roster.append([Pokedex.search(dex_no=first[i]).name if len(first) > i else "",
                           Pokedex.search(dex_no=second[i]).name if len(second) > i else "",
                           Pokedex.search(dex_no=third[i]).name if len(third) > i else ""])
        table = '\n'.join(monospace_tabular(roster, title_row=["First", "Second", "Third"]))
        description = ("{article} was spotted with the following Pokemon:\n{pokemon}\n"
                       "{article} {second} leave the second pokemon behind.\n"
                       "Look for them at the following locations:")
        return description.format(article="She" if self._key.gender else "He",
                                  second="might" if self._key.second_reward else "won't",
                                  pokemon=table)

    @property
    def _embed_thumbnail(self):
        if self._key.type == "Giovanni":
            return "https://cdn.bulbagarden.net/upload/f/f3/VSGiovanni_GO.png"
        elif self._key.type == "Cliff":
            return "https://cdn.bulbagarden.net/upload/d/d9/VSCliff.png"
        elif self._key.type == "Arlo":
            return "https://cdn.bulbagarden.net/upload/5/5c/VSArlo.png"
        elif self._key.type == "Sierra":
            return "https://cdn.bulbagarden.net/upload/8/82/VSSierra.png"
        elif self._key.type == "Snorlax":
            return Pokedex.search(name="Snorlax").thumbnail
        elif self._key.type == "Kanto":
            return Pokedex.search(dex_no=random.randint(1, 9)).thumbnail
        else:
            return f"https://raw.githubusercontent.com/pmsf/PMSF/main/static/sprites/invasion/{self._key.team_id}.png"

    @property
    def _embed_colour(self):
        return TYPE_COLORS.get(self._key.type, super()._embed_colour)

    def _prepare_data(self, incidents: Iterable[Incidents]) -> Optional[list]:
        if set(i.rowid for i in incidents) == set(i.rowid for i in self._data):
            return None

        incidents = list(incidents)
        if self._key.type in ("Cliff", "Arlo", "Sierra", "Giovanni"):
            incidents.sort(key=lambda i: i.pokestop_id)
        else:
            incidents.sort(key=lambda i: i.end_time)
        value = ("[Directions](https://www.google.com/maps/dir/?api=1&destination={lat},{lng}).  "
                 "{article} escapes at {end_time}!")
        return [(str(i.pokestop.name), value.format(lat=i.pokestop.latitude, lng=i.pokestop.longitude,
                                                    article="She" if self._key.gender else "He",
                                                    end_time=datetime.datetime.fromtimestamp(i.end_time).strftime(
                                                        "%I:%M:%S"))) for i in incidents]


class IncidentNotifier(Notifier):

    _loop_duration = 30
    _EmbedSetClass = IncidentEmbedSet
    _name = "rocket"
    embed_order = ["Normal", "Fighting", "Flying", "Poison", "Ground", "Rock", "Bug", "Ghost", "Steel", "Fire",
                   "Water", "Grass", "Electric", "Psychic", "Ice", "Dragon", "Dark", "Fairy",
                   "Grunt", "Cliff", "Arlo", "Sierra", "Giovanni"]

    def _fetch_data(self):
        current_time = time.time()
        with session_scope(expire_on_commit=False) as sql:
            rocket_teams = sql.query(Incidents).options(joinedload(Incidents.rocket_team)). \
                filter(Incidents.end_time > current_time - 24*3600, Incidents.team_id < 500). \
                group_by(Incidents.team_id).all()
            incidents = sql.query(Incidents).options(joinedload(Incidents.pokestop),
                                                     joinedload(Incidents.rocket_team)). \
                filter(Incidents.end_time > current_time).all()

        rocket_teams = list(map(lambda i: i.rocket_team, rocket_teams))
        rocket_teams.sort(key=lambda rt: (IncidentNotifier.embed_order.index(rt.type), rt.gender))

        return [(rt, tuple(filter(lambda i: i.rocket_team is rt, incidents))) for rt in rocket_teams]


class EventTrainerNotifier(IncidentNotifier):

    _name = "event-trainers"

    def _fetch_data(self):
        current_time = time.time()
        with session_scope(expire_on_commit=False) as sql:
            rocket_teams = sql.query(Incidents).options(joinedload(Incidents.rocket_team)). \
                filter(Incidents.end_time > current_time - 24*3600, Incidents.team_id >= 500). \
                group_by(Incidents.team_id).all()
            incidents = sql.query(Incidents).options(joinedload(Incidents.pokestop),
                                                     joinedload(Incidents.rocket_team)). \
                filter(Incidents.end_time > current_time).all()

        rocket_teams = list(map(lambda i: i.rocket_team, rocket_teams))
        rocket_teams.sort(key=lambda rt: (rt.team_id, rt.gender))

        return [(rt, tuple(filter(lambda i: i.rocket_team is rt, incidents))) for rt in rocket_teams]


class IncidentCog(commands.Cog):

    CHANNEL_ID = [(645062606872248340, IncidentNotifier),
                  (946458510873473075, EventTrainerNotifier)]

    def __init__(self, bot):
        self._bot = bot
        self._notifiers = [cls(bot, cid) for (cid, cls) in self.CHANNEL_ID]

    def cog_unload(self):
        for incident_notify in self._notifiers:
            incident_notify.stop()

    async def cog_command_error(self, ctx, error):
        # TODO: handle when message is more then 2000 characters
        if isinstance(error, commands.UserInputError):
            await ctx.send(content=f"UserInputError: {error}")
        elif isinstance(error, commands.ConversionError):
            await ctx.send(content=f"ConversionError: {error.original}")
        else:
            raise error

    @commands.command()
    async def rocket_team(self, ctx: commands.Context, rtype: str, gender: Union[int, str], position: int,
                          pokemon1: Union[int, str], pokemon2: Union[int, str, None], pokemon3: Union[int, str, None]):
        """
        Update the team rocket roster

        #!rocket_team rtype gender position pokemon1 [pokemon2] [pokemon3]

        Arguments:
            rtype: str
                The type of grunt or leader.  Note: the female grunt uses multiple Snorlax is type "Snorlax".
            gender: Union[str, int]
                Gender of the grunt or leader.  Value can be "male", "m", and 0 or "female", "f", or 1.
            position: int
                The position of the pokemon set in the lineup.
            pokemon1: Union[str, int]
                The name or Pokedex number of a Pokemon in the line.
            pokemon2: Union[str, int, None]
                The name or Pokedex number of a Pokemon in the line.  May be left blank.
            pokemon3: Union[str, int, None]
                The name or Pokedex number of a Pokemon in the line.  May be left blank.
        """
        if isinstance(gender, int) and 0 <= gender <= 1:
            pass
        elif gender.lower() in ("male", "m", "female", "f"):
            gender = int(gender.lower()[0] == "f")
        else:
            raise UserInputError("unable to parse gender: must be either male, m, 0 or female, f, 1")

        try:
            if isinstance(pokemon1, str):
                pokemon1 = Pokedex.search(name=pokemon1)
            elif isinstance(pokemon1, int):
                pokemon1 = Pokedex.search(dex_no=pokemon1)

            if isinstance(pokemon2, str):
                pokemon2 = Pokedex.search(name=pokemon2)
            elif isinstance(pokemon2, int):
                pokemon2 = Pokedex.search(dex_no=pokemon2)

            if isinstance(pokemon3, str):
                pokemon3 = Pokedex.search(name=pokemon3)
            elif isinstance(pokemon3, int):
                pokemon3 = Pokedex.search(dex_no=pokemon3)
        except KeyError:
            raise UserInputError("unknown Pokemon name or Pokedex number")

        pokemon = tuple(filter(None, (pokemon1, pokemon2, pokemon3)))

        with session_scope() as sql:
            team = sql.query(RocketTeams).filter(RocketTeams.type == rtype.capitalize(),
                                                 RocketTeams.gender == gender).all()
            if len(team) >= 2:
                raise Exception("something is wrong with database")
            team = team[0]

            if position == 1:
                team.pokemon_first = ','.join(str(p.dex_no) for p in pokemon)
            elif position == 2:
                team.pokemon_second = ','.join(str(p.dex_no) for p in pokemon)
            elif position == 3:
                team.pokemon_third = ','.join(str(p.dex_no) for p in pokemon)
            else:
                raise UserInputError("position can only be an integer from 1 to 3")

        position = "first" if position == 1 else ("second" if position == 2 else "third")
        if rtype in ("Cliff", "Arlo", "Sierra", "Giovanni"):
            await ctx.send(content=f"Updated the Pokemon in the {position} position of {rtype}'s lineup to be "
                                   f"{or_list(tuple(map(str, pokemon)))}")
        else:
            gender = "female" if gender else "male"
            await ctx.send(content=f"Updated the Pokemon in the {position} position of {gender} grunt's "
                                   f"{rtype.capitalize()} lineup to be {or_list(tuple(map(str, pokemon)))}")


def setup(bot):
    bot.add_cog(IncidentCog(bot))


def teardown(bot):
    pass
