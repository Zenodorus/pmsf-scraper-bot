import asyncio
import datetime
import logging
import time
import re
import pytz
from typing import Optional, Dict, Tuple, List

import discord
from discord.ext import commands, tasks
from sqlalchemy import and_
from sqlalchemy.orm import joinedload

from sql import session_scope
from sql.table import PokemonEncounters, PVPStats, Pokestops, PokemonFilter, Types, _PokemonFilter

GENDER_CODES = (None, "Male", "Female", "Genderless")
WEATHER_CODES = ("No Weather Boost", "Clear Weather Boost", "Rain Weather Boost", "Partly Cloudy Weather Boost",
                 "Cloudy Weather Boost", "Windy Weather Boost", "Snow Weather Boost", "Fog Weather Boost")
MIN_RESPONSE_TIME = 180


COSTUMES = {1: "Santa Hat", 3: "Ash Hat", 8: "Holiday Bells"}

# TODO remove
ALT_BULBAPEDIA_CDN = True

log = logging.getLogger(__name__)


class EncounterMessage(object):

    def __init__(self, enc: PokemonEncounters, pvp: Optional[List[PVPStats]] = None):
        assert enc.weather_boosted is not None and enc.gender is not None
        if enc.level is None:
            assert all(getattr(enc, k) is None for k in ("atk_iv", "def_iv", "sta_iv", "cp", "move_1", "move_2",
                                                         "weight", "height"))
        else:
            assert all(getattr(enc, k) is not None for k in ("atk_iv", "def_iv", "sta_iv", "cp", "move_1", "move_2",
                                                             "weight", "height"))

        self._enc: PokemonEncounters = enc
        self._pvp = pvp if pvp is not None else []
        self._embed = discord.Embed()
        self._nearby: Optional[Tuple[float, Pokestops]] = None
        if not self._enc.nearby_pokestop_id and not self._enc.spawn_id and not self._enc.expire_timestamp_verified:
            self._nearby = Pokestops.nearest(lat=float(self._enc.latitude), lng=float(self._enc.longitude))

        if self._enc.dex is None:
            print("Pokedex mismatch:", self._enc)

    @property
    def encounter(self):
        return self._enc

    @property
    def spawnpoint_info(self):
        direct = (f"[Directions](https://www.google.com/maps/dir/?api=1&destination="
                  f"{self._enc.latitude},{self._enc.longitude}).")
        if self._enc.spawn_id is not None and self._enc.expire_timestamp_verified:
            d, stop = Pokestops.nearest(lat=float(self._enc.latitude), lng=float(self._enc.longitude))
            return f"{direct}  Location and end time are verified.\nNearest Pokestop: {stop.name} ({round(d, 1)} m)."
        elif self._enc.spawn_id is not None:
            return (f"{direct}  Location is verified, but end time is not, "
                    f"so {self._enc.dex.name} may despawn at any moment.")
        elif self._enc.expire_timestamp_verified:
            return f"{direct}  End time is verified, but location is not.  It is likely correct though."
        elif self._enc.nearby_pokestop_id:
            return f"{direct}  Nearby Pokestop: {Pokestops.get(self._enc.nearby_pokestop_id).name}."
        elif self._nearby and self._nearby[0] < 50:
            return f"{direct}  Nearby Pokestop: {self._nearby[1].name}."
        else:
            return (f"{direct}  Location and end time unverified.  {self._enc.dex.name} may not be exactly at " 
                    f"this spot and may despawn at any time.")

    @property
    def full_name(self) -> str:
        costume = COSTUMES.get(self._enc.costume, None)
        return ((costume + ' ') if costume else "") + str(self._enc.dex)

    @property
    def costume(self) -> str:
        return COSTUMES.get(self._enc.costume, str(self._enc.costume))

    @property
    def gender(self):
        return GENDER_CODES[self._enc.gender]

    @property
    def stat_string(self):
        if self._enc.level is None:
            return ""
        elif self._enc.atk_iv == self._enc.def_iv == self._enc.sta_iv == 15:
            return f"L{self._enc.level}, CP {self._enc.cp}"
        else:
            return f"{self._enc.atk_iv}/{self._enc.def_iv}/{self._enc.sta_iv}, L{self._enc.level}, CP {self._enc.cp}"

    @property
    def end_time(self):
        return datetime.datetime.fromtimestamp(self._enc.disappear_time, tz=pytz.timezone("US/Eastern")).strftime(
            "%I:%M:%S")

    @staticmethod
    def pokemon_mention(dex, roles: Dict[str, discord.Role]):
        if dex is not None:
            if str(dex) in roles:
                return roles[str(dex)].mention
            elif dex.name in roles:
                return roles[dex.name].mention
            elif dex.family in roles:
                return roles[dex.family].mention + ' ' + dex.name
            else:
                return str(dex)
        return ""

    def content(self, roles: Dict[str, discord.Role]):
        current_time = time.time()
        next_hour = int(self._enc.discovered_time / 3600 + 1) * 3600

        has_ivs = self._enc.level is not None

        tags = []
        if has_ivs:
            if self._enc.atk_iv + self._enc.def_iv + self._enc.sta_iv == 45 and not self._pvp:
                tags.append("<@&420309415602946070>")
            elif self._pvp:
                if any(pvp.rank == 1 for pvp in self._pvp):
                    tags.append("<@&687090025066004481>")
                if any(pvp.great_league for pvp in self._pvp):
                    tags.append("<@&685361624294490116>")
                if any(pvp.ultra_league for pvp in self._pvp):
                    tags.append("<@&687085938110758987>")

        if self._enc.dex is not None:
            if str(self._enc.dex) in roles:
                tags.append(roles[str(self._enc.dex)].mention)
            elif self._enc.dex.name in roles:
                tags.append(roles[self._enc.dex.name].mention)
            elif self._enc.dex.family in roles:
                tags.append(roles[self._enc.dex.family].mention)

        if tags:
            rtv = "Tags: " + ' '.join(tags) + '\n'
        else:
            rtv = ""

        rtv += f"```Gender:  {self.gender}\n"
        if self._enc.costume:
            rtv += f"Costume: {self._enc.costume}\n"
        elif self._enc.form_id == 2669 and self._enc.dex_no == 25:
            rtv += "Costume: Explorer\n"

        if has_ivs:
            rtv += (f"Stats:   {self._enc.atk_iv}/{self._enc.def_iv}/{self._enc.sta_iv}, Level {self._enc.level}, "
                    f"CP {self._enc.cp}\n")

        rtv += f"Weather: {WEATHER_CODES[self._enc.weather_boosted]}\n"

        if self._enc.expire_timestamp_verified:
            rtv += f"Flees:   {self.end_time}{' (*)' if has_ivs and next_hour < self._enc.disappear_time else ''}```\n"
            if has_ivs and next_hour < self._enc.disappear_time:
                remaining = next_hour - int(current_time)
                if remaining > 0:
                    rtv += f"* Weather may change in {int(remaining / 60)}m{remaining % 60}s\n"
                else:
                    rtv += "* Weather may have already changed\n"
        else:
            rtv += f"Flees:   unknown```\n"

        if self._pvp:
            rtv += "\nPVP Ranks\n"
            for pvp in self._pvp:
                if pvp.great_league:
                    rtv += f"- GL {pvp.target_dex}, Level {pvp.great_league}, Rank {pvp.rank}, Metric {pvp.metric}\n"
                elif pvp.ultra_league:
                    rtv += f"- UL {pvp.target_dex}, Level {pvp.ultra_league}, Rank {pvp.rank}, Metric {pvp.metric}\n"
                elif pvp.little_league:
                    rtv += f"- LL {pvp.target_dex}, Level {pvp.little_league}, Rank {pvp.rank}, Metric {pvp.metric}\n"

        if self._nearby and self._nearby[0] < 100:
            rtv += f"Nearest Pokestop: {self._nearby[1].name} ({round(self._nearby[0], 1)} m)"

        return rtv

    def message_content(self, roles: Dict[str, discord.Role]) -> Optional[str]:
        blurb = ""
        if self._enc.level is not None:
            if self._enc.atk_iv + self._enc.def_iv + self._enc.sta_iv == 45 and not self._pvp:
                blurb = f"<@&420309415602946070> {self.gender} {self.pokemon_mention(self._enc.dex, roles)}"
            elif self._pvp:
                if any(pvp.rank == 1 for pvp in self._pvp):
                    blurb += "<@&687090025066004481> "
                if any(pvp.great_league for pvp in self._pvp):
                    blurb += "<@&685361624294490116> "
                if any(pvp.ultra_league for pvp in self._pvp):
                    blurb += "<@&687085938110758987> "
                targets_dex = sorted(list(set(pvp.target_dex for pvp in self._pvp)), key=lambda x: x.dex_no)
                blurb += f"{self.gender} " + ', '.join(map(lambda x: self.pokemon_mention(x, roles), targets_dex))
                if self._enc.dex not in targets_dex:
                    blurb += f" as {self.pokemon_mention(self._enc.dex, roles)}"

        if not blurb:
            blurb += f"{self.gender} {self.pokemon_mention(self._enc.dex, roles)}"

        if self._enc.expire_timestamp_verified:
            next_hour = int(time.time() / 3600 + 1) * 3600
            blurb += f" ends {self.end_time}{' (*)' if next_hour < self._enc.disappear_time else ''}"
        else:
            blurb += " ends unknown"

        # content = self.content(roles)
        if re.search(r"<@&\d+>", blurb):
            return blurb
        return None

    def build_embed(self, roles: Dict[str, discord.Role]):
        self._embed.title = f"A wild {self.full_name} was spotted!"
        self._embed.description = f"{self.content(roles)}\n{self.spawnpoint_info}"
        tn = self._enc.dex.thumbnail
        if tn is None:
            tn = f"https://serebii.net/pokemon/art/{self._enc.dex.dex_no}.png"
        elif ALT_BULBAPEDIA_CDN:
            tn = tn.replace('cdn.bulbagarden.net', 'archives.bulbagarden.net/media')
        self._embed.set_thumbnail(url=tn)
        self._embed.set_footer(text=self._enc.encounter_id)
        self._embed.timestamp = datetime.datetime.utcnow()
        ptype = Types.get_or_none(self._enc.dex.type_1)
        self._embed.colour = ptype.color if ptype is not None else discord.embeds.EmptyEmbed
        return self._embed


enc_keys = ("encounter_id", "disappear_time", "discovered_time", "last_updated_time", "dex_no", "form", "form_id",
            "costume", "gender", "weather_boosted", "expire_timestamp_verified", "spawn_id", "weight", "height",
            "atk_iv", "def_iv", "sta_iv", "move_1", "move_2", "cp", "level")


def same_encounter(new: PokemonEncounters, old: PokemonEncounters):
    if not all(getattr(new, k) == getattr(old, k) for k in enc_keys):
        return False
    elif new.nearby_pokestop_id is None:
        return old.latitude == new.latitude and old.longitude == new.longitude
    elif old.nearby_pokestop_id == new.nearby_pokestop_id:
        return True
    else:
        return False


class EncounterNotifier(object):

    _loop_duration = 15

    def __init__(self, bot, channel_id):
        self._bot = bot
        self._channel_id = channel_id
        self._channel: Optional[discord.channel.TextChannel] = None

        self._messages: Dict[str, Tuple[discord.Message, PokemonEncounters]] = {}
        self._freq_check_timestamp = 0
        self._freq = {}

        self._guild_roles: Dict[str, discord.Role] = {}
        self._loop = tasks.loop(seconds=self._loop_duration)(self._loop)
        self._loop.before_loop(self._loop_init)
        self._loop.after_loop(self._loop_teardown)
        self._loop.start()

    def _fetch_pokemon(self):
        results = {}
        with session_scope() as sql:
            # if self._freq_check_timestamp + 3600 < (t := time.time()):
            #     self._freq = {}
            #     for enc in sql.query(PokemonEncounters).filter(PokemonEncounters.disappear_time > t - 24 * 3600):
            #         self._freq[enc.dex_no, enc.form] = self._freq.get((enc.dex_no, enc.form), 0) + 1
            #     self._freq_check_timestamp = t

            # query = sql.query(PokemonEncounters, PokemonFilter). \
            #     options(joinedload(PokemonEncounters.dex)). \
            #     join(PokemonFilter, PokemonEncounters.dex_no == PokemonFilter.pokemon_id). \
            #     filter(PokemonEncounters.disappear_time > int(time.time()))
            # for enc, pf in query:
            #     if enc.dex.prior_evolution is not None and enc.dex.prior_evolution != "":
            #         continue
            #     if enc.level is not None:
            #         if enc.atk_iv == enc.def_iv == enc.sta_iv == 15:
            #             pass
            #         elif 0 < pf.verified_end_time <= enc.level and enc.expire_timestamp_verified:
            #             results[enc.encounter_id] = enc
            #         elif pf.atk <= enc.level and enc.atk_iv == 15 and enc.def_iv >= 12 and enc.sta_iv >= 12:
            #             results[enc.encounter_id] = enc
            #         elif pf.always == 1:
            #             results[enc.encounter_id] = enc
            #         elif self._freq.get((enc.dex_no, enc.form), 0) < 5:
            #             self._freq[enc.dex_no, enc.form] = self._freq.get((enc.dex_no, enc.form), 0) + 1
            #             results[enc.encounter_id] = enc
            #     else:
            #         if pf.verified_end_time == 1 and enc.expire_timestamp_verified:
            #             results[enc.encounter_id] = enc
            #         elif pf.always == 1:
            #             if enc.dex_no != 136 or enc.gender != 1:
            #                 results[enc.encounter_id] = enc
            #         elif self._freq.get((enc.dex_no, enc.form), 0) < 5:
            #             self._freq[enc.dex_no, enc.form] = self._freq.get((enc.dex_no, enc.form), 0) + 1
            #             results[enc.encounter_id] = enc

            query = sql.query(PokemonEncounters). \
                options(joinedload(PokemonEncounters.dex)). \
                filter(PokemonEncounters.disappear_time > int(time.time()))
            for enc in query:
                pf: Optional[_PokemonFilter] = _PokemonFilter.get_or_none((enc.dex_no, enc.form))

                if enc.dex.prior_evolution is not None and enc.dex.prior_evolution != "":
                    # evolved forms go into the evolved channel
                    continue
                elif enc.atk_iv == enc.def_iv == enc.sta_iv == 15:
                    # perfect pokemon go into the perfect channel
                    continue
                elif pf is not None:
                    if pf.always:
                        results[enc.encounter_id] = enc
                    elif enc.level is not None:
                        if pf.verified_end_time <= enc.level and enc.expire_timestamp_verified:
                            results[enc.encounter_id] = enc
                    elif pf.verified_end_time == 0 and enc.expire_timestamp_verified:
                        results[enc.encounter_id] = enc

            sql.expunge_all()
        return results

    async def _loop(self) -> None:
        # elif p.dex_no == 19 and p.weight is not None and p.weight <= 2.41 and is_on_campus(p.lat, p.lng):
        #     pass
        # elif p.dex_no == 129 and p.weight is not None and p.weight > 13.13:
        #     pass
        # with profiled():
        self._guild_roles = {r.name: r for r in self._channel.guild.roles}

        pokemon = self._fetch_pokemon()
        current_time = time.time()

        for p in pokemon.values():
            if isinstance(p, PokemonEncounters):
                enc_message = EncounterMessage(p)
            else:
                assert isinstance(p, tuple) and isinstance(p[0], PokemonEncounters)
                enc_message = EncounterMessage(*p)
                p = p[0]

            if p.disappear_time <= current_time + MIN_RESPONSE_TIME:
                continue

            if p.encounter_id in self._messages and (self._messages[p.encounter_id][0].content != "" or
                                                     not same_encounter(p, self._messages[p.encounter_id][1])):
                await self._messages[p.encounter_id][0].edit(content=None,
                                                             embed=enc_message.build_embed(self._guild_roles))
                self._messages[p.encounter_id] = (self._messages[p.encounter_id][0], p)
            elif p.encounter_id not in self._messages:
                self._messages[p.encounter_id] = (await self._channel.send(
                    content=enc_message.message_content(self._guild_roles),
                    embed=enc_message.build_embed(self._guild_roles)), p)

        for key in list(self._messages.keys()):
            if key not in pokemon:
                await self._messages[key][0].delete()
                del self._messages[key]

    async def _loop_init(self) -> None:
        await self._bot.wait_until_ready()
        self._channel = self._bot.get_channel(self._channel_id)
        # await self._channel.edit(name="pokemon")

        pokemon = self._fetch_pokemon()
        self._guild_roles = {r.name: r for r in self._channel.guild.roles}

        async for message in self._channel.history(oldest_first=True):
            message: discord.Message
            if message.pinned:
                pass
            elif message.author != self._bot.user or not message.embeds:
                await message.delete()
            else:
                eid = message.embeds[0].footer.text
                if eid not in pokemon:
                    await message.delete()
                elif isinstance(pokemon[eid], PokemonEncounters):
                    enc_message = EncounterMessage(pokemon[eid])
                    await message.edit(embed=enc_message.build_embed(self._guild_roles))
                    self._messages[eid] = (message, pokemon[eid])
                elif isinstance(pokemon[eid], tuple):
                    enc_message = EncounterMessage(*pokemon[eid])
                    await message.edit(embed=enc_message.build_embed(self._guild_roles))
                    self._messages[eid] = (message, pokemon[eid][0])

    async def _loop_teardown(self) -> None:
        if self._loop.is_being_cancelled():
            log.info(f"Repeated task for {self.__class__.__name__} on channel {self._channel.name} cancelled.")
        # await self._channel.edit(name="pokemon-offline")

    def stop(self):
        self._loop.cancel()


class EvolvedEncounterNotifier(EncounterNotifier):

    def _fetch_pokemon(self):
        results = {}
        with session_scope() as sql:
            query = sql.query(PokemonEncounters). \
                options(joinedload(PokemonEncounters.dex)). \
                filter(PokemonEncounters.disappear_time > int(time.time()))
            for enc in query:
                pf: Optional[_PokemonFilter] = _PokemonFilter.get_or_none((enc.dex_no, enc.form))

                if enc.dex.prior_evolution is None or enc.dex.prior_evolution == "":
                    # only want evolved forms
                    continue
                elif enc.atk_iv == enc.def_iv == enc.sta_iv == 15:
                    # perfect pokemon go into the perfect channel
                    continue
                elif pf is not None:
                    if pf.always:
                        results[enc.encounter_id] = enc
                    elif enc.level is not None:
                        if pf.verified_end_time <= enc.level and enc.expire_timestamp_verified:
                            results[enc.encounter_id] = enc
                    elif pf.verified_end_time == 0 and enc.expire_timestamp_verified:
                        results[enc.encounter_id] = enc

            sql.expunge_all()
        return results


class PerfectEncounterNotifier(EncounterNotifier):

    def _fetch_pokemon(self):
        with session_scope() as sql:
            # query = sql.query(PokemonEncounters, PokemonFilter). \
            #     options(joinedload(PokemonEncounters.dex)). \
            #     join(PokemonFilter, PokemonEncounters.dex_no == PokemonFilter.pokemon_id). \
            #     filter(PokemonEncounters.disappear_time > int(time.time()), PokemonEncounters.atk_iv == 15,
            #            PokemonEncounters.def_iv == 15, PokemonEncounters.sta_iv == 15)

            query = sql.query(PokemonEncounters). \
                options(joinedload(PokemonEncounters.dex)). \
                filter(PokemonEncounters.disappear_time > int(time.time()), PokemonEncounters.atk_iv == 15,
                       PokemonEncounters.def_iv == 15, PokemonEncounters.sta_iv == 15)

            results = {enc.encounter_id: enc for enc in query
                       if ((pf := _PokemonFilter.get_or_none((enc.dex_no, enc.form))) is None or pf.perfect <=
                           enc.level)}
            sql.expunge_all()
        return results


class PVPEncounterNotifier(EncounterNotifier):

    def __init__(self, bot, cid, league):
        self._league = league
        super(PVPEncounterNotifier, self).__init__(bot, cid)

    @property
    def league_column(self):
        if self._league == "little":
            return PVPStats.little_league
        elif self._league == "great":
            return PVPStats.great_league
        elif self._league == "ultra":
            return PVPStats.ultra_league

    def _fetch_pokemon(self):
        results = {}
        with session_scope() as sql:
            query_results = sql.query(PokemonEncounters, PVPStats). \
                options(joinedload(PokemonEncounters.dex), joinedload(PVPStats.target_dex)). \
                join(PVPStats, and_(PokemonEncounters.dex_no == PVPStats.dex_no,
                                    PokemonEncounters.atk_iv == PVPStats.atk_iv,
                                    PokemonEncounters.def_iv == PVPStats.def_iv,
                                    PokemonEncounters.sta_iv == PVPStats.sta_iv,
                                    self.league_column > 0)). \
                filter(PokemonEncounters.disappear_time > int(time.time())).all()

            for enc, pvp in query_results:
                level = pvp.little_league or pvp.great_league or pvp.ultra_league
                if enc.level > level:
                    continue
                elif enc.form == "Normal":
                    if pvp.form != "":
                        continue
                elif enc.form != pvp.form:
                    continue

                if enc.encounter_id not in results:
                    results[enc.encounter_id] = enc, []
                results[enc.encounter_id][1].append(pvp)
            sql.expunge_all()
        return results


class EncounterCog(commands.Cog):

    CHANNEL_ID = [(645062553109528626, EncounterNotifier),
                  (883217754562388048, EvolvedEncounterNotifier),
                  (811352782841053214, PerfectEncounterNotifier),
                  (811352485024628736, PVPEncounterNotifier, "little"),
                  (883100776455618631, PVPEncounterNotifier, "great"),
                  (883100809179566120, PVPEncounterNotifier, "ultra")]

    def __init__(self, bot):
        self._bot = bot
        self._notifiers = [cls(bot, cid, *args) for (cid, cls, *args) in self.CHANNEL_ID]

    def cog_unload(self):
        for encounter_notify in self._notifiers:
            encounter_notify.stop()


def setup(bot):
    bot.add_cog(EncounterCog(bot))


def teardown(bot):
    pass
