import asyncio
import collections
import logging
import json
import traceback

import s2sphere
import time
import re
import urllib.parse as urlparse
import aiohttp
import bs4
import random
import datetime
from typing import Dict, Optional, Union, List, Tuple, Any
import math

from sqlalchemy import select, func, inspect
import discord.gateway
from discord.ext import commands, tasks
from sqlalchemy.orm import joinedload

import config
from bot import PMSFBot
from sql.table import (Pokedex, Raids, PokemonEncounters, Incidents, Quests, Gyms, Spawnpoints, Pokestops,
                       RocketTeams, QuestTypes, Items)
# from util import gnu_parse, apply, profiled
from util import apply
from util.display import monospace_tabular
from util.s2sphere import get_s2_cellid, nearby_sighting_s2_cells, s2_cell_neighbor, surface_distance
from util.math import midnight
from sql import session_scope


discord.client.log.setLevel(logging.INFO)
discord.gateway.log.setLevel(logging.INFO)


PURDUE_POGO_SERVER_ID = 284552964352245760
DEBUG_SERVER_ID = 344206160691200000
CHANNEL_ID = 284553225309257728
DEBUG_CHANNEL_ID = 344206160691200000


ROCKET_TEAM_TYPE = {"Executive Sierra": "Sierra", "Executive Arlo": "Arlo", "Executive Cliff": "Cliff",
                    "Giovanni or Decoy": "Giovanni"}
WEATHER_BOOST = [3, 4, 5, 4, 1, 3, 1, 7, 6, 1, 2, 1, 2, 5, 6, 5, 7, 4]

log = logging.getLogger(__name__)


def set_if_different(obj: Any, attr: str, value: Any) -> None:
    if getattr(obj, attr) != value:
        setattr(obj, attr, value)


class UnknownQuestError(Exception):
    pass


class Weather(object):

    s2cells: List[int] = [9805177515180818432, 9805148927878496256, 9805184112250585088, 9805212699552907264,
                          9805186311273840640, 9805210500529651712]
    weather_ids: List[str] = ["Unset", "Clear", "Rain", "Partly Cloudy", "Cloudy", "Windy", "Snow", "Fog"]

    def __init__(self):
        self._hour: int = int(time.time() - 10) // 3600
        self._weather: Dict[int, int] = {i: 0 for i in Weather.s2cells}
        self._done_updating: bool = False

    @property
    def hour(self) -> int:
        return self._hour

    @property
    def _is_time_to_update(self) -> bool:
        return self._hour == int(time.time() - 10) // 3600

    @property
    def _is_weather_determined(self) -> bool:
        if not self._done_updating:
            self._done_updating = all(self._weather.values())
        return self._done_updating

    @property
    def is_updated(self) -> bool:
        if self._is_time_to_update:
            for k in self._weather:
                self._weather[k] = 0
            self._done_updating = False
            self._hour = int(time.time() - 10) // 3600
            return False
        else:
            return self._is_weather_determined

    def update(self, lat: float, lng: float, new_weather: int) -> None:
        cellid = s2sphere.CellId.from_lat_lng(s2sphere.LatLng.from_degrees(lat, lng)).parent(10).id()
        try:
            old_weather = self._weather[cellid]
        except KeyError:
            pass
        else:
            if old_weather == 0:
                self._weather[cellid] = new_weather
            else:
                assert old_weather == new_weather

    def get_weather(self, token: Optional[str] = None, cellid: Optional[Union[int, s2sphere.CellId]] = None,
                    lat: Optional[float] = None, lng: Optional[float] = None) -> Optional[str]:
        if cellid is not None:
            assert token is None and lat is None and lng is None
            if type(cellid) is int:
                cellid = s2sphere.CellId(cellid)
            assert type(cellid) is s2sphere.CellId
        elif token is not None:
            assert lat is None and lng is None
            cellid = s2sphere.CellId.from_token(token)
        elif lat is not None and lng is not None:
            cellid = s2sphere.CellId.from_lat_lng(s2sphere.LatLng.from_degrees(lat, lng))
        else:
            assert False

        for cid in self._weather:
            if s2sphere.CellId(cid).contains(cellid):
                return self.weather_ids[self._weather[cid]]


class PMSFClientDataCache(object):

    def __init__(self):
        self.encounter: Dict[str, PokemonEncounters] = {}
        self.raid: Dict[str, Raids] = {}
        self.incident: Dict[str, Incidents] = {}
        self.quest: Dict[str, Quests] = {}
        self.gym: Dict[str, Gyms] = {}
        self.spawnpoint: Dict[int, Spawnpoints] = {}
        self.pokestop: Dict[str, Pokestops] = {}
        self.rocket_team: Dict[int, RocketTeams] = {}
        self.quest_type: Dict[Tuple[int, int, int], QuestTypes] = {}
        self.nearby: Dict[Tuple[float, float], str] = {}

    def init(self):
        with session_scope(expire_on_commit=False) as s:
            query = s.query(PokemonEncounters).options(joinedload(PokemonEncounters.spawnpoint)). \
                filter(PokemonEncounters.disappear_time >= time.time() - 5400)
            self.encounter = {r.encounter_id: r for r in query}

            query = s.query(Raids, func.max(Raids.start_time)).group_by(Raids.gym_id)
            self.raid = {r[0].gym_id: r[0] for r in query}

            query = s.query(Incidents, func.max(Incidents.end_time)).group_by(Incidents.pokestop_id)
            self.incident = {i[0].pokestop_id: i[0] for i in query}

            query = s.query(Quests, func.max(Quests.timestamp)). \
                options(joinedload(Quests.quest_type), joinedload(Quests.reward_dex)). \
                group_by(Quests.pokestop_id)
            self.quest = {q[0].pokestop_id: q[0] for q in query}

            self.gym = {g.gym_id: g for g in s.query(Gyms)}
            self.spawnpoint = {sp.spawnpoint_id: sp for sp in s.query(Spawnpoints)}
            self.pokestop = {p.pokestop_id: p for p in s.query(Pokestops)}
            self.rocket_team = {rt.team_id: rt for rt in s.query(RocketTeams)}
            self.quest_type = {tuple(map(int, qt.type_id.split(','))): qt for qt in s.query(QuestTypes)}

    async def update_nearby(self):
        self.nearby.clear()
        diag = 0.0003/math.sqrt(2)
        for stop in self.pokestop.values():
            lat, lng = float(stop.latitude), float(stop.longitude)
            coords = ((lat + 0.0003, lng), (lat, lng + 0.0003), (lat - 0.0003, lng), (lat, lng - 0.0003),
                      (lat + diag, lng + diag), (lat + diag, lng - diag),
                      (lat - diag, lng - diag), (lat - diag, lng + diag))
            for c in coords:
                if c in self.nearby:
                    print("cache fail", c, stop.pokestop_id, self.nearby[c])
                self.nearby[c] = stop.pokestop_id


class PMSFClientStatus(object):

    def __init__(self, cache: PMSFClientDataCache):
        self.cache: PMSFClientDataCache = cache
        self.logged_in: bool = False
        self.start: int = 0
        self.fail_time: int = 0
        self.http_code: int = 0
        self.total_downloaded: int = 0
        self.perf_times = {"http": collections.deque((0.0,) * 60, maxlen=60),
                           "process": collections.deque((0.0,) * 60, maxlen=60),
                           "sqlsession": collections.deque((0.0,) * 60, maxlen=60),
                           "commit": collections.deque((0.0,) * 60, maxlen=60)}
        self._message: Optional[discord.Message] = None
        self._channel: Optional[discord.TextChannel] = None

    async def init(self, channel: discord.TextChannel):
        self._channel = channel
        self._message = await channel.send(content="Starting PMSFClient")

        self.start = int(time.time())
        self.total_downloaded = 0
        self.logged_in = False

    async def failed_login(self):
        await self._message.edit(content="Unable to login to PMSF.")

    async def success_login(self):
        await self._message.edit(content="Successfully logged into PMSF.")

    async def update(self):
        content = f"""
        PMSFClient status info.
        
        Time: {datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        
        Start time: {datetime.datetime.fromtimestamp(self.start).strftime("%Y-%m-%d %H:%M:%S")}
        HTTP Status: {self.http_code}
        Last 200 time: {datetime.datetime.fromtimestamp(self.fail_time).strftime("%Y-%m-%d %H:%M:%S")}
        Total Downloaded: {self.total_downloaded:,} bytes
        
        Load (5 min avg)
        HTTP (async): {round(sum(self.perf_times["http"]) / 60 * 1000)}
        Process (async): {round(sum(self.perf_times["process"]) / 60 * 1000)}
        SQL Session (sync): {round(sum(self.perf_times["sqlsession"]) / 60 * 1000)}
        Commit (sync): {round(sum(self.perf_times["commit"]) / 60 * 1000)}
        
        Cache
        PokemonEncounters: {len(self.cache.encounter):,}
        Pokestops: {len(self.cache.pokestop)}
        Gyms: {len(self.cache.gym)}
        Raids: {len(self.cache.raid)}
        Incidents: {len(self.cache.incident)}
        Quests: {len(self.cache.quest)}
        """
        await self._message.edit(content=content)


class PMSFClientRawData(object):

    RAW_DATA_PAYLOAD_KEYS = (
        "timestamp",         "pokemon",           "loadOverviewStats", "loadPokemonStats",  "loadRewardStats",
        "loadShinyStats",    "lastpokemon",       "pokestops",         "lures",             "rocket",
        "quests",            "dustamount",        "reloaddustamount",  "nestavg",           "nests",
        "lastnests",         "communities",       "lastcommunities",   "portals",           "pois",
        "lastpois",          "newportals",        "lastportals",       "lastpokestops",     "gyms",
        "raids",             "lastgyms",          "exEligible",        "lastslocs",         "spawnpoints",
        "lastspawns",        "scanlocations",     "minIV",             "prevMinIV",         "minLevel",
        "prevMinLevel",      "bigKarp",           "tinyRat",           "despawnTimeType",   "pokemonGender",
        "swLat",             "swLng",             "neLat",             "neLng",             "oSwLat",
        "oSwLng",            "oNeLat",            "oNeLng",            "reids",             "eids",
        "exMinIV",           "qpreids",           "qpeids",            "qireids",           "qieids",
        "qereids",           "qeeids",            "qcreids",           "qceids",            "geids",
        "greids",            "rbeids",            "rbreids",           "reeids",            "rereids",
        "token")

    UPDATE_KEYS = (
        "timestamp", "lastpokestops", "lastgyms", "lastslocs", "lastspawns", "lastpokemon", "lastnests",
        "lastcommunities", "lastportals", "lastpois", "oSwLat", "oSwLng", "oNeLat", "oNeLng", "token"
    )

    EXCLUDED_IDS_KEYS = ("reids", "eids", "exMinIV", "qpeids", "qieids", "geids", "rbeids", "reeids")

    timestamp: int
    pokemon: bool = True
    loadOverviewStats: bool = False
    loadPokemonStats: bool = False
    loadRewardStats: bool = False
    loadShinyStats: bool = False
    lastpokemon: bool
    pokestops: bool = True
    lures: bool = False
    rocket: bool = False
    quests: bool = False
    dustamount: int = 1
    reloaddustamount: bool
    nestavg: int = 5
    nests: bool = False
    lastnests: bool
    communities: bool = False
    lastcommunities: bool
    portals: bool = False
    pois: bool = False
    lastpois: bool
    newportals: int = 0
    lastportals: bool
    lastpokestops: bool
    gyms: bool = True
    raids: bool = True
    lastgyms: bool
    exEligible: bool = False
    lastslocs: bool
    spawnpoints: bool = False
    lastspawns: bool
    scanlocations: bool = True
    minIV: int = 0
    prevMinIV: str = ""
    minLevel: int = 0
    prevMinLevel: str = ""
    bigKarp: bool = False
    tinyRat: bool = False
    despawnTimeType: int = 0
    pokemonGender: int = 0
    swLat: float = config.sw_lat
    swLng: float = config.sw_lng
    neLat: float = config.ne_lat
    neLng: float = config.ne_lng
    oSwLat: float
    oSwLng: float
    oNeLat: float
    oNeLng: float
    reids: str = ""
    eids: str = ""
    exMinIV: str = ""
    qpreids: str = ""
    qpeids: str = ""
    qireids: str = ""
    qieids: str = ""
    qereids: str = ""
    qeeids: str = ""
    qcreids: str = ""
    qceids: str = ""
    geids: str = ""
    greids: str = ""
    rbeids: str = ""
    rbreids: str = ""
    reeids: str = "0"
    rereids: str = ""
    token: str = ""

    def __init__(self):
        self._spawnpoint_delay = random.randint(10, 20)

    @property
    def form(self):
        data = []
        assert self.token and self.timestamp
        if self._spawnpoint_delay > 0:
            self._spawnpoint_delay -= 1
        else:
            self.spawnpoints = True

        # TODO: lastlocs is not positioned correctly and *reids are not being included in form_data cuz they are None
        for k in PMSFClientRawData.RAW_DATA_PAYLOAD_KEYS:
            if (v := getattr(self, k, None)) is not None:
                if isinstance(v, bool):
                    data.append((k, str(v).lower()))
                else:
                    data.append((k, v))
        return data

    def update(self, data):
        for k in PMSFClientRawData.UPDATE_KEYS:
            setattr(self, k, data.get(k, None))

        for k in PMSFClientRawData.EXCLUDED_IDS_KEYS:
            setattr(self, k, 0)

        if self.pokemon:
            self.prevMinIV = data["preMinIV"]
            self.prevMinLevel = data["preMinLevel"]
        self.reloaddustamount = False


class PMSFClient(commands.Cog):
    """
    Represents the connection and maintains data cache from the map.
    """

    STRERR_CHANNEL_ID = 688497874862211083
    NEW_STOPS_GYMS_CHANNEL_ID = 689561547281924168
    STATUS_CHANNEL_ID = 696468002673000448
    # STATUS_CHANNEL_ID = 688497874862211083

    def __init__(self, bot) -> None:
        self._bot: PMSFBot = bot

        # self._logged_in: bool = False
        self._dirty_count = {}
        # self._new_count = {"pokemon": 0}
        self._stderr_channel: Optional[discord.TextChannel] = None

        self._raw_data = PMSFClientRawData()
        self._cache: PMSFClientDataCache = PMSFClientDataCache()
        self._status: PMSFClientStatus = PMSFClientStatus(self._cache)
        self._loop.start()

    @classmethod
    def _get_url(cls, path: str = '') -> str:
        return urlparse.urljoin(config.host, path)

    async def _new_poi(self, poi_type: str, pid: int, name: str, lat: float, lng: float):
        c = self._bot.get_channel(self.NEW_STOPS_GYMS_CHANNEL_ID)
        d = datetime.datetime.utcnow().strftime("%d-%m-%Y")
        await c.send(content=f'{d}.  New {poi_type} {pid} "{name}" found at '
                             f'<https://www.google.com/maps/dir/?api=1&destination={lat},{lng}>')

    @tasks.loop(seconds=2.5)
    async def _loop(self):
        t0 = time.time()

        # if not logged in, wait 1 minute and retry login
        if not self._status.logged_in:
            await asyncio.sleep(60)
            self._loop.restart()

        headers = {"accept": "application/json, text/javascript, */*; q=0.01",
                   "accept-encoding": "gzip, deflate, br",
                   "accept-language": "en-US,en;q=0.9",
                   "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                   "dnt": "1",
                   "origin": self._get_url(),
                   "referer": self._get_url("/vip/"),
                   "sec-fetch-dest": "empty",
                   "sec-fetch-mode": "cors",
                   "sec-fetch-site": "same-origin",
                   "sec-gpc": "1",
                   "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
                                 "Chrome/79.0.3945.130 Safari/537.36",
                   "x-requested-with": "XMLHttpRequest"}

        async with self._bot.session.post(self._get_url("/vip/raw_data"), data=self._raw_data.form,
                                          headers=headers) as resp:
            self._status.http_code = resp.status
            # if bad code, wait 60 seconds
            if self._status.http_code != 200:
                text = await resp.text()
                await self._status.update()
                await asyncio.sleep(60 - (time.time() - t0))
                return

            # update status info
            self._status.fail_time = int(time.time())
            text = await resp.text()
            self._status.total_downloaded += len(text)
            if (time.time() % 60) < 5:
                await self._status.update()

            try:
                data = json.loads(text)
            except json.JSONDecodeError:
                log.error(f'Json decode error from {self._get_url("/vip/raw_data")}')
                return

            self._raw_data.update(data)

        t1 = time.time()
        # with profiled():
        # print(t1, len(data["pokemons"]), len(data["gyms"]), len(data["spawnpoints"]), len(data["pokestops"]))
        if "spawnpoints" in data:
            await self.process_spawnpoints(data["spawnpoints"])
        await self.process_pokestops(data.get("pokestops", []))
        await self.process_gyms(data.get("gyms", []))
        await self.process_pokemon(data.get("pokemons", []))

        t2 = time.time()
        with session_scope(expire_on_commit=False) as s:
            if "spawnpoints" in data and data["spawnpoints"]:
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.spawnpoint.values()))
            if data.get("pokestops", False):
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.rocket_team.values()))
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.pokestop.values()))
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.incident.values()))
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.quest.values()))
            if data.get("gyms", False):
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.gym.values()))
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.raid.values()))
            if data.get("pokemons", False):
                s.add_all(filter(lambda x: inspect(x).modified, self._cache.encounter.values()))
            t3 = time.time()

        t4 = time.time()
        current_time = time.time()
        # remove needs to be precomputed since the dictionary is gonna change as its being deleted
        remove = tuple(eid for eid, p in self._cache.encounter.items() if p.disappear_time < current_time - 5400)
        apply(self._cache.encounter.__delitem__, remove)

        self._status.perf_times["http"].append(t1 - t0)
        self._status.perf_times["process"].append(t2 - t1)
        self._status.perf_times["sqlsession"].append(t3 - t2)
        self._status.perf_times["commit"].append(t4 - t3)

    @_loop.before_loop
    async def _loop_init(self):
        await self._bot.wait_until_ready()
        await self._status.init(self._bot.get_channel(self.STATUS_CHANNEL_ID))
        # self._stderr_channel = self._bot.get_channel(self.STRERR_CHANNEL_ID)
        # await self._status_message.channel.edit(name="status")

        # login via cookies
        headers = {"sec-fetch-dest": "document",
                   "sec-fetch-mode": "navigate",
                   "sec-fetch-site": "none",
                   "sec-fetch-user": "?1",
                   "upgrade-insecure-requests": "1"}
        async with self._bot.session.get(self._get_url(), headers=headers) as resp:
            bs = bs4.BeautifulSoup(await resp.text(), features="lxml")
            csrf_token = bs.find("input", attrs={"name": "_token"})["value"]
            if bs.find_all("a", attrs={"href": self._get_url("/logout")}):
                # there's a logout button, so probably logged in
                assert bs.find("meta", attrs={"name": "csrf-token"})["content"] == csrf_token
                self._status.logged_in = True

        # if login via cookies failed, try password
        await asyncio.sleep(1)
        if not self._status.logged_in:
            log.debug("Typing in username and password")
            await asyncio.sleep(random.randint(5, 7))
            payload = {"_token": csrf_token, "username": config.username, "password": config.password,
                       "remember": "1"}
            headers = {"cache-control": "max-age=0", "referer": self._get_url("/login"),
                       "upgrade-insecure-requests": "1"}
            async with self._bot.session.post(self._get_url("/login"), data=payload, headers=headers) as resp:
                bs = bs4.BeautifulSoup(await resp.text(), features="lxml")
                if not(bs.find_all("div", attrs={"class": "alert"})):
                    self._status.logged_in = True

        if not self._status.logged_in:
            await self._status.failed_login()
            # TODO proper error handling here
            raise

        # if successful login, save cookies and get the token
        self._bot.session.cookie_jar.save("resources/cookies")
        headers = {"connection": "keep-alive", "upgrade-insecure-requests": "1"}
        async with self._bot.session.get(self._get_url("/map"), headers=headers) as resp:
            bs = bs4.BeautifulSoup(await resp.text(), features="lxml")
            for script_tag in bs.find_all("script", attrs={"src": None}):
                if m_token := re.search(r"var token = '(\S{44})';", script_tag.string):
                    self._raw_data.token = m_token.group(1)
                if m_timestamp := re.search(r"var timestamp = (\d+);", script_tag.string):
                    self._raw_data.timestamp = m_timestamp.group(1)

        if len(self._raw_data.token) == 44:
            await self._status.success_login()
        else:
            # TODO proper error handling here
            raise

        self._status.fail_time = int(time.time())
        self._cache.init()
        await asyncio.gather(asyncio.sleep(2), self._cache.update_nearby())
        self.loop_count = 0

    @_loop.after_loop
    async def _loop_teardown(self) -> None:
        if self._loop.is_being_cancelled():
            log.info(f"Repeated task for {self.__class__.__name__} cancelled.")
        # await self._status_message.channel.edit(name="status-offline")

    def cog_unload(self):
        self._loop.cancel()

    async def close(self):
        log.info("Closing PMSFClient")
        self._status.logged_in = False
        self._loop.cancel()

    async def process_pokemon(self, pokemon: List[dict]) -> None:
        non_stat_keys = ("dex_no", "form", "form_id", "costume", "gender")
        stat_keys = ("weight", "height", "atk_iv", "def_iv", "sta_iv", "move_1", "move_2", "cp", "level")
        all_keys = (("encounter_id", "disappear_time", "discovered_time", "last_updated_time") + non_stat_keys +
                    ("weather_boosted", "expire_timestamp_verified", "spawn_id", "latitude", "longitude") + stat_keys)

        def display_time(t):
            return datetime.datetime.fromtimestamp(t).strftime("%I:%M:%S")

        # count = len(self._cache.encounter)
        for p in filter(None, map(PokemonEncounters.from_form_data, pokemon)):
            # # append joined data to p
            # try:
            #     p.dex = Pokedex.search(dex_no=p.dex_no, form_id=p.form_id)
            # except KeyError:
            #     await self._stderr_channel.send(f"Unable to find form id {p.form_id} for #{p.dex_no}")
            #     p.dex = Pokedex.search(dex_no=p.dex_no, form=p.form)

            if p.spawn_id in self._cache.spawnpoint:
                p.spawnpoint = self._cache.spawnpoint[p.spawn_id]
                # if p.latitude != p.spawnpoint.latitude or p.longitude != p.spawnpoint.longitude:
                #     log.warn(f"PokemonEncounter {p.encounter_id} has spawnpoint location mismatch")
            elif p.spawn_id is not None:
                log.warn(f"PokemonEncounter has spawn_id {p.spawn_id} which is not in Spawnpoints table")
            else:
                p.nearby_pokestop_id = self._cache.nearby.get((float(p.latitude), float(p.longitude)), None)

            # insert check
            if (cached_p := self._cache.encounter.get(p.encounter_id, None)) is None:
                if p.spawn_id is None and p.nearby_pokestop_id is None:
                    # print(f"can't find nearby pokestop for {p}")
                    pass
                log.debug(f"New Pokemon {p.dex.name} found")
                self._cache.encounter[p.encounter_id] = p
                continue

            continue

            # this is all actions to check previous data with current data

            # debug compare message
            mismatch = []
            content = tuple(monospace_tabular([(k, getattr(cached_p, k), getattr(p, k)) for k in all_keys],
                                              title_row=["Field", "Cached", "New"],
                                              message="PokemonEncounter attribute mismatch.  New data will "
                                                      "clobber old."))[0]

            # if spawnpoint and IV data are not available, then assume pokemon identity is unreliable; always clobber
            if cached_p.spawn_id is None and p.spawn_id is None and cached_p.level is None and p.level is None:
                keys = non_stat_keys + ("weather_boosted", "disappear_time")
                if any(getattr(cached_p, k) != getattr(p, k) for k in keys):
                    apply(lambda k: setattr(cached_p, k, getattr(p, k)), keys + ("last_updated_time",))
            # if ditto is the old pokemon, ignore new data's attempt to overwrite
            elif cached_p.dex_no == 132:
                pass
            # if ditto is the new pokemon, trust source to compute ditto and always clobber
            elif p.dex_no == 132:
                apply(lambda k: setattr(cached_p, k, getattr(p, k)),
                      non_stat_keys + ("weather_boosted", "last_updated_time") + stat_keys)
            # if an updated scan occurs on different hours, then clobber
            elif cached_p.discovered_time < p.discovered_time // 3600 * 3600:
                for k in non_stat_keys + ("weather_boosted", "last_updated_time") + stat_keys:
                    set_if_different(cached_p, k, getattr(p, k))
            # if the only change is that form went from default to normal, then clobber
            elif (cached_p.form == "" and p.form == "Normal" and
                  all(getattr(cached_p, k) == getattr(p, k) for k in ("dex_no", "form_id", "costume", "gender"))):
                if cached_p.level is None and p.level is not None:
                    apply(lambda k: setattr(cached_p, k, getattr(p, k)), stat_keys + ("last_updated_time", "form"))
                elif all(getattr(cached_p, k) == getattr(p, k) for k in stat_keys):
                    cached_p.last_updated_time = p.last_updated_time
                    cached_p.form = p.form
            else:
                if any(getattr(cached_p, k) != getattr(p, k) for k in non_stat_keys):
                    mismatch.append("non")
                if (cached_p.weather_boosted != p.weather_boosted and
                        cached_p.last_updated_time < p.last_updated_time // 3600 * 3600):
                    mismatch.append("weather")
                apply(lambda k: setattr(cached_p, k, getattr(p, k)), non_stat_keys)
                if p.level is not None:
                    if cached_p.level is not None and any(getattr(cached_p, k) != getattr(p, k) for k in stat_keys):
                        mismatch.append("stat")
                    apply(lambda k: setattr(cached_p, k, getattr(p, k)), stat_keys)

            if cached_p.spawn_id is None and p.spawn_id is not None:
                cached_p.expire_timestamp_verified = p.expire_timestamp_verified
                cached_p.latitude = p.latitude
                cached_p.longitude = p.longitude
                if (p.disappear_time % 3600) == p.spawnpoint.despawn_time != (cached_p.disappear_time % 3600):
                    cached_p.disappear_time = p.disappear_time
            elif cached_p.spawn_id is None and p.spawn_id is None:
                if (cached_p.nearby_pokestop_id is not None and p.nearby_pokestop_id is not None and
                        cached_p.nearby_pokestop_id != p.nearby_pokestop_id):
                    cached_stop = self._pokestop_cache[cached_p.nearby_pokestop_id]
                    stop = self._pokestop_cache[p.nearby_pokestop_id]
                    d = (abs(float(cached_stop.latitude) - float(stop.latitude)) +
                         abs(float(cached_stop.longitude) - float(stop.longitude))) * 111195
                    if d > 140:
                        log.warn(f"PokemonEncounter has nearby_pokestop mismatch:\n"
                                    f"\tOld LatLng: {cached_p.latitude}, {cached_p.longitude}\n"
                                    f"\tOld Pokestop: {cached_stop}\n"
                                    f"\tNew LatLng: {p.latitude}, {p.longitude}\n"
                                    f"\tNew Pokestop: {stop}\n"
                                    f"\tPokestop Distance: {d}")
                    # cached_p.latitude = p.latitude
                    # cached_p.longitude = p.longitude
            elif not p.same_s2_cell(cached_p):
                cached_p.latitude = p.latitude
                cached_p.longitude = p.longitude
                if p.spawnpoint is not None:
                    mismatch.append("latlng")

            if p.spawnpoint is not None:
                pass

            # only columns left are disappear_time, discovered_time, last_updated_time, expire_timestamp_verified,
            # spawn_id, nearby

            mismatch_2 = False
            warn = ""
            if p.disappear_time != cached_p.disappear_time:
                if p.expire_timestamp_verified == cached_p.expire_timestamp_verified == 0:
                    # if abs(p.disappear_time - cached_p.disappear_time) >= 600:
                    #     warn = (f"Pokemon encounter {p.encounter_id} update has large unverified timestamp mismatch.\n"
                    #             f"\tOld: {cached_p.disappear_time}\tNew: {p.disappear_time}"
                    #             f"\tDelta: {p.disappear_time - cached_p.disappear_time}\n")
                    #     mismatch = True
                    cached_p.disappear_time = p.disappear_time
                elif p.expire_timestamp_verified == 0 and cached_p.expire_timestamp_verified == 1:
                    warn = (f"Pokemon encounter {p.encounter_id} update is unverified timestamp.\n"
                            f"\tOld disappear_time: {display_time(cached_p.disappear_time)}"
                            f"\tNew disappear_time: {display_time(p.disappear_time)}"
                            f"\tDelta: {p.disappear_time - cached_p.disappear_time}\n"
                            f"\tOld discovered_time: {display_time(cached_p.discovered_time)}"
                            f"\tNew discovered_time: {display_time(p.discovered_time)}"
                            f"\tDelta: {p.discovered_time - cached_p.discovered_time}\n"
                            f"\tOld time since discovery: {cached_p.disappear_time - cached_p.discovered_time}"
                            f"\tNew time since discovery: {p.disappear_time - p.discovered_time}\n"
                            f"\tOld spawnpoint: {cached_p.spawnpoint if cached_p.spawn_id else None}\n"
                            f"\tNew spawnpoint: {self._spawnpoint_cache.get(p.spawn_id, None)}\n")
                    mismatch_2 = True
                elif p.expire_timestamp_verified == cached_p.expire_timestamp_verified == 1:
                    if (p.disappear_time % 3600) == p.spawnpoint.despawn_time != (cached_p.disappear_time % 3600):
                        cached_p.disappear_time = p.disappear_time
                    else:
                        warn = (f"Pokemon encounter {p.encounter_id} update has verified timestamp mismatch.\n"
                                f"\tOld disappear_time: {display_time(cached_p.disappear_time)}"
                                f"\tNew disappear_time: {display_time(p.disappear_time)}"
                                f"\tDelta: {p.disappear_time - cached_p.disappear_time}\n"
                                f"\tOld discovered_time: {display_time(cached_p.discovered_time)}"
                                f"\tNew discovered_time: {display_time(p.discovered_time)}"
                                f"\tDelta: {p.discovered_time - cached_p.discovered_time}\n"
                                f"\tOld time since discovery: {cached_p.disappear_time - cached_p.discovered_time}"
                                f"\tNew time since discovery: {p.disappear_time - p.discovered_time}\n"
                                f"\tOld spawnpoint: {cached_p.spawnpoint if cached_p.spawn_id else None}\n"
                                f"\tNew spawnpoint: {self._spawnpoint_cache.get(p.spawn_id, None)}\n")
                        mismatch_2 = True

            if mismatch_2:
                warn += "\tMismatch fields: " + ', '.join(k for k in stat_keys + non_stat_keys
                                                          if getattr(p, k) != getattr(cached_p, k))
                log.warn(warn)

            if mismatch:
                print(p.encounter_id, ' '.join(mismatch))
                cached_p.last_updated_time = p.last_updated_time
                await self._stderr_channel.send(content=content)

        # self._new_count["pokemon"] = self._new_count.get("pokemon", 0) + len(self._encounter_cache) - count
        # log.debug(f"{len(pokemon)} received, {self._new_count['pokemon']} new Pokemon found")

    async def process_gyms(self, gyms: List[dict]) -> None:
        insert_gym_count = 0
        insert_raid_count = 0
        modified_count = 0
        for g in gyms:
            # old version keys
            assert all(k not in g for k in ("form",))

            assert type(g["pokemon"]) is list and len(g["pokemon"]) == 0
            gym_state = "stale"
            if g["gym_id"] not in self._cache.gym:
                gym_state = "new"
                self._cache.gym[g["gym_id"]] = Gyms(gym_id=g["gym_id"])
                await self._new_poi("Gym", g["gym_id"], g["name"], g["latitude"], g["longitude"])
                insert_gym_count += 1
                Gyms.set_cache_dirty()
            gym = self._cache.gym[g["gym_id"]]
            if gym_state != "new" and gym.last_scanned < g["last_scanned"] // 1000:
                gym_state = "updated"

            # if gym._sa_instance_state.modified:
            #     modified_count += 1
            gym.latitude = str(g["latitude"])
            gym.longitude = str(g["longitude"])
            gym.s2_cellid = str(get_s2_cellid(g["latitude"], g["longitude"], 17).id())
            gym.name = g["name"] if gym.name is None else gym.name
            gym.url = g["url"]
            gym.last_modified = g["last_modified"] // 1000
            gym.team_id = g["team_id"]
            gym.slots_available = g["slots_available"]
            gym.last_scanned = g["last_scanned"] // 1000
            gym.ex_gym = g["park"]
            gym.sponsor = g.get("sponsor", None)

            # set_if_different(gym, "latitude", str(g["latitude"]))
            # set_if_different(gym, "longitude", str(g["longitude"]))
            # set_if_different(gym, "s2_cellid", str(get_s2_cellid(g["latitude"], g["longitude"], 17).id()))
            # set_if_different(gym, "name", g["name"] if gym.name is None else gym.name)
            # set_if_different(gym, "url", g["url"])
            # set_if_different(gym, "last_modified", g["last_modified"] // 1000)
            # set_if_different(gym, "team_id", g["team_id"])
            # set_if_different(gym, "slots_available", g["slots_available"])
            # set_if_different(gym, "last_scanned", g["last_scanned"] // 1000)
            # set_if_different(gym, "ex_gym", g["park"])
            # set_if_different(gym, "sponsor", g["sponsor"])
            if gym_state == "new":
                # TODO announce in discord
                log.info(f"New Gym {gym.name} found")

            raid = _verify_raid(g)
            if raid is not None:
                cached_raid = self._cache.raid.get(raid.gym_id, None)
                if cached_raid is None or raid.start_time - cached_raid.start_time > 45*60:
                    insert_raid_count += 1
                    log.debug(f"New Raid at {gym.name} found")
                    self._cache.raid[raid.gym_id] = raid
                elif gym_state == "updated":
                    log.debug(f"Raid at {gym.name} updated")
                    for k in ("start_time", "end_time", "pokemon_id", "pokemon_form", "level", "pokemon_move_1",
                              "pokemon_move_2", "pokemon_gender"):
                        setattr(cached_raid, k, getattr(raid, k))

        # print(modified_count)
        log.debug(f"{len(gyms)} Gyms received, {insert_gym_count} new Gyms found, "
                     f"{insert_raid_count} new Raids found")

    async def process_spawnpoints(self, spawnpoints: List[dict]) -> None:
        keys = ("spawnpoint_id", "latitude", "longitude", "despawn_sec", "time")

        insert_spawn_count = 0
        for sp in spawnpoints:
            assert (sid := int(sp["spawnpoint_id"])) < 2**63
            assert ((sp["despawn_sec"] is None and sp["time"] == 0) or int(sp["despawn_sec"]) == sp["time"])
            assert "duration" not in sp

            if spawnpoint := self._cache.spawnpoint.get(sid, None):
                assert spawnpoint.latitude == str(sp["latitude"]) and spawnpoint.longitude == str(sp["longitude"])
            else:
                spawnpoint = Spawnpoints(spawnpoint_id=sid,
                                         latitude=str(sp["latitude"]),
                                         longitude=str(sp["longitude"]),
                                         s2_cellid=str(get_s2_cellid(sp['latitude'], sp['longitude'], 20).id()))
                self._cache.spawnpoint[sid] = spawnpoint
                log.debug(f"New spawnpoint found at {spawnpoint.latitude}, {spawnpoint.longitude} with cell id "
                          f"{spawnpoint.s2_cellid} that ends at {sp['time']}")

            set_if_different(spawnpoint, "despawn_time", None if sp["despawn_sec"] is None else sp["time"])

        log.debug(f"{len(spawnpoints)} Spawnpoints received, {insert_spawn_count} new Spawnpoints found")

    async def process_pokestops(self, pokestops: List[dict]) -> None:
        sql_quest_keys = ("quest_type", "quantity", "args", "reward_stardust", "reward_item_id",
                          "reward_item_quantity", "reward_dex")

        stop_keys = {'pokestop_id', 'latitude', 'longitude', 'pokestop_name', 'url', 'last_seen',
                     'lure_expiration', 'lure_id'}
        incident_keys = {'incident_expiration', 'grunt_type', 'grunt_type_name', 'grunt_type_gender', 'encounters',
                         'second_reward'}
        quest_keys = {'quest_type', 'quest_timestamp', 'quest_target', 'quest_rewards', 'reward_item_id',
                      'quest_condition_type', 'quest_condition_type_1', 'quest_condition_info', 'quest_reward_type'}
        reward_keys = {'reward_amount', 'reward_pokemon_id', 'reward_pokemon_formid', 'reward_pokemon_costumeid',
                       'reward_pokemon_genderid', 'reward_pokemon_shiny', 'reward_pokemon_name', 'reward_item_name'}

        def _verify_reward(_q: Quests, _stop: dict):
            reward_info = None
            for r in json.loads(_stop["quest_rewards"]):
                assert set(r.keys()) == {"info", "type"}, f"bad quest_rewards in {_stop}"
                if r["type"] == _stop["quest_reward_type"]:
                    reward_info = r["info"]
            assert reward_info is not None

            if "amount" in reward_info:
                assert reward_info["amount"] == _stop["reward_amount"], f"bad reward_info amount in {_stop}"
                del reward_info["amount"]
            if "pokemon_id" in reward_info:
                assert reward_info["pokemon_id"] == _stop["reward_pokemon_id"], f"bad reward_info pokemon_id in {_stop}"
                del reward_info["pokemon_id"]
            if "form_id" in reward_info:
                assert reward_info["form_id"] == _stop["reward_pokemon_formid"], f"bad reward_info form_id in {_stop}"
                assert reward_info["costume_id"] == _stop["reward_pokemon_costumeid"], \
                    f"bad reward_info costume_id in {_stop}"
                assert reward_info["gender_id"] == _stop["reward_pokemon_genderid"], f"bad reward_info gender_id in {_stop}"
                assert reward_info["shiny"] == _stop["reward_pokemon_shiny"], f"bad reward_info shiny in {_stop}"
                del (reward_info["form_id"], reward_info["costume_id"],
                     reward_info["gender_id"], reward_info["shiny"])
            if "item_id" in reward_info:
                assert reward_info["item_id"] == _stop["reward_item_id"], f"bad reward_info item_id in {_stop}"
                del reward_info["item_id"]
            assert not reward_info, f"Extra reward_info data {reward_info}"

            _q.reward_dex = None
            match _stop['quest_reward_type']:
                case 2:
                    item = Items.get_or_none(_stop["reward_item_id"])
                    assert item is not None, f"Unknown item ID {_stop['reward_item_id']}"
                    assert (item.name == _stop["reward_item_name"] or
                            item.name.replace("'", "") == _stop["reward_item_name"]), \
                        f"Bad item name in reward data {_stop}"
                    _q.reward_item_id = item.item_id
                    _q.reward_item_quantity = _stop["reward_amount"]
                case 3:
                    _q.reward_stardust = _stop["reward_amount"]
                case 4:
                    # Pokemon candy reward
                    assert False, f"Pokemon candy reward quest not implemented"
                case 7:
                    try:
                        dex: Pokedex = Pokedex.search(dex_no=_stop["reward_pokemon_id"],
                                                      form_id=_stop["reward_pokemon_formid"])
                    except KeyError as e:
                        assert False, str(e)

                    _q.reward_dex = dex
                    # costume_id = reward_info["costume_id"]
                    # gender_id = reward_info["gender_id"]
                    # shiny = reward_info["shiny"]
                    assert (dex.name == _stop["reward_pokemon_name"] or
                            dex.name[:-1] == _stop["reward_pokemon_name"]), \
                        f"Bad dex name in reward data {_stop}"
                    assert _stop["reward_pokemon_formid"] == dex.form_id, f"Bad form id in reward data {_stop}"
                case 12:
                    # mega energy, reward_amount is the amount of energy
                    try:
                        dex = Pokedex.search(dex_no=_stop["reward_pokemon_id"], form="")
                    except KeyError as _e:
                        assert False, str(_e)

                    _q.reward_dex = dex
                    _q.reward_energy_amount = _stop["reward_amount"]
                case _:
                    assert False, f"Unknown quest reward type {_stop['quest_reward_type']}"


        bool_new_stop = False
        for p in pokestops:
            assert set(p) == stop_keys | incident_keys | quest_keys | reward_keys

            sid = p["pokestop_id"]
            if sid not in self._cache.pokestop:
                self._cache.pokestop[sid] = Pokestops(pokestop_id=sid, last_scanned=0)
                await self._new_poi("Pokestop", sid, p["pokestop_name"], p["latitude"], p["longitude"])
                bool_new_stop = True

            # update data
            stop = self._cache.pokestop[sid]
            if stop.last_scanned < p["last_seen"] // 1000:
                stop.latitude = str(p["latitude"])
                stop.longitude = str(p["longitude"])
                stop.s2_cellid = str(get_s2_cellid(p["latitude"], p["longitude"], 17).id())
                stop.name = p["pokestop_name"]
                stop.url = p["url"]
                stop.last_scanned = p["last_seen"] // 1000

            if p["incident_expiration"]:
                rtid = int(p["grunt_type"])
                if rtid not in self._cache.rocket_team:
                    self._cache.rocket_team[rtid] = RocketTeams(team_id=rtid)
                rocket_team = self._cache.rocket_team[rtid]
                rocket_team.type = ROCKET_TEAM_TYPE.get(p["grunt_type_name"], p["grunt_type_name"])
                if rocket_team.type in ["Cliff", "Arlo", "Giovanni"]:
                    rocket_team.gender = 0
                elif rocket_team.type == "Sierra":
                    rocket_team.gender = 1
                else:
                    rocket_team.gender = {"Male": 0, "Female": 1, "": 2}[p["grunt_type_gender"]]
                if rocket_team.type == "":
                    rocket_team.type = "Snorlax" if rocket_team.gender == 1 else "Kanto"
                if 500 <= rtid < 600:
                    # event pvp trainers
                    rocket_team.pokemon_first = rocket_team.pokemon_second = rocket_team.pokemon_third = ""
                    rocket_team.second_reward = 0
                # rocket_team.second_reward = {"false": 0, "true": 1}[p["second_reward"]]
                # rocket_team.pokemon_first = ','.join(dex.split('_')[0] for dex in p["encounters"]["first"])
                # try:
                #     rocket_team.pokemon_second = ','.join(dex.split('_')[0] for dex in p["encounters"]["second"])
                # except KeyError:
                #     rocket_team.pokemon_second = ""
                # try:
                #     rocket_team.pokemon_third = ','.join(dex.split('_')[0] for dex in p["encounters"]["third"])
                # except KeyError:
                #     rocket_team.pokemon_third = ""

                incident = Incidents(pokestop=stop, end_time=int(p["incident_expiration"] // 1000),
                                     rocket_team=rocket_team)
                cached_incident = self._cache.incident.get(sid, None)
                if cached_incident is None or incident.end_time - cached_incident.end_time > 30 * 60:
                    log.debug(f"New Incident at {stop.name} found")
                    self._cache.incident[sid] = incident
            else:
                assert all(not p[k] for k in incident_keys)

            if p["quest_type"] != 0:
                # assert p["quest_dust_amount"] == p["reward_amount"]
                # del p["quest_dust_amount"]

                quest = Quests(pokestop=stop, timestamp=int(p["quest_timestamp"]), quantity=p["quest_target"])

                try:
                    _verify_reward(quest, p)
                except AssertionError as e:
                    await self._stderr_channel.send(content=str(e))

                try:
                    quest.quest_type, args = self.process_quest_type(p)
                    quest.args = ','.join('='.join(map(str, item)) for item in args.items())
                except UnknownQuestError as e:
                    await self._stderr_channel.send(content=str(e))
                except AssertionError as e:
                    tb = e.__traceback__
                    stack_str = ''.join(traceback.StackSummary.from_list(traceback.extract_tb(tb)).format())
                    await self._stderr_channel.send(content=f"Bad quest type info in {p}")

                cached_quest = self._cache.quest.get(sid, None)
                if (cached_quest is None or midnight(quest.timestamp) > cached_quest.timestamp or
                        any(getattr(quest, k) != getattr(cached_quest, k) for k in sql_quest_keys)):
                    log.info(f"New Quest at {stop.name} found")
                    self._cache.quest[sid] = quest
            else:
                assert all(not p[k] for k in (quest_keys | reward_keys))

        if bool_new_stop:
            await self._cache.update_nearby()
            Pokestops.set_cache_dirty()

    def process_quest_type(self, p: dict) -> Optional[Tuple[QuestTypes, dict]]:
        info = {} if p["quest_condition_info"] is None else json.loads(p["quest_condition_info"])
        quest_type = (p["quest_type"], p["quest_condition_type"], p["quest_condition_type_1"])
        try:
            quest = self._cache.quest_type[quest_type]
        except KeyError:
            raise UnknownQuestError(f"unknown quest type {quest_type} with data {p}")
        args = {}

        # 1: {"prototext": "QUEST_FIRST_CATCH_OF_THE_DAY", "text": "First catch of the day"}
        # 2: {"prototext": "QUEST_FIRST_POKESTOP_OF_THE_DAY", "text": "First pokéstop of the day"}
        # 3: {"prototext": "QUEST_MULTI_PART", "text": "Multi part"}
        # 9: {"prototext": "QUEST_COMPLETE_QUEST", "text": "Complete {0} quest(s)"}
        # 11: {"prototext": "QUEST_FAVORITE_POKEMON", "text": "Favorite {0} pokémon"}
        # 12: {"prototext": "QUEST_AUTOCOMPLETE", "text": "Autocomplete"}
        # 18: {"prototext": "QUEST_BADGE_RANK", "text": "Collect {0} badge(s)"}
        # 19: {"prototext": "QUEST_PLAYER_LEVEL", "text": "Become level {0}"}
        # 20: {"prototext": "QUEST_JOIN_RAID", "text": "Join {0} raid(s)"}
        # 21: {"prototext": "QUEST_COMPLETE_BATTLE", "text": "Complete {0} raid battle(s)"}
        # 25: {"prototext": "QUEST_EVOLVE_INTO_POKEMON", "text": "Evolve {0} pokémon into"}
        # 30: {"prototext": "QUEST_PURIFY_POKEMON", "text": "Purify {0} pokémon"}
        # 31: {"prototext": "QUEST_FIND_TEAM_ROCKET", "text": "Find Team Rocket {0} times"}
        # 32: {"prototext": "QUEST_FIRST_GRUNT_OF_THE_DAY", "text": "First Grunt of the day"}
        # 33: {"prototext": "QUEST_BUDDY_FEED", "text": ""}
        # 35: {"prototext": "QUEST_BUDDY_PET", "text": ""}
        # 36: {"prototext": "QUEST_BUDDY_LEVEL", "text": ""}
        # 37: {"prototext": "QUEST_BUDDY_WALK", "text": ""}
        # 38: {"prototext": "QUEST_BUDDY_YATTA", "text": ""}

        # https://raw.githubusercontent.com/pmsf/PMSF/main/static/data/conditiontype.json

        match quest_type:
            case (0, 0, 0):
                pass
            case (4, 0, 0):
                # "Catch {num} Pokémon."
                assert not info
            case (4, 1, 0):
                # "Catch {num} {type_list}type Pokémon."
                args["type_list"] = ' '.join(map(lambda x: str(x - 1), info["pokemon_type_ids"]))
                del info["pokemon_type_ids"]
                assert not info
            case (4, 1, 21):
                # "Catch {num} different species of {type_list}type Pokémon."
                args["type_list"] = ' '.join(map(lambda x: str(x - 1), info["pokemon_type_ids"]))
                del info["pokemon_type_ids"]
                assert not info
            case (4, 2, 0):
                # "Catch {num} {poke_list}."
                args["poke_list"] = ' '.join(map(str, info["pokemon_ids"]))
                del info["pokemon_ids"]
                assert not info
            case (4, 3, 0) | (4, 21, 0):
                # "Catch {num} Pokémon with weather boost."
                # "Catch {num} different species of Pokémon."
                assert not info
            case (4, 26, 0):
                # "Catch {num} shadow Pokémon."
                assert info["alignment_ids"] == [1]
                del info["alignment_ids"]
                assert not info
            case (5, 0, 0) | (5, 12, 0):
                # "Spin {num} Pokéstop(s) or Gym(s)."
                # "Spin {num} Pokéstop(s) you haven't visited before."
                assert not info
            case (6, 0, 0):
                # "Hatch {num} egg(s)."
                assert not info
            case (7, 0, 0) | (7, 9, 0) | (7, 10, 0):
                # "Battle in a gym {num} time(s)."
                # "Win {num} gym battle(s)."
                # "Use a super effective charged attack in {num} gym battle(s)."
                assert not info
            case (8, 0, 0) | (8, 6, 0) | (8, 6, 6):
                # "Battle in {num} raid(s)."
                # "Win {num} raid(s)."
                # "Win {num} raid(s)."
                assert not info
            case (8, 6, 44):
                # "Win a raid in under {time} seconds."
                args["time"] = info["time"]
                del info["time"]
                assert not info
            case (8, 7, 6):
                # "Win {num} level 3 or higher raid(s)."
                assert info["raid_levels"] in ([3, 4, 5], [6], [3, 4, 5, 6])
                del info["raid_levels"]
                assert not info
            case (10, 0, 0):
                # "Transfer {num} Pokémon."
                assert not info
            case (13, 0, 0):
                # "Use {num} berr(ies) to help catch Pokémon."
                assert not info
            case (13, 11, 0):
                # "Use {num} Golden Razz berr(ies) to help catch Pokémon."
                pass
            case (14, 0, 0):
                # "Power up Pokémon {num} time(s)."
                assert not info
            case (15, 0, 0):
                # "Evolve {num} Pokémon."
                assert not info
            case (15, 1, 0):
                # "Evolve {num} {type_list}type Pokémon."
                args["type_list"] = ' '.join(map(lambda x: str(x - 1), info["pokemon_type_ids"]))
                del info["pokemon_type_ids"]
                assert not info
            # case (15, 11, 0):
            #     pass
            case (15, 2, 0):
                args["poke_list"] = ' '.join(map(str, info["pokemon_ids"]))
                del info["pokemon_ids"]
                assert not info
            case (16, 8, 0) | (16, 8, 15) | (16, 14, 0):
                # "Make {num} {throw_type} throw(s)."
                # "Make {num} {throw_type} curveball throw(s)."
                # "Make {num} {throw_type} throw(s) in a row."
                args["throw_type"] = info["throw_type_id"]
                assert not info["hit"]
                del info["throw_type_id"]
                del info["hit"]
                assert not info
            case (16, 14, 15):
                # "Make {num} {throw_type} curveball throw(s) in a row."
                if "throw_type_id" in info:
                    args["throw_type"] = info["throw_type_id"]
                    del info["throw_type_id"]
                    assert not info["hit"]
                else:
                    args["throw_type"] = 0
                    assert info["hit"]
                del info["hit"]
                assert not info
            case (16, 15, 0):
                # "Make {num} curveball throw(s)."
                assert not info
            case (17, 0, 0):
                # "Earn {num} cand(ies) walking with your buddy."
                assert not info
            case (22, 0, 0):
                # "Make {num} new friend(s)."
                assert not info
            case (23, 0, 0):
                # "Trade {num} Pokémon."
                assert not info
            # case (23, 25, 0):
            #     # "Trade Pokémon caught {:,} km apart."
            #     pass
            case (24, 0, 0) | (24, 46, 0):
                # "Send {num} gift(s) to friend(s)."
                # "Send {num} gift(s) to friend(s) and add a sticker to each."
                assert not info
            case (27, 23, 0):
                # "Battle another trainer {num} time(s)."
                battle_types = {"COMBAT_LEAGUE_DEFAULT_GREAT", "COMBAT_LEAGUE_DEFAULT_ULTRA",
                                "COMBAT_LEAGUE_DEFAULT_MASTER", "COMBAT_LEAGUE_VS_SEEKER_GREAT",
                                "COMBAT_LEAGUE_VS_SEEKER_ULTRA", "COMBAT_LEAGUE_VS_SEEKER_MASTER",
                                "COMBAT_LEAGUE_VS_SEEKER_MASTER_NO_LEGENDARY", "COMBAT_LEAGUE_VS_SEEKER_ULTRA_NO_LEGENDARY",
                                "COMBAT_LEAGUE_VS_SEEKER_GREAT_FLYING", "COMBAT_LEAGUE_VS_SEEKER_GREAT_HALLOWEEN",
                                "COMBAT_LEAGUE_VS_SEEKER_PREMIER"}
                assert set(info["template_ids"]) <= battle_types
                assert "win" in info
                # TODO how does win affect
                del info["template_ids"]
                del info["win"]
                assert not info
            case (27, 41, 0) | (27, 41, 18):
                # "Battle in the GO Battle League."
                # "Win {num} trainer battle(s) in the GO Battle League."
                assert not info
            case (28, 0, 0):
                # "Take {num} snapshot(s) of your buddy."
                assert not info
            case (28, 1, 0):
                # "Take {num} snapshot(s) of {type_list} Pokémon."
                args["type_list"] = ' '.join(map(lambda x: str(x - 1), info["pokemon_type_ids"]))
                del info["pokemon_type_ids"]
                assert not info
            case (28, 2, 0):
                # "Take {num} snapshot(s) of {poke_list}."
                args["poke_list"] = ' '.join(map(str, info["pokemon_ids"]))
                del info["pokemon_ids"]
                assert not info
            case (28, 28, 0):
                # "Take {num} snapshot(s) of your buddy."
                assert info["min_buddy_level"] == 0
                assert not info["must_be_on_map"]
                del info["min_buddy_level"]
                del info["must_be_on_map"]
                assert not info
            case (29, 18, 0):
                # "Defeat {num} Team GO Rocket Grunt(s)."
                # gruntCharacterTypes = ['unset', 'Team Leader(s)', 'Team GO Rocket Grunt(s)', 'Arlo', 'Cliff', 'Sierra',
                # 'Giovanni']
                assert not info
            case (29, 18, 27):
                # "Defeat {num} Team GO Rocket Grunt(s)."
                # gruntCharacterTypes = ['unset', 'Team Leader(s)', 'Team GO Rocket Grunt(s)', 'Arlo', 'Cliff', 'Sierra',
                # 'Giovanni']
                assert info["character_category_ids"] == [2]
                # TODO if character_category_ids is 2, its a grunt, if its [3,4,5] its a leader
                del info["character_category_ids"]
                assert not info
            case (29, 27, 0):
                # "Defeat a Team GO Rocket Leader."
                # gruntCharacterTypes = ['unset', 'Team Leader(s)', 'Team GO Rocket Grunt(s)', 'Arlo', 'Cliff', 'Sierra',
                # 'Giovanni']
                # assert info["character_category_ids"] == [2]
                # del info["character_category_ids"]
                # assert not info
                # TODO info checks
                pass
            case (29, 27, 18):
                # "Defeat {num} Team GO Rocket Grunt(s)."
                # gruntCharacterTypes = ['unset', 'Team Leader(s)', 'Team GO Rocket Grunt(s)', 'Arlo', 'Cliff', 'Sierra',
                # 'Giovanni']
                # assert info["character_category_ids"] == [2]
                # TODO if character_category_ids is 2, its a grunt, if its [3,4,5] its a leader
                del info["character_category_ids"]
                assert not info
            case (30, 0, 0):
                assert not info
            case (33, 0, 0):
                # "Give your buddy {num} treat(s)."
                assert not info
            case (34, 0, 0):
                # "Earn {num} Heart(s) with your Buddy."
                assert not info
            case (35, 0, 0):
                # "Play with your Buddy {num} time(s)."
                assert not info
            case (35, 28, 0):
                # "Play with your Buddy {num} time(s)."
                assert info["min_buddy_level"] == 0
                assert not info["must_be_on_map"]
                del info["min_buddy_level"]
                del info["must_be_on_map"]
                assert not info
            case (36, 28, 0):
                assert not info["must_be_on_map"]
                assert info["min_buddy_level"] == 2
                del info["min_buddy_level"]
                del info["must_be_on_map"]
                assert not info
            case (39, 0, 0):
                assert not info
            case (42, 0, 0):
                # "Walk {num} km."
                assert not info
            case (43, 2, 0):
                # "Mega Evolve a {poke_list}"
                args["poke_list"] = ' '.join(map(str, info["pokemon_ids"]))
                del info["pokemon_ids"]
                assert not info
            case (46, 0, 0) | (46, 42, 0):
                # "AR-scan a Pokestop."
                # "AR-scan a Pokestop."
                assert not info
            case (53, 10, 0):
                # Use {num} super effective charged attacks.
                assert not info
            case (57, 0, 0):
                # Take {num} snapshot(s) of wild Pokémon.
                assert not info
            case (57, 1, 0):
                # Take {num} snapshot(s) of wild {type_list}type Pokémon."
                args["type_list"] = ' '.join(map(lambda x: str(x - 1), info["pokemon_type_ids"]))
                del info["pokemon_type_ids"]
                assert not info
            case (57, 2, 0):
                # "Take a snapshot of {poke_list} in the wild."
                args["poke_list"] = ' '.join(map(str, info["pokemon_ids"]))
                del info["pokemon_ids"]
                assert not info
            case (59, 0, 0):
                # Open {num} gifts.
                assert not info
            case _:
                assert False, f"unknown quest type {quest_type} with data {info}"

        return quest, args


def _verify_raid(gym: dict) -> Optional[Raids]:
    # raid data:
    # ['raid_end', 'raid_start', 'raid_pokemon_id', 'raid_level', 'raid_pokemon_move_1', 'raid_pokemon_move_2',
    # 'raid_pokemon_form', 'raid_pokemon_cp', 'raid_pokemon_name',
    if gym["raid_level"] is not None:
        dex_entry: Optional[Pokedex] = None
        if gym["raid_pokemon_id"] is None:
            assert gym["raid_pokemon_name"] is None
        elif gym["raid_pokemon_evolution"]:
            form = {1: "Mega", 2: "MegaX", 3: "MegaY"}[gym["raid_pokemon_evolution"]]
            dex_entry = Pokedex.search(dex_no=int(gym["raid_pokemon_id"]), form=form)
            assert dex_entry.name == gym["raid_pokemon_name"]
        else:
            try:
                dex_entry = Pokedex.search(dex_no=int(gym["raid_pokemon_id"]),
                                           form_id=gym["raid_pokemon_form"])
            except KeyError:
                form = gym.get("raid_pokemon_form_name", "")
                form = {"Fall": "Fall 2019", "Alolan": "Alola", "Armored": "A", "Defence": "Defense",
                        "5th Anniversary": "Flying 5th Anniv"}.get(form, form)
                dex_entry = Pokedex.search(dex_no=int(gym["raid_pokemon_id"]), form=form)
            if dex_entry.name != gym["raid_pokemon_name"]:
                print(dex_entry.name, gym["raid_pokemon_name"])

        r = Raids(gym_id=gym["gym_id"],
                  start_time=gym["raid_start"] // 1000,
                  end_time=gym["raid_end"] // 1000,
                  # dex=dex_entry,
                  pokemon_id=None if dex_entry is None else dex_entry.dex_no,
                  pokemon_form=None if dex_entry is None else dex_entry.form,
                  level=int(gym["raid_level"]),
                  pokemon_move_1=None if not int(gym["raid_pokemon_move_1"]) else int(gym["raid_pokemon_move_1"]),
                  pokemon_move_2=None if not int(gym["raid_pokemon_move_2"]) else int(gym["raid_pokemon_move_2"]),
                  pokemon_gender=None if not int(gym["raid_pokemon_gender"]) else int(gym["raid_pokemon_gender"]))
        # r.dex = dex_entry
        return r
    else:
        assert gym["raid_start"] == 0
        assert all(gym["raid_pokemon_" + k] is None for k in ("id", "name", "cp", "move_1", "move_2"))
        assert all(gym["raid_pokemon_" + k] == 0 for k in ("form",))

        return None


def setup(bot):
    bot.add_cog(PMSFClient(bot))


def teardown(bot):
    pass
