import asyncio
import datetime
import logging
import time
import pytz
from typing import Optional, List, Iterable, Tuple, Union

from sql.table import Types, Pokedex, Raids, Moves, Gyms
from util.math import midnight
from util.notifier import Notifier, EmbedSet
from sql import session_scope

from discord.ext import commands
from sqlalchemy import select, func
from sqlalchemy.orm import joinedload

GENDER_CODES = [None, "Male", "Female", ""]
TEAMS = {0: "<:item_gym:284615005570072576>", 1: "<:team_mystic:284614278709903371>",
         2: "<:team_valor:284614087336525826>", 3: "<:team_instinct:284614748702638081>"}
EX_PASS = "<:expass:439662479908405258>"

log = logging.getLogger(__name__)


class RaidEmbedSet(EmbedSet):

    def __init__(self, boss: Optional[Tuple[Union[bool, Pokedex], int]]):
        self._last_seen = 0
        super().__init__(boss)
        self._data: List[Raids]
        self._key: Optional[Tuple[Union[bool, Pokedex], int]]

    @property
    def last_seen(self) -> int:
        return self._last_seen

    @property
    def last_seen_pretty(self) -> str:
        return datetime.datetime.fromtimestamp(self._last_seen, tz=pytz.timezone("US/Eastern")).strftime("%H:%M:%S")

    @property
    def expired(self) -> bool:
        return time.time() - 24 * 3600 > self._last_seen

    @property
    def _level(self) -> Optional[int]:
        return self._key[1] if self._key is not None else None

    @property
    def _is_ex_embed(self) -> bool:
        return self._key is None

    @property
    def _is_hatched(self) -> bool:
        return self._key is not None and self._key[0]

    @property
    def _is_unhatched(self) -> bool:
        return self._key is not None and not self._key[0]

    @property
    def _full_pokemon_name(self) -> Optional[str]:
        if isinstance(self._key[0], Pokedex):
            return ((self._key[0].form + ' ') if self._key[0].form not in ("Normal", "") else "") + self._key[0].name

    @property
    def _embed_title(self) -> str:
        if not self._data:
            if self._is_ex_embed:
                return "No raids at potential EX gyms currently ongoing."
            elif isinstance(self._key[0], Pokedex):
                return f"No {self._key[0].name} found."
            elif self._is_unhatched:
                if self._level == 6:
                    return "No unhatched mega eggs found."
                else:
                    return f"No unhatched level {self._level} eggs found."
            else:
                if self._level == 6:
                    return "No unknown hatched mega eggs found."
                else:
                    return f"No unknown hatched level {self._level} eggs found."
        else:
            if self._is_ex_embed:
                return "A raid is occurring at an EX Eligible Gym!"
            elif isinstance(self._key[0], Pokedex):
                return f"{self._full_pokemon_name} (Raid Level {self._level}) was spotted!"
            elif self._is_unhatched:
                if self._level == 6:
                    return "An unhatched mega egg was spotted!"
                else:
                    return f"An unhatched level {self._level} egg was spotted!"
            else:
                if self._level == 6:
                    return "An unknown hatched mega egg was spotted!"
                else:
                    return f"An unknown hatched level {self._level} egg was spotted!"

    @property
    def _embed_description(self) -> str:
        if not self._data:
            if self._is_ex_embed:
                return f"Last EX raid was seen at {self.last_seen_pretty}."
            elif isinstance(self._key[0], Pokedex):
                return f"Last {self._full_pokemon_name} was seen at {self.last_seen_pretty}."
            else:
                return f"Last level {self._level} egg was seen at {self.last_seen_pretty}."
        else:
            if self._is_ex_embed:
                return "The following gyms have a raid happening:"
            else:
                return "Look for it at the following gyms:"

    @property
    def _embed_thumbnail(self) -> Optional[str]:
        if self._is_ex_embed:
            return "https://archives.bulbagarden.net/media/upload/a/ae/GO_Legendary_Raid_Pass.png"
        elif isinstance(self._key[0], Pokedex):
            if self._key[0].thumbnail is None:
                return f"https://serebii.net/pokemon/art/{self._key[0].dex_no}.png"
            return self._key[0].thumbnail
        elif self._is_unhatched:
            return f"https://raw.githubusercontent.com/pmsf/PMSF/main/static/sprites/raid/egg/{self._level}.png"
        else:
            return f"https://raw.githubusercontent.com/pmsf/PMSF/main/static/sprites/raid/egg/{self._level}_h.png"

    @property
    def _embed_colour(self):
        if isinstance(self._key[0], Pokedex) and (result := Types.get_or_none(self._key[0].type_1)):
            return result.color
        return super()._embed_colour

    def field_name(self, raid: Raids) -> str:
        if self._is_ex_embed:
            return f"{TEAMS[raid.gym.team_id]} {raid.gym.name}"
        else:
            return f"{TEAMS[raid.gym.team_id]} {raid.gym.name} {EX_PASS if raid.gym.ex_gym else ''}".rstrip()

    def field_value(self, raid: Raids) -> str:
        try:
            move_1 = Moves.get(raid.pokemon_move_1).name
        except KeyError:
            move_1 = raid.pokemon_move_1
        try:
            move_2 = Moves.get(raid.pokemon_move_2).name
        except KeyError:
            move_2 = raid.pokemon_move_2

        gender = ((GENDER_CODES[raid.pokemon_gender] + " ") if
                  raid.pokemon_gender is not None and 1 <= raid.pokemon_gender <= 2 else "")

        start = datetime.datetime.fromtimestamp(raid.start_time, tz=pytz.timezone("US/Eastern")).strftime("%I:%M:%S")
        end = datetime.datetime.fromtimestamp(raid.end_time, tz=pytz.timezone("US/Eastern")).strftime("%I:%M:%S")
        if self._is_ex_embed and self._is_unhatched:
            return (f"[Directions](https://www.google.com/maps/dir/?api=1&destination="
                    f"{raid.gym.latitude},{raid.gym.longitude}).  "
                    f"A level {self._level} egg hatches at {start} and the pokemon flees at {end}.")
        elif self._is_ex_embed and isinstance(self._key[0], Pokedex):
            return (f"[Directions](https://www.google.com/maps/dir/?api=1&destination="
                    f"{raid.gym.latitude},{raid.gym.longitude}).  "
                    f"A {gender}{self._full_pokemon_name} attacks with {move_1} and {move_2}, and it flees at {end}.")
        elif self._is_ex_embed:
            return (f"[Directions](https://www.google.com/maps/dir/?api=1&destination="
                    f"{raid.gym.latitude},{raid.gym.longitude}).  "
                    f"A hatched level {self._level} egg flees at {end}.")
        elif self._is_unhatched:
            return (f"[Directions](https://www.google.com/maps/dir/?api=1&destination="
                    f"{raid.gym.latitude},{raid.gym.longitude}).  "
                    f"Egg hatches at {start} and the pokemon flees at {end}.")
        elif isinstance(self._key[0], Pokedex):
            return (f"[Directions](https://www.google.com/maps/dir/?api=1&destination="
                    f"{raid.gym.latitude},{raid.gym.longitude}).  "
                    f"A {gender}{self._full_pokemon_name} attacks with {move_1} and {move_2}, and it flees at {end}.")
        else:
            return (f"[Directions](https://www.google.com/maps/dir/?api=1&destination="
                    f"{raid.gym.latitude},{raid.gym.longitude}).  "
                    f"The hatched pokemon flees at {end}.")

    def _prepare_data(self, raids: Iterable[Raids]) -> Optional[list]:
        if set(r.rowid for r in raids) == set(r.rowid for r in self._data):
            return None

        raids = list(raids)
        raids.sort(key=lambda r: r.end_time)
        if raids:
            self._last_seen = raids[-1].end_time
        return [(self.field_name(r), self.field_value(r)) for r in raids]


class RaidNotifier(Notifier):

    _loop_duration = 60
    _per_message_delay = 2
    _EmbedSetClass = RaidEmbedSet
    _name = "raids"

    def __init__(self, bot, cid, level):
        self._level = level
        super().__init__(bot, cid)

    def _fetch_data(self):
        current_time = time.time()
        with session_scope(expire_on_commit=False) as sql:
            bosses = sql.query(Raids).options(joinedload(Raids.dex)).filter(Raids.end_time > current_time - 24*3600,
                                                                            Raids.level == self._level). \
                group_by(Raids.pokemon_id, Raids.pokemon_form).order_by(Raids.pokemon_id).all()

            # TODO joinedload on moves
            raids = sql.query(Raids).options(joinedload(Raids.dex), joinedload(Raids.gym)). \
                filter(Raids.end_time > current_time, Raids.level == self._level).all()

        def unhatched(lvl):
            return

        def hatched(lvl):
            return

        data = [(dex_lvl, tuple(filter(lambda r: r.level == dex_lvl[1] and r.dex is dex_lvl[0], raids)))
                for dex_lvl in filter(lambda x: x[0] is not None, map(lambda r: (r.dex, r.level), bosses))]
        data.append(((True, self._level),
                     tuple(filter(lambda r: (r.start_time <= current_time and r.dex is None), raids))))
        data.append(((False, self._level),
                     tuple(filter(lambda r: r.start_time > current_time, raids))))
        data.sort(key=lambda x: x[0][1])

        return data

    async def _loop_init(self) -> None:
        # to spread out the raid notifiers from not acting simultaneously
        await asyncio.sleep(self._level * 5)
        await super()._loop_init()


class RaidCog(commands.Cog):

    CHANNEL_ID = [(645062627722133504, 1), (811353508866555924, 3), (811353529851183126, 5), (811353543448854618, 6),
                  (971158963804594277, 7)]

    def __init__(self, bot):
        self._bot = bot
        self._notifiers = [RaidNotifier(bot, cid, l) for (cid, l) in self.CHANNEL_ID]

    def cog_unload(self):
        for raid_notify in self._notifiers:
            raid_notify.stop()

    @commands.command()
    async def after_raid_hour(self, ctx: commands.Context, day: str = ""):
        try:
            if not day:
                mid_time = midnight(time.time())
            else:
                mid_time = int(datetime.datetime.strptime(day, "%Y-%m-%d").timestamp())
            start_time = mid_time + 18*3600
        except ValueError:
            await ctx.send(content="Error: could not parse date", delete_after=30)
            return

        # TODO: check for raids that start before 6:00pm in case scraper was wrong.  these raids would have duration
        #  45 minutes if its not a raid hour raid
        query = select([Raids]).where(Raids.start_time > start_time - 30).where(Raids.start_time < start_time + 30)
        gyms = Gyms.copy_cache()
        with session_scope() as sql:
            for raid in sql.execute(query):
                if raid.end_time - raid.start_time < 55*60:
                    # this is not a raid hour raid
                    continue
                try:
                    del gyms[raid.gym_id]
                except KeyError:
                    log.warning("tried to remove a gym that didn't exist in gyms table during after_raid_hour")

            for gym in gyms.values():
                query = select([Raids,
                                func.time(Raids.end_time, "unixepoch", "localtime").label("str_end_time"),
                                func.time(Raids.start_time, "unixepoch", "localtime").label("str_start_time")]). \
                    where(Raids.end_time > mid_time). \
                    where(Raids.end_time < mid_time + 86400). \
                    where(Raids.gym_id == gym.gym_id)
                print(gym.name)
                for raid in sql.execute(query):
                    print(raid.str_start_time, raid.str_end_time, raid.pokemon_id)
                print()

        # content = "Gyms that did not start a raid hour raid immediately at 6:00pm on {day}:\n".format(day=day)
        # await ctx.send(content=content + '\n'.join(str(gym.name) for gym in gyms.values()))

        # content = "Gyms that did not start a raid hour raid immediately at 6:00pm on {day}:\n".format(day=day)
        # content += '\n'.join(str(gym.name) for gym in gyms.values())
        # await ctx.send(content=content[:2000])
        # if len(content) > 2000:
        #     await ctx.send(content=content[2000:])

    @commands.command()
    async def gyms_without_raids(self, ctx: commands.Context, date: str):
        try:
            date = datetime.datetime.strptime(date, "%Y-%m-%d").timestamp()
        except ValueError:
            await ctx.send(content="Cannot parse date")
            return

        with session_scope() as sql:
            ex_gyms = {g.gym_id: g for g in sql.query(Gyms).filter(Gyms.ex_gym == 1)}
            for raid in sql.query(Raids).filter(Raids.start_time > date, Raids.end_time < date + 3600*24):
                if raid.gym_id in ex_gyms:
                    del ex_gyms[raid.gym_id]
            if ex_gyms:
                await ctx.send(content='\n'.join(str(g.name) for g in ex_gyms.values()))
            else:
                await ctx.send(content="All EX gyms had a raid that day.")


def setup(bot):
    bot.add_cog(RaidCog(bot))


def teardown(bot):
    pass
