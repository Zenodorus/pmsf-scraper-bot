import datetime
import json
import logging
import os
import re
from typing import Optional
import urllib.parse as urlparse


import discord
from discord.ext import commands, tasks

import config
from sql import session_scope
from sql.table import Types, Moves, Pokedex
from util.display import send_big_delimited_message

log = logging.getLogger(__name__)


class GameMasterUpdater(commands.Cog):

    MINERS_URL = "https://raw.githubusercontent.com/PokeMiners/game_masters/master/latest/"
    PMSF_DEX_URL = "https://raw.githubusercontent.com/pmsf/PMSF/main/static/data/pokemon.json"

    def __init__(self, bot):
        self._bot = bot
        self._latest: Optional[int] = None
        self.check.start()

    @property
    def channel(self) -> Optional[discord.TextChannel]:
        return self._bot.get_channel(config.stdout_channel)

    @property
    def latest(self):
        if self._latest is None:
            try:
                self._latest = max(map(int, (name.split('.', 1)[0] for name in os.listdir("resources/game_masters"))))
            except ValueError:
                self._latest = 0
        return self._latest

    async def cog_check(self, ctx):
        return await self._bot.is_owner(ctx.author)

    def cog_unload(self):
        self.check.cancel()

    @commands.command(hidden=True)
    async def process_gm(self, ctx: commands.Context):
        with open(f"resources/game_masters/{self.latest}.json", 'r') as f:
            try:
                await self._process_gm(json.load(f))
            except AssertionError as e:
                await self.channel.send("AssertionError: " + str(e))

    @tasks.loop(minutes=60)
    async def check(self):
        async with self._bot.session.get(urlparse.urljoin(GameMasterUpdater.MINERS_URL, "timestamp.txt")) as resp:
            # TODO error handling
            t = int(await resp.read())

        if self.latest >= t:
            return
        await self.channel.send(f'Found new game master from '
                                f'{datetime.datetime.fromtimestamp(t / 1000).strftime("%Y-%m-%d %H:%M")}')

        async with self._bot.session.get(urlparse.urljoin(GameMasterUpdater.MINERS_URL, "latest.json")) as resp:
            # TODO error handling
            gm = (await resp.read()).decode('utf-8')
            with open(f"resources/game_masters/{t}.json", 'w') as f:
                f.write(gm)

            try:
                await self._process_gm(json.loads(gm))
            except AssertionError as e:
                await self.channel.send("AssertionError: " + str(e))

    async def _process_gm(self, gm: list):
        assert isinstance(gm, list), "Game master json not a list"

        # Each entry in the game master is a pair of objects "templateId" and "data".  "templateId" is a string that
        # starts with a keyword related to its contents.  Some keywords are:
        #     "AVATAR", "AWARDS", "BADGE", "BUDDY", "CHARACTER", "COMBAT", "FORMS", "ITEM", "POKEMON_TYPE", "SPAWN",
        #     "STICKER", "TEMPORARY_EVOLUTION", "VS_SEEKER", "WEATHER", "bundle", "camera", "sequence"
        pokemon, families, moves, other = {}, [], [], []
        for item in gm:
            assert ("templateId" in item and "data" in item and len(item) == 2 and
                    item["data"]["templateId"] == item["templateId"]), f"Bad template {item}"

            if m := re.match(r"V(\d\d\d\d)_POKEMON_", item["templateId"]):
                dex = str(int(m.group(1)))
                if dex not in pokemon:
                    pokemon[dex] = {}
                if f := process_gm_form(item["data"]):
                    assert f['form'] not in pokemon[dex]
                    pokemon[dex][f['form']] = f
            elif re.match(r"V\d\d\d\d_FAMILY_", item["templateId"]):
                families.append(item["data"])
            elif re.match(r"V\d\d\d\d_MOVE_", item["templateId"]):
                moves.append(item["data"])
            else:
                other.append(item["data"])

        with session_scope() as sql:
            for move in filter(None, map(process_move, moves)):
                sql.merge(move)

        Moves.set_cache_dirty()

        async with self._bot.session.get(GameMasterUpdater.PMSF_DEX_URL) as resp:
            # TODO error handling
            pmsf_dex = json.loads((await resp.read()).decode('utf-8'))

        for n, item in pmsf_dex.items():
            e = f"Bad PMSF pokedex entry {item}"
            assert int(n) and "name" in item and "rarity" in item and "types" in item and len(item) <= 4, e
            pmsf_dex[n] = {"name": item["name"], "forms": {}}

            if "forms" in item:
                for form in item["forms"]:
                    assert "nameform" in form and "protoform" in form and "formtypes" in form and len(form) == 3, e
                    assert int(form["protoform"]), e
                    pmsf_dex[n]["forms"][form["nameform"]] = int(form["protoform"])
            else:
                assert len(item) == 3, e

        entries = []
        extra_pmsf_forms = []
        extra_gm_forms = []
        name_mapping_general = {"Alolan": "Alola"}
        name_mapping_restricted = {"1": {"Fall": "Fall 2019"}, "4": {"Fall": "Fall 2019"}, "7": {"Fall": "Fall 2019"},
                                   "3": {"Clone": "Copy 2019"}, "6": {"Clone": "Copy 2019"},
                                   "9": {"Clone": "Copy 2019"},
                                   "25": {"Clone": "Copy 2019", "Fall": "Fall 2019",
                                          "5th Anniversary": "Flying 5th Anniv"},
                                   "150": {"Armored": "A"},
                                   "302": {"Fall 2020": "Costume 2020"},
                                   "669": {"Red Flower": "Red", "Orange Flower": "Orange", "Yellow Flower": "Yellow",
                                           "Blue Flower": "Blue", "White Flower": "White"},
                                   "670": {"Red Flower": "Red", "Orange Flower": "Orange", "Yellow Flower": "Yellow",
                                           "Blue Flower": "Blue", "White Flower": "White"},
                                   "671": {"Red Flower": "Red", "Orange Flower": "Orange", "Yellow Flower": "Yellow",
                                           "Blue Flower": "Blue", "White Flower": "White"},
                                   "741": {"Baile Style": "Baile", "Pom-Pom Style": "Pompom", "Pa'u Style": "Pau",
                                           "Sensu Style": "Sensu"},
                                   # "774": {"Red": "Meteor Red", "Orange": "Meteor Orange", "Yellow": "Meteor Yellow",
                                   #         "Green": "Meteor Green", "Blue": "Meteor Blue", "Indigo": "Meteor Indigo",
                                   #         "Violet": "Meteor Violet"}
                                   }
        for n, p in pmsf_dex.items():
            pmsf_forms = p["forms"]
            if n not in pokemon:
                continue

            gm_forms = pokemon[n]
            base = gm_forms.pop("")
            required_base = dict(dex_no=base["dex_no"], name=base["name"], type_1=base["type_1"], type_2=base["type_2"],
                                 family=base["family"], can_evolve=base["can_evolve"])
            try:
                normal = gm_forms.pop("Normal")
            except KeyError:
                normal = required_base

            entries.append((n, "", 0, base))
            if n in ("201", "327", "664", "665", "666"):
                for (name, fid) in pmsf_forms.items():
                    entries.append((n, name, fid, base))
            else:
                for name, fid in pmsf_forms.items():
                    if name in ("Normal", "Shadow", "Purified"):
                        entries.append((n, name, fid, normal))
                    elif name in gm_forms:
                        entries.append((n, name, fid, gm_forms.pop(name)))
                    elif name_mapping_general.get(name, name) in gm_forms:
                        entries.append((n, name, fid, gm_forms.pop(name_mapping_general[name])))
                    elif n in name_mapping_restricted and name_mapping_restricted[n].get(name, name) in gm_forms:
                        entries.append((n, name, fid, gm_forms.pop(name_mapping_restricted[n][name])))
                    else:
                        extra_pmsf_forms.append(f'Form "{name}" not in game master for {p["name"]} (#{int(n)})')

                for name, f in gm_forms.items():
                    extra_gm_forms.append(f'TemplateId {f["templateId"]} not matched in PMSF')

        if extra_pmsf_forms:
            await send_big_delimited_message(self.channel, '\n'.join(extra_pmsf_forms))
        if extra_gm_forms:
            await self.channel.send('\n'.join(extra_gm_forms))
        with session_scope() as sql:
            for e in entries:
                sql.merge(construct_dex(*e))

        # TODO megas
        Pokedex.set_cache_dirty()
        return


def process_move(m):
    assert len(m) == 2 and "templateId" in m, f"{m['templateId']} incorrect format"
    assert "moveSettings" in m, f"{m['templateId']} incorrect format with keys {m.keys()}"

    tid = m["templateId"]
    settings = m["moveSettings"]
    required_settings_keys = ("movementId", "pokemonType", "animationId")
    assert all(k in settings for k in required_settings_keys), "missing required setting key"

    mid = settings["movementId"]
    assert settings.get("trainerLevelMin", 1) == 1 and settings.get("trainerLevelMax", 100) == 100, \
        f"bad trainer levels for {tid}"
    assert tid[11:].lower() == mid.lower(), f"bad movementId for {tid}"
    # aeroblast plus moves do not work with this line
    # assert mid.lower()[:len(settings["vfxName"])] == settings["vfxName"]
    assert "power" not in settings or settings["power"] == int(settings["power"]), f"bad power for {tid}"
    assert int(settings["accuracyChance"]) == settings["accuracyChance"] == 1, f"bad accuracy for {tid}"

    name = ' '.join(word.capitalize() for word in mid.split('_'))
    if name.endswith(" Blastoise"):
        if name == "Water Gun Fast Blastoise":
            name = "Water Gun Blastoise"
            assert settings["animationId"] == 4
        else:
            assert name in ("Scald Blastoise", "Hydro Pump Blastoise") and settings["animationId"] == 5
    elif name.endswith(" Fast"):
        if name in ("Present Fast", "Gust Fast", "Incinerate Fast"):
            settings["animationId"] = 4
        assert settings["animationId"] == 4, f"bad animationId for a fast move {tid}"
        name = name[:-5]
    else:
        assert settings["animationId"] == 5, f"bad animationId for a cinematic move {tid}"

    return Moves(move_id=int(tid[1:5]),
                 name=name,
                 move_type=settings["animationId"],
                 power=settings.get("power", 0),
                 pokemon_type=Types.get(settings["pokemonType"][13:].capitalize()).type_id,
                 energy_delta=settings.get("energyDelta", 0),
                 critical_chance=int(100 * settings.get("criticalChance", 0)),
                 stamina_loss_scalar=int(1000 * settings.get("staminaLossScalar", 0)),
                 heal_scalar=int(100 * settings.get("healScalar", 0)),
                 damage_window_start=settings["damageWindowStartMs"],
                 damage_window_end=settings["damageWindowEndMs"],
                 duration=settings["durationMs"])


def process_gm_form(p: dict):
    assert len(p) == 2 and "templateId" in p, f"{p['templateId']} incorrect format"
    if "pokemonHomeFormReversions" in p:
        return None
    assert "pokemonSettings" in p, f"{p['templateId']} incorrect format with keys {p.keys()}"

    tid = p["templateId"]
    settings = p["pokemonSettings"]
    required_settings_keys = ("pokemonId", "type", "stats", "thirdMove")
    assert all(k in settings for k in required_settings_keys), "missing required setting key"

    pid = settings["pokemonId"]
    if tid[14:].startswith("NIDORAN"):
        assert pid in ("NIDORAN_FEMALE", "NIDORAN_MALE"), "bad Nidoran pokemonId"
        name = "Nidoran♀" if pid == "NIDORAN_FEMALE" else "Nidoran♂"
        assert "form" not in settings or settings["form"].startswith("NIDORAN"), "bad Nidoran form"
        form = ' '.join(word.capitalize() for word in settings.get("form", "")[8:].split('_'))
    else:
        assert "form" not in settings or settings["form"].startswith(pid), "bad form"

        form = settings.get("form", "")[1 + len(pid):]

        match pid:
            case "FARFETCHD":
                name = "Farfetch'd"
            case "MR_MIME":
                name = "Mr. Mime"
            case "HO_OH":
                name = "Ho-Oh"
            case "MIME_JR":
                name = "Mime Jr."
            case "PORYGON_Z":
                name = "Porygon-Z"
            case "TYPE_NULL":
                name = "Type: Null"
            case "JANGMO_O":
                name = "Jangmo-o"
            case "HAKAMO_O":
                name = "Hakamo-o"
            case "KOMMO_O":
                name = "Kommo-o"
            case "SIRFETCHD":
                name = "Sirfetch'd"
            case "MR_RIME":
                name = "Mr. Rime"
            case _:
                name = ' '.join(word.capitalize() for word in pid.split('_'))

        if tid[14:] == pid:
            form = ""
        elif tid[14:] == pid + '_' + form:
            form = ' '.join(word.capitalize() for word in form.split('_'))
        else:
            raise AssertionError("error parsing {key}".format(key=tid))

    # log.info(f"{tid}, {pid}, {name}, {form}, " + settings["familyId"])

    assert settings["type"].startswith("POKEMON_TYPE_")
    assert "type2" not in settings or settings["type2"].startswith("POKEMON_TYPE_")
    assert settings["kmBuddyDistance"] == int(settings["kmBuddyDistance"])
    if len(settings["stats"]) == 3:
        assert set(settings["stats"].keys()) == {"baseAttack", "baseDefense", "baseStamina"}, "bad stats keys"
    else:
        assert not len(settings["stats"]), "unknown stats keys"
    assert set(settings["thirdMove"].keys()) <= {"stardustToUnlock", "candyToUnlock"}

    prior = settings.get("parentPokemonId", "").capitalize()
    match prior:
        case "FARFETCHD":
            prior = "Farfetch'd"
        case "MR_MIME":
            prior = "Mr. Mime"
        case "MIME_JR":
            prior = "Mime Jr."
        case "JANGMO_O":
            prior = "Jangmo-o"
        case "HAKAMO_O":
            prior = "Hakamo-o"

    assert "pokemonClass" not in settings or settings["pokemonClass"].startswith("POKEMON_CLASS_")

    # return p
    return dict(templateId=tid,
                dex_no=int(tid[1:5]),
                name=name,
                form=form,
                type_1=Types.get(settings["type"][13:].capitalize()).type_id,
                type_2=Types.get(settings["type2"][13:].capitalize()).type_id if "type2" in settings else None,
                # TODO: edit family for nidoran male/female
                family=' '.join(word.capitalize() for word in settings["familyId"][7:].split('_')),
                prior_evolution=prior,
                base_atk=settings["stats"].get("baseAttack", None),
                base_def=settings["stats"].get("baseDefense", None),
                base_sta=settings["stats"].get("baseStamina", None),
                buddy_distance=int(settings["kmBuddyDistance"]),
                height=str(settings["pokedexHeightM"]) if "pokedexHeightM" in settings else None,
                weight=str(settings["pokedexWeightKg"]) if "pokedexWeightKg" in settings else None,
                height_std_dev=str(settings["heightStdDev"]) if "heightStdDev" in settings else None,
                weight_std_dev=str(settings["weightStdDev"]) if "weightStdDev" in settings else None,
                is_gym_deployable=settings.get("isDeployable", False),
                is_transferable=settings.get("isTransferable", False),
                rarity=' '.join(word.capitalize() for word in settings.get("pokemonClass", "")[14:].split('_')),
                quick_moves=settings.get("quickMoves", []),
                charge_moves=settings.get("cinematicMoves", []),
                third_move_stardust=settings["thirdMove"].get("stardustToUnlock", 0),
                third_move_candy=settings["thirdMove"].get("candyToUnlock", 0),
                purify_stardust=settings["shadow"]["purificationStardustNeeded"] if "shadow" in settings else 0,
                purify_candy=settings["shadow"]["purificationCandyNeeded"] if "shadow" in settings else 0,
                can_evolve=int("evolutionBranch" in settings))


def construct_dex(n, fname, fid, form):
    form = form.copy()
    if "templateId" in form:
        del form["templateId"]
    form["form"] = fname
    form["form_id"] = fid

    try:
        quick_moves = [Moves.get(' '.join(word.capitalize() for word in move[:-5].split('_') if
                                          move.endswith("_FAST"))) for move in form.get("quick_moves", [])]
        assert len(quick_moves) == len(form.get("quick_moves", [])), f"bad quick move set for #{n}"
    except KeyError:
        quick_moves = []

    try:
        charge_moves = [Moves.get(' '.join(word.capitalize() for word in move.split('_')))
                        for move in form.get("charge_moves", [])]
        assert len(charge_moves) == len(form.get("charge_moves", [])), f"bad charge move set for #{n}"
    except KeyError:
        charge_moves = []

    assert not list(filter(lambda m: m.move_type != 4, quick_moves))
    assert not list(filter(lambda m: m.move_type != 5, charge_moves))

    form["quick_moves"] = ','.join(str(move.move_id) for move in quick_moves)
    form["charge_moves"] = ','.join(str(move.move_id) for move in charge_moves)

    match fname:
        case "Alolan":
            code = "-a"
        case "Galarian":
            code = "-g"
        case "Mega":
            code = "-m"
        case "MegaX":
            code = "-mx"
        case "MegaY":
            code = "-my"
        case "Primal":
            code = "-m"
        case _:
            code = ""

    if cached_dex := Pokedex.cache.get((form["dex_no"], form["form"]), None):
        form["thumbnail"] = cached_dex.thumbnail
        form["bit_icon"] = cached_dex.bit_icon
    else:
        form["thumbnail"] = f"https://serebii.net/pokemon/art/{int(n):03d}{code}.png"
        form["bit_icon"] = f"https://serebii.net/pokedex-swsh/icon/{int(n):03d}{code}.png"

    return Pokedex(**form)


def setup(bot):
    bot.add_cog(GameMasterUpdater(bot))
