import os.path
import re
import json
from typing import Optional, Union, Dict, Tuple

import discord
from discord.ext import commands
from discord.utils import get

from bot import PMSFBot
from sql.table import Pokedex


class ReactionRoles(commands.Cog):

    def __init__(self, bot):
        self._bot: commands.Bot = bot
        self._config: Dict[int, Dict[str, Union[int, str, Dict[Union[int, str], int]]]] = {}
        self._load_config()

    # cog settings

    async def cog_check(self, ctx: commands.Context) -> bool:
        return await self._bot.is_owner(ctx.author)

    async def cog_command_error(self, ctx, error):
        # TODO: handle when message is more then 2000 characters
        if isinstance(error, commands.UserInputError):
            await ctx.send(content=f"UserInputError: {error}")
        elif isinstance(error, commands.ConversionError):
            await ctx.send(content=f"ConversionError: {error.original}")
        else:
            raise error

    # config

    def _load_config(self):
        if not os.path.exists("resources/roles.json"):
            self._config = {}
            self._save_config()
            return

        with open("resources/roles.json", 'r') as _f:
            config = json.load(_f)

        for mid, c in config.items():
            roles = {}
            for k, v in c["roles"].items():
                try:
                    roles[int(k)] = v
                except ValueError:
                    roles[k] = v
            c["roles"] = roles
            self._config[int(mid)] = c

    def _save_config(self):
        with open("resources/roles.json", 'w') as _f:
            json.dump(self._config, _f)

    def _check_reaction_payload(self, payload) -> Optional[Tuple[discord.Member, discord.Role]]:
        if payload.message_id not in self._config:
            return
        if payload.user_id == self._bot.user.id:
            return
        config = self._config[payload.message_id]
        guild: discord.Guild = self._bot.get_guild(config["guild"])
        emoji_key = payload.emoji.id if payload.emoji.id is not None else payload.emoji.name
        user = guild.get_member(payload.user_id)
        if user is None:
            return
        return user, guild.get_role(config["roles"][emoji_key])

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if (results := self._check_reaction_payload(payload)) is None:
            return
        await results[0].add_roles(results[1], reason="User requested role")

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload):
        if (results := self._check_reaction_payload(payload)) is None:
            return
        await results[0].remove_roles(results[1], reason="User requested role removal")

    @staticmethod
    def _parse_emoji_roles(message: discord.Message):
        content = message.content
        roles = {}
        for line in content.split('\n'):
            if m := re.match(r"<:.*:(\d*)> <@&(\d*)>", line):
                roles[int(m.group(1))] = int(m.group(2))
            elif m := re.match(r"(.) <@&(\d*)>", line):
                roles[m.group(1)] = int(m.group(2))
        if len(roles) > 20:
            raise commands.UserInputError("message has more than 20 reaction roles")
        return roles

    async def _build_message(self, message: discord.Message):
        mid = message.id
        if self._config[mid]["type"] != "auto":
            raise commands.UserInputError("this message cannot be built")

        guild: discord.Guild = message.guild
        config = self._config[message.id]
        content = config["description"]
        for k, v in config["roles"].items():
            if isinstance(k, int):
                content += '\n' + f"{self._bot.get_emoji(k)} {guild.get_role(v).mention}"
            elif isinstance(k, str):
                content += '\n' + f"{k} {guild.get_role(v).mention}"
        await message.edit(content=content)

        my_reactions = set()
        for r in message.reactions:
            if isinstance(r.emoji, str) and r.emoji not in config["roles"]:
                await r.clear()
            elif isinstance(r.emoji, discord.Emoji) and r.emoji.id not in config["roles"]:
                await r.clear()
            elif isinstance(r.emoji, discord.PartialEmoji) and r.emoji.id not in config["roles"]:
                await r.clear()
            elif r.me:
                my_reactions.add(r.emoji if isinstance(r.emoji, str) else r.emoji.id)
        for e in set(config["roles"]) - my_reactions:
            if isinstance(e, str):
                await message.add_reaction(e)
            elif isinstance(e, int):
                await message.add_reaction(self._bot.get_emoji(e))

    async def create(self, message: Optional[discord.Message] = None,
                     channel: Optional[discord.TextChannel] = None,
                     description: Optional[str] = None):
        if message is not None and channel is not None:
            raise commands.UserInputError("a message and channel were both given")
        elif message is not None:
            mtype = "manual"
            description = None
        elif channel is not None:
            mtype = "auto"
            description = description or "Description of the role message"
            message = await channel.send(content=description)
        else:
            raise commands.UserInputError("not given a message or channel to place a role message")

        message_config = {"channel": message.channel.id,
                          "guild": message.guild.id,
                          "author": message.author.id,
                          "type": mtype,
                          "description": description,
                          "roles": self._parse_emoji_roles(message)}
        self._config[message.id] = message_config
        self._save_config()
        return message

    async def edit(self, message: discord.Message, description: str = ""):
        self._config[message.id]["description"] = description
        await self._build_message(message)
        self._save_config()
        return message

    def update(self, message: discord.Message):
        if message.id not in self._config:
            raise commands.UserInputError("message is not being monitored")

        self._config[message.id]["roles"] = self._parse_emoji_roles(message)
        self._save_config()

    def delete(self, message: discord.Message):
        if message.id not in self._config:
            raise commands.UserInputError("message is not being monitored")
        del self._config[message.id]
        self._save_config()

    async def add_role(self, message: discord.Message, emoji: Union[discord.Emoji, str], role: discord.Role):
        config = self._config[message.id]
        if len(config["roles"]) >= 20:
            raise commands.UserInputError("message has reached maximum 20 reaction roles")

        emoji_key = emoji.id if isinstance(emoji, discord.Emoji) else emoji
        if emoji_key in config["roles"]:
            raise commands.UserInputError(f"message already has a role assigned to the emoji {emoji}")
        config["roles"][emoji_key] = role.id
        await self._build_message(message)
        self._save_config()

    async def remove_role(self, message: discord.Message,
                          emoji: Optional[Union[discord.Emoji, str]] = None,
                          role: Optional[discord.Role] = None):
        config = self._config[message.id]
        if emoji is None and role is None:
            raise commands.UserInputError("neither emoji nor role was specified to be removed")
        elif emoji is not None:
            emoji_key = emoji.id if isinstance(emoji, discord.Emoji) else emoji
            if role is not None and config["roles"][emoji_key] != role.id:
                raise commands.UserInputError("both emoji and role specified in given role message and do not agree")
        elif role is not None:
            emoji_key = None
            for e, r in config["roles"].items():
                if role.id == r:
                    emoji_key = e
        else:
            emoji_key = None

        if emoji_key is None or emoji_key not in config["roles"]:
            raise commands.UserInputError("role is not in given message")

        del config["roles"][emoji_key]
        await self._build_message(message)
        self._save_config()

    @commands.command(name="create")
    async def _create_cmd(self, ctx: commands.Context,
                          message: Optional[discord.Message] = None,
                          channel: Optional[discord.TextChannel] = None,
                          *, description: Optional[str] = None):
        """
        Create a message to add roles based on reactions.

        #!create ARGS

        ARGS can be in one of three forms:
        - just a string to be the message.  defaults to adding the message in the channel where the command was invoked
        - a channel name or ID to place the message, with or without a string message
        - a message ID for a message to watch

        Raises error if message cannot be found, if channel cannot be found, if message or channel is not in the
        guild where the command is invoked, if the user does not have permissions
        """
        if message is not None:
            if ctx.guild != message.guild:
                raise commands.UserInputError("command must be executed in the same guild as the message")
            channel = None
            description = None
        elif channel is not None:
            if ctx.guild != channel.guild:
                raise commands.UserInputError("command must be executed in the same guild as the message")
            description = description or "Description of the role message"
        else:
            description = description or "Description of the role message"
            channel = ctx.channel

        message = await self.create(message=message, channel=channel, description=description)
        await ctx.send(content=f"Message {message.channel.id}-{message.id} added to role monitor with "
                               f"{len(self._config[message.id]['roles'])} roles.")

    @commands.command(name="edit")
    async def _edit_cmd(self, ctx: commands.Context, message: discord.Message, *, description: str = ""):
        """
        Edit the introduction description in a roles message.

        #!edit MESSAGE_ID DESC

        Raise error if message cannot be found, if the message is not in the guild where the command is invoked,
        or if the user does not have permissions
        """
        if ctx.guild != message.guild:
            raise commands.UserInputError("command must be executed in the same guild as the message")
        elif message.author.id != self._bot.user.id:
            raise commands.UserInputError("only messages created by the bot can be directly edited")
        elif message.id not in self._config:
            raise commands.UserInputError("message is not being monitored")
        elif self._config[message.id]["type"] != "auto":
            raise commands.UserInputError("this message cannot be managed via role commands")

        await self.edit(message, description)

    @commands.command(name="update")
    async def _update_cmd(self, ctx: commands.Context, message: discord.Message):
        """
        Reparse a message to update for newly added roles.

        #!update MESSAGE_ID

        Raise error if message cannot be found, if the message is not in the guild where the command is invoked,
        or if the user does not have permissions
        """
        if ctx.guild != message.guild:
            raise commands.UserInputError("command must be executed in the same guild as the message")

        self.update(message)
        await ctx.send(content=f"Message {message.channel.id}-{message.id} updated.  "
                               f"Found {len(self._config[message.id]['roles'])} reaction roles.")

    @commands.command(name="delete")
    async def _delete_cmd(self, ctx: commands.Context, message: discord.Message):
        """
        Delete a roles message entirely.

        #!delete MESSAGE_ID

        Raise error if message cannot be found, if the message is not in the guild where the command is invoked,
        or if the user does not have permissions
        """
        if ctx.guild != message.guild:
            raise commands.UserInputError("command must be executed in the same guild as the message")
        self.delete(message)
        await ctx.send(content=f"Message {message.channel.id}-{message.id} is not longer being monitored.")

    @commands.command(name="add_role")
    async def _add_role_cmd(self, ctx: commands.Context,
                            message: discord.Message,
                            emoji: Union[discord.Emoji, str],
                            role: discord.Role):
        """
        Add a role to a roles message.

        #!add_role MESSAGE_ID EMOJI ROLE

        ROLE can be in one of three forms:
        - a mention
        - name of role
        - role ID

        Raise error if message cannot be found, if the message already has the role with a different emoji, if the
        message is not in the guild where the command is invoked, or if the user does not have permissions
        """
        if ctx.guild != message.guild:
            raise commands.UserInputError("command must be executed in the same guild as the message")
        elif message.author.id != self._bot.user.id:
            raise commands.UserInputError("only messages created by the bot can be directly edited")
        elif message.id not in self._config:
            raise commands.UserInputError("message is not being monitored")
        elif self._config[message.id]["type"] != "auto":
            raise commands.UserInputError("this message cannot be managed via role commands")

        await self.add_role(message, emoji, role)

    @commands.command(name="remove_role")
    async def _remove_role_cmd(self, ctx: commands.Context,
                               message: discord.Message,
                               emoji: Optional[Union[discord.Emoji, str]] = None,
                               role: Optional[discord.Role] = None):
        """
        Remove a role from a roles message.

        #!remove_role MESSAGE_ID ROLE

        ROLE can be in one of three forms:
        - a mention
        - name of role
        - role ID

        Raise error if message cannot be found, if the message does not have that role in it, if the message is not in
        the guild where the command is invoked, or if the user does not have permissions
        """
        if ctx.guild != message.guild:
            raise commands.UserInputError("command must be executed in the same guild as the message")
        elif message.author.id != self._bot.user.id:
            raise commands.UserInputError("only messages created by the bot can be directly edited")
        elif message.id not in self._config:
            raise commands.UserInputError("message is not being monitored")
        elif self._config[message.id]["type"] != "auto":
            raise commands.UserInputError("this message cannot be managed via role commands")

        await self.remove_role(message=message, emoji=emoji, role=role)


class PokemonRoles(commands.Cog):

    def __init__(self, bot, role_cog):
        self._bot: "PMSFBot" = bot
        self._role_cog: ReactionRoles = role_cog
        self._config = {}
        self._load_config()

    # cog settings

    async def cog_check(self, ctx: commands.Context) -> bool:
        return await self._bot.is_owner(ctx.author)

    async def cog_command_error(self, ctx, error):
        # TODO: handle when message is more then 2000 characters
        if isinstance(error, commands.UserInputError):
            await ctx.send(content=f"UserInputError: {error}", delete_after=20)
        elif isinstance(error, commands.ConversionError):
            await ctx.send(content=f"ConversionError: {error.original}", delete_after=20)
        else:
            raise error

    # config

    def _load_config(self):
        if not os.path.exists("resources/pokemon_roles.json"):
            self._config = {}
            self._save_config()
            return

        with open("resources/pokemon_roles.json", 'r') as _f:
            self._config = json.load(_f)
        if "pokemon" in self._config:
            self._config["pokemon"] = {int(k): v for k, v in self._config["pokemon"].items()}

    def _save_config(self):
        with open("resources/pokemon_roles.json", 'w') as _f:
            json.dump(self._config, _f)

    # commands

    @commands.command(hidden=True)
    async def create_role_messages(self, ctx: commands.Context):
        guild = ctx.guild
        rc = self._role_cog
        eid = {"Mystic": 284614278709903371, "Valor": 284614087336525826, "Instinct": 284614748702638081,
               "PVPmon": 685361178599358465}

        msg = await rc.create(description="React with the corresponding team emoji to be assigned the team:",
                              channel=ctx.channel)
        for r in ("Mystic", "Valor", "Instinct"):
            await rc.add_role(message=msg, emoji=self._bot.get_emoji(eid[r]), role=get(guild.roles, name=r))
        self._config["team"] = msg.id

        msg = await rc.create(description="React with the corresponding emoji to be assigned alerts for IVs:",
                              channel=ctx.channel)
        await rc.add_role(message=msg, emoji=b"\xf0\x9f\x92\xaf".decode("utf-8"), role=get(guild.roles, name="100IV"))
        await rc.add_role(message=msg, emoji=self._bot.get_emoji(eid["PVPmon"]), role=get(guild.roles, name="PVPmon"))
        self._config["ivs"] = msg.id

        self._config["pokemon"] = {}
        ordinals = ("first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth")
        for i, ordinal in enumerate(ordinals):
            msg = await rc.create(description=f"React with the corresponding emoji to be assigned an "
                                              f"alert for Pokémon in the {ordinal} generation:",
                                  channel=ctx.channel)
            self._config["pokemon"][i + 1] = msg.id

        self._config["channel"] = ctx.channel.id
        self._save_config()

    @commands.command(hidden=True)
    async def create_pokemon_role(self, ctx: commands.Context, *, pokemon: str):
        await ctx.message.delete()

        try:
            dex = Pokedex.search_str(pokemon)
        except KeyError:
            raise commands.UserInputError(f'Unable to identify pokemon "{pokemon}".  Check your spelling.')

        generation = 0
        if dex.form == "Alolan":
            generation = 7
        elif dex.form == "Galarian" or dex.form == "Hisuian":
            generation = 8
        elif dex.dex_no < 152:
            generation = 1
        elif dex.dex_no < 252:
            generation = 2
        elif dex.dex_no < 387:
            generation = 3
        elif dex.dex_no < 494:
            generation = 4
        elif dex.dex_no < 650:
            generation = 5
        elif dex.dex_no < 722:
            generation = 6
        elif dex.dex_no < 810:
            generation = 7
        elif dex.dex_no < 906:
            generation = 8
        if not generation:
            await ctx.send(content=f"Unable to identify the generation of {pokemon}", delete_after=20)
            return

        guild: discord.Guild = ctx.guild
        role = get(guild.roles, name=str(dex))
        emoji_name = str(dex)
        for c in " .':-":
            emoji_name = emoji_name.replace(c, '')
        emoji = get(guild.emojis, name=emoji_name)

        if emoji is None:
            path = os.path.join("resources", "icons", os.path.split(dex.bit_icon)[1])
            if not os.path.exists(path):
                with open(path, 'wb') as f:
                    async with self._bot.session.get(dex.bit_icon) as resp:
                        data = await resp.read()
                        f.write(data)
            else:
                with open(path, 'rb') as f:
                    data = f.read()
            emoji_name = str(dex)
            for c in " .':-":
                emoji_name = emoji_name.replace(c, '')
            emoji = await ctx.guild.create_custom_emoji(name=emoji_name, image=data,
                                                        reason="Emoji for role assignment system")
        if role is None:
            role = await guild.create_role(name=str(dex), mentionable=True, reason="Alert for particular species")

        channel = self._bot.get_channel(self._config["channel"])
        msg = await channel.fetch_message(self._config["pokemon"][generation])
        await self._role_cog.add_role(message=msg, emoji=emoji, role=role)
        self._save_config()

    @commands.command(hidden=True)
    async def delete_pokemon_role(self, ctx, *, pokemon: str):
        await ctx.message.delete()

        try:
            dex = Pokedex.search_str(pokemon)
        except KeyError:
            raise commands.UserInputError(f'Unable to identify pokemon "{pokemon}".  Check your spelling.')

        generation = 0
        if dex.form == "Alolan":
            generation = 7
        elif dex.form == "Galarian" or dex.form == "Hisuian":
            generation = 8
        elif dex.dex_no < 152:
            generation = 1
        elif dex.dex_no < 252:
            generation = 2
        elif dex.dex_no < 387:
            generation = 3
        elif dex.dex_no < 494:
            generation = 4
        elif dex.dex_no < 650:
            generation = 5
        elif dex.dex_no < 722:
            generation = 6
        elif dex.dex_no < 810:
            generation = 7
        elif dex.dex_no < 906:
            generation = 8
        if not generation:
            await ctx.send(content=f"Unable to identify the generation of {pokemon}", delete_after=20)
            return

        guild: discord.Guild = ctx.guild
        emoji_name = str(dex)
        for c in " .':-":
            emoji_name = emoji_name.replace(c, '')
        emoji = get(guild.emojis, name=emoji_name)
        channel = self._bot.get_channel(self._config["channel"])
        msg = await channel.fetch_message(self._config["pokemon"][generation])
        await self._role_cog.remove_role(message=msg, emoji=emoji)


def setup(bot):
    role_cog = ReactionRoles(bot)
    bot.add_cog(role_cog)
    bot.add_cog(PokemonRoles(bot, role_cog))


def teardown(bot):
    pass
