from typing import Optional

from discord.ext import commands

from sql import session_scope
from sql.table import Pokedex, PVPStats


class PVPPokemonManager(commands.Cog):

    CHANNEL_ID = 685678303503581272

    def __init__(self, bot):
        self._bot = bot

    async def cog_command_error(self, ctx, error):
        # TODO: handle when message is more then 2000 characters
        if isinstance(error, commands.UserInputError):
            await ctx.send(content=f"UserInputError: {error}")
        elif isinstance(error, commands.ConversionError):
            await ctx.send(content=f"ConversionError: {error.original}")
        else:
            raise error

    # @commands.command()
    # async def pvp_ivs(self, ctx: commands.Context, pokemon: str):
    #     if pokemon.lower() in ("mr mime", "mr. mime"):
    #         form, pokemon = "", "Mr. Mime"
    #     elif pokemon.lower() in ("mime jr", "mime jr."):
    #         form, pokemon = "", "Mime Jr."
    #     elif pokemon.lower() in ("galarian mr mime", "galarian mr. mime"):
    #         form, pokemon = "Galarian", "Mr. Mime"
    #     else:
    #         pokemon = pokemon.rsplit(' ', 1)
    #         form, pokemon = ("", pokemon[0]) if len(pokemon) == 1 else pokemon
    #
    #     try:
    #         dex = Pokedex.search(name=pokemon, form=form)
    #     except KeyError:
    #         raise commands.UserInputError(f"cannot find {form} {pokemon}")
    #
    #     with session_scope() as sql:
    #         results = sql.query(PVPStats).filter(PVPStats.dex_no == dex.dex_no, PVPStats.target_dex == dex.dex_no,
    #                                              PVPStats.form == form).all()
    #         for r in results:
    #             await ctx.send(str(r))

    @commands.command()
    async def pvp_ivs_best(self, ctx: commands.Context, *, pokemon: str):
        try:
            dex = Pokedex.search_str(pokemon)
        except KeyError:
            raise commands.UserInputError(f'Pokemon "{pokemon}" not found')

        star = {}
        for i in range(23):
            star[i] = "0* "
        for i in range(23, 30):
            star[i] = "1* "
        for i in range(30, 37):
            star[i] = "2* "
        for i in range(37, 45):
            star[i] = "3* "
        star[45] = "4* "

        with session_scope() as sql:
            results = sql.query(PVPStats).filter(PVPStats.dex_no == dex.dex_no, PVPStats.form == dex.form,
                                                 PVPStats.rank == 1).all()
            iv_sets = list(set((row.atk_iv, row.def_iv, row.sta_iv) for row in results))
            iv_sets.sort()
            await ctx.send("```" + '\n'.join(star[sum(row)] + '/'.join(map(str, row)) for row in iv_sets) + "```")

    @commands.command()
    async def pvp_rank(self, ctx: commands.Context, atk_iv: int, def_iv: int, sta_iv: int, *, pokemon: str):
        try:
            dex = Pokedex.search_str(pokemon)
        except KeyError:
            raise commands.UserInputError(f'Pokemon "{pokemon}" not found')

        with session_scope() as sql:
            results = sql.query(PVPStats).filter(PVPStats.dex_no == dex.dex_no, PVPStats.form == dex.form,
                                                 PVPStats.atk_iv == atk_iv, PVPStats.def_iv == def_iv,
                                                 PVPStats.sta_iv == sta_iv).all()
            if results:
                output = f'Ranks for {dex} with IVs {atk_iv}/{def_iv}/{sta_iv}:'
                for pvp in results:
                    if pvp.great_league:
                        output += (f'\n- GL {pvp.target_dex}, Level {pvp.great_league}, Rank {pvp.rank}, '
                                   f'Metric {pvp.metric}')
                    elif pvp.ultra_league:
                        output += (f'\n- UL {pvp.target_dex}, Level {pvp.ultra_league}, Rank {pvp.rank}, '
                                   f'Metric {pvp.metric}')
                    elif pvp.little_league:
                        output += (f'\n- LL {pvp.target_dex}, Level {pvp.little_league}, Rank {pvp.rank}, '
                                   f'Metric {pvp.metric}\n')
                await ctx.send(f'```{output}```')
            else:
                await ctx.send(f'{dex} with IVs {atk_iv}/{def_iv}/{sta_iv} is not in the top 5 ranks.')


def setup(bot):
    bot.add_cog(PVPPokemonManager(bot))


def teardown(bot):
    pass
