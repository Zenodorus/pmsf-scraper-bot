import datetime
import logging
import time
from typing import Optional, Dict, Tuple

import discord
import pytz
from discord.ext import commands, tasks
from sqlalchemy.orm import joinedload

from sql import session_scope
from sql.table import Gyms, Raids
from util import MAPS_URL

GENDER_CODES = [None, "Male", "Female", ""]
NUMBER_EMOJIS = tuple((i + b'\xef\xb8\x8f\xe2\x83\xa3').decode("utf-8") for i in (b'1', b'2', b'3', b'4', b'5'))
TEAM_EMOJI_IDS = (284614278709903371, 284614087336525826, 284614748702638081)
TEAM_COLOURS = (discord.embeds.EmptyEmbed, 0x3D9FFF, 0xEE5B5B, 0xFFC432)

log = logging.getLogger(__name__)


class GymEmbed(object):

    def __init__(self, bot: commands.Bot, message: discord.Message, gym: Gyms, raid: Optional[Raids]):
        self._bot: commands.Bot = bot
        self._message: discord.Message = message
        self._gym: Gyms = gym
        self._raid: Optional[Raids] = raid

    @property
    def message(self) -> discord.Message:
        return self._message

    @property
    def embed(self) -> discord.Embed:
        return self._message.embeds[0]

    @property
    def channel(self) -> discord.TextChannel:
        return self._message.channel

    @property
    def gym_occupant(self) -> int:
        if isinstance(self.embed.colour, discord.Colour):
            return TEAM_COLOURS.index(self.embed.colour.value)
        return 0

    @staticmethod
    def _raid_text(raid) -> str:
        start = datetime.datetime.fromtimestamp(raid.start_time, tz=pytz.timezone("US/Eastern")).strftime("%I:%M:%S")
        end = datetime.datetime.fromtimestamp(raid.end_time, tz=pytz.timezone("US/Eastern")).strftime("%I:%M:%S")
        if raid.pokemon_id is not None:
            move_1 = raid.move_1.name if raid.move_1 is not None else raid.pokemon_move_1
            move_2 = raid.move_2.name if raid.move_2 is not None else raid.pokemon_move_2
            # pokemon_name = ((raid.dex.form + ' ') if raid.dex.form not in ("Normal", "") else "") + raid.dex.name
            gender = ((GENDER_CODES[raid.pokemon_gender] + " ") if
                      raid.pokemon_gender is not None and 1 <= raid.pokemon_gender <= 2 else "")
            # return f"A {gender}{pokemon_name} attacks with {move_1} and {move_2}, and it flees at {end}."
            return f"A {gender}{raid.dex} attacks with {move_1} and {move_2}, and it flees at {end}."
        elif time.time() < raid.start_time:
            return f"A level {raid.level} egg hatches at {start} and the pokemon flees at {end}."
        else:
            return f"A hatched level {raid.level} egg flees at {end}."

    @classmethod
    def build_embed(cls, gym: Gyms, raid: Optional[Raids]) -> discord.Embed:
        embed = discord.Embed(title=gym.name, description=MAPS_URL.format(lat=gym.latitude, lng=gym.longitude),
                              timestamp=datetime.datetime.fromtimestamp(gym.last_scanned + 4*3600),
                              colour=TEAM_COLOURS[gym.team_id])
        embed.set_thumbnail(url=gym.url)
        embed.set_footer(text=gym.gym_id)
        embed.add_field(name="Current Occupants", inline=False,
                        value=(("Unoccupied", "Mystic", "Valor", "Instinct")[gym.team_id] +
                               f" has {6 - gym.slots_available} defender{'' if gym.slots_available == 5 else 's'}."))
        if raid:
            embed.add_field(name="Raids", value=cls._raid_text(raid), inline=False)
        else:
            embed.add_field(name="Raids", value="No raids present at this gym.", inline=False)
        return embed

    async def _update_embed(self) -> None:
        self.embed.colour = TEAM_COLOURS[self._gym.team_id]
        self.embed.timestamp = datetime.datetime.fromtimestamp(self._gym.last_scanned + 4*3600)
        self.embed.set_field_at(0, name="Current Occupants", inline=False,
                                value=(("Unoccupied", "Mystic", "Valor", "Instinct")[self._gym.team_id] +
                                       f" has {6 - self._gym.slots_available} defender"
                                       f"{'' if self._gym.slots_available == 5 else 's'}."))
        if self._raid:
            self.embed.set_field_at(1, name="Raid", value=self._raid_text(self._raid), inline=False)
        else:
            self.embed.set_field_at(1, name="Raid", value="No raid present at this gym.", inline=False)
        await self._message.edit(embed=self.embed)

    async def _control_notification(self, users, gym):
        old_team = ("", "Mystic", "Valor", "Instinct")[self._gym.team_id]
        new_team = ("", "Mystic", "Valor", "Instinct")[gym.team_id]
        if new_team:
            content = f'Team {old_team} lost control of "{self._gym.name}" to team {new_team}.\n\n'
        else:
            content = (f'Team {old_team} lost control of "{self._gym.name}".  The victors haven\'t put '
                       f'a defender in yet.\n\n')
        content += ' '.join(u.mention for u in users)
        await self.channel.send(content=content)

    async def _raid_notification(self, users, raid):
        start = datetime.datetime.fromtimestamp(raid.start_time, tz=pytz.timezone("US/Eastern")).strftime("%I:%M:%S")
        end = datetime.datetime.fromtimestamp(raid.end_time, tz=pytz.timezone("US/Eastern")).strftime("%I:%M:%S")
        team = ("", "Mystic", "Valor", "Instinct")[self._gym.team_id]
        if raid.pokemon_id is not None:
            move_1 = raid.move_1.name if raid.move_1 is not None else raid.pokemon_move_1
            move_2 = raid.move_2.name if raid.move_2 is not None else raid.pokemon_move_2
            pokemon_name = ((raid.dex.form + ' ') if raid.dex.form not in ("Normal", "") else "") + raid.dex.name
            gender = ((GENDER_CODES[raid.pokemon_gender] + " ") if
                      raid.pokemon_gender is not None and 1 <= raid.pokemon_gender <= 2 else "")
            content = f'"{self._gym.name}" is being attacked by a {gender}{pokemon_name} that knowns {move_1} and ' \
                      f'{move_2}!  It flees at {end}.\n\n'
        elif time.time() < raid.start_time:
            content = (f'"{self._gym.name}" has a level {raid.level} egg that hatches at {start} and flees '
                       f'at {end}.  Team {team} holds the gym right now.\n\n')
        else:
            content = f'"{self._gym.name}" hatched a level {raid.level} egg that flees at {end}.\n\n'
        content += ' '.join(u.mention for u in users)
        await self.channel.send(content=content)

    async def update(self, message: discord.Message, gym: Gyms, raid: Optional[Raids] = None):
        assert self._message.id == message.id
        self._message = message
        # TODO does this affect raids?
        # compare with cached gym
        if self._gym.last_scanned == gym.last_scanned:
            return

        do_edit = False
        last_modified = datetime.datetime.utcfromtimestamp(gym.last_modified)
        if (self._message.edited_at or self._message.created_at) < last_modified:
            do_edit = True
            if gym.team_id != self.gym_occupant != 0:
                # requires reactions in fixed order
                r = message.reactions[self.gym_occupant - 1]
                users = [user async for user in r.users() if user != self._bot.user]
                if users:
                    await self._control_notification(users, gym)
                    # await asyncio.gather(*(r.remove(user) for user in users))

                # async for user in r.users():
                #     if user != self._bot.user:
                #         await self._control_notification(gym)
                #         await r.remove(user)
            elif self.gym_occupant != gym.team_id and len(message.reactions) > 7:
                # special notification for all notifications on gym change
                r = message.reactions[7]
                users = [user async for user in r.users() if user != self._bot.user]
                if users:
                    await self._control_notification(users, gym)

        if self._raid is None and raid is None:
            pass
        elif self._raid is None and raid is not None:
            r = message.reactions[{1: 3, 3: 4, 5: 5, 6: 6}[raid.level]]
            users = [user async for user in r.users() if user != self._bot.user]
            if users:
                await self._raid_notification(users, raid)
            do_edit = True
        elif self._raid is not None and raid is None:
            do_edit = True
        elif self._raid.pokemon_id is None and raid.pokemon_id is not None:
            r = message.reactions[{1: 3, 3: 4, 5: 5, 6: 6}[raid.level]]
            users = [user async for user in r.users() if user != self._bot.user]
            if users:
                await self._raid_notification(users, raid)
            do_edit = True

        self._gym = gym
        self._raid = raid
        if do_edit:
            await self._update_embed()

    async def delete(self):
        await self._message.delete()


class GymNotifier(object):

    def __init__(self, bot, channel_id):
        self._bot = bot
        self._channel_id = channel_id
        self._channel: Optional[discord.channel.TextChannel] = None
        self._gym_messages: Dict[str, GymEmbed] = {}
        self._cache: Dict[str, Tuple[Gyms, Raids]] = {}
        self._loop.start()

    async def gym_add(self, gid) -> bool:
        if gid not in self._gym_messages:
            m = await self._channel.send(embed=GymEmbed.build_embed(*self._cache[gid]))
            for e in (284614278709903371, 284614087336525826, 284614748702638081, 750426460891775098,
                      750426519582801959, 750426533554028594, 750426545067262022):
                await m.add_reaction(self._bot.get_emoji(e))
            self._gym_messages[gid] = GymEmbed(self._bot, m, *self._cache[gid])
            return True
        return False

    async def gym_remove(self, gid, override=False) -> bool:
        if gid in self._gym_messages:
            if override or all(r.count == 1 for r in self._gym_messages[gid].message.reactions):
                await self._gym_messages[gid].delete()
                del self._gym_messages[gid]
                return True
        return False

    @tasks.loop(seconds=30)
    async def _loop(self) -> None:
        with session_scope(expire_on_commit=False) as s:
            raids = {r.gym_id: r for r in s.query(Raids).options(joinedload(Raids.dex), joinedload(Raids.gym),
                                                                 joinedload(Raids.move_1), joinedload(Raids.move_2)).
                     filter(Raids.end_time > time.time())}
            self._cache = {g.gym_id: (g, raids.get(g.gym_id, None)) for g in s.query(Gyms)}

        current_time = datetime.datetime.utcnow()
        async for message in self._channel.history(oldest_first=True):
            message: discord.Message
            if message.pinned:
                pass
            elif message.author == self._bot.user and message.embeds:
                gid = message.embeds[0].footer.text
                if gid not in self._cache:
                    await message.delete()
                elif gid not in self._gym_messages:
                    self._gym_messages[gid] = GymEmbed(self._bot, message, *self._cache[gid])
                else:
                    await self._gym_messages[gid].update(message, *self._cache[gid])
            else:
                if (current_time - message.created_at).total_seconds() > 300:
                    await message.delete()

    @_loop.before_loop
    async def _loop_init(self) -> None:
        await self._bot.wait_until_ready()
        self._channel = self._bot.get_channel(self._channel_id)
        # await self._channel.edit(name="gyms")

    @_loop.after_loop
    async def _loop_teardown(self) -> None:
        if self._loop.is_being_cancelled():
            log.info(f"Repeated task for {self.__class__.__name__} on channel {self._channel.name} cancelled.")
        # await self._channel.edit(name="gyms-offline")

    def stop(self):
        self._loop.cancel()


class GymCog(commands.Cog):

    CHANNEL_ID = [657052365979123722]

    def __init__(self, bot):
        self._bot = bot
        self._notifiers = {cid: GymNotifier(bot, cid) for cid in self.CHANNEL_ID}

    async def cog_command_error(self, ctx, error):
        # TODO: handle when message is more then 2000 characters
        if isinstance(error, commands.UserInputError):
            await ctx.send(content=f"UserInputError: {error}")
        elif isinstance(error, commands.ConversionError):
            await ctx.send(content=f"ConversionError: {error.original}")
        else:
            raise error

    # @commands.Cog.listener()
    # async def on_raw_reaction_add(self, payload):
    #     if payload.channel_id not in self.CHANNEL_ID:
    #         return
    #     if payload.user_id == self._bot.user.id:
    #         return
    #     print(payload)
    #     # if payload.message_id not in self._config:
    #     #     return
    #     # if payload.user_id == self._bot.user.id:
    #     #     return
    #     # config = self._config[payload.message_id]
    #     # guild: discord.Guild = self._bot.get_guild(config["guild"])
    #     # emoji_key = payload.emoji.id if payload.emoji.id is not None else payload.emoji.name
    #     # role: discord.Role = guild.get_role(config["roles"][emoji_key])
    #     # user: discord.Member = guild.get_member(payload.user_id)
    #     # await user.add_roles(role, reason="User requested role")

    @commands.command()
    async def gym(self, ctx: commands.Context, cmd: str, *, args):
        """
        Add or remove a gym from #gym-watch.

        #!gym ADD ARGS
        #!gym REMOVE ARGS

        ARGS is the string identifier for the gym.  It can be a substring of the gym name or a substring of the
        hexadecimal gym ID.
        """
        if cmd not in ("add", "remove", "force-remove"):
            raise commands.UserInputError("gym command takes add/remove first")
        if ctx.channel.id not in self._notifiers:
            raise commands.UserInputError("run gym command in the associated channel")

        results = Gyms.search(args)
        if len(results) == 1:
            gid = results[0].gym_id
        elif len(results) > 1:
            content = "I found more than one gym that fits that query:\n"
            await ctx.send(content=content + '\n'.join(gym.gym_id + ":  " + str(gym.name) for gym in results))
            return
        else:
            await ctx.send(content="I did not find any gyms that matched that query.")
            return

        if cmd == "add" and not await self._notifiers[ctx.channel.id].gym_add(gid):
            await ctx.send(content="Gym already exists in this channel.")
        elif cmd == "remove" and not await self._notifiers[ctx.channel.id].gym_remove(gid):
            await ctx.send(content="Gym is not present in this channel.")
        elif cmd == "force-remove" and await self._bot.is_owner(ctx.author):
            if not await self._notifiers[ctx.channel.id].gym_remove(gid, override=True):
                await ctx.send(content="Gym is not present in this channel.")

    def cog_unload(self):
        for gym_notify in self._notifiers.values():
            gym_notify.stop()


def setup(bot):
    bot.add_cog(GymCog(bot))


def teardown(bot):
    pass
