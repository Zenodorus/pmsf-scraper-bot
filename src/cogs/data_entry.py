import logging
from typing import Optional

import discord
from discord.ext import commands

# from sql import engine
# from util import total_size

from bot import PMSFBot

log = logging.getLogger(__name__)


class DataEntry(commands.Cog):
    """Admin-only commands that make the bot dynamic."""

    def __init__(self, bot: PMSFBot):
        self._bot: PMSFBot = bot

    @commands.command()
    async def gym_missing_name(self, ctx: commands.Context):
        line = "{gid} <https://www.google.com/maps/dir/?api=1&destination={lat},{lng}>"
        content = [line.format(gid=gym.gym_id, lat=gym.latitude, lng=gym.longitude)
                   for gym in engine.execute(select([Gyms]).where(Gyms.name == None))]
        for i in range(0, len(content), 15):
            await ctx.send(content='\n'.join(content[i:i + 15]))

    @commands.command()
    async def gym_name(self, ctx: commands.Context, *, args):
        """
        Add or update name of a gym.

        #!gym_name [OPTION...] GYM_ID NAME

        GYM_ID is the hexadecimal ID for the gym, and NAME is the name to be added.  NAME must be a quoted string,
        using either ' or " as quotes.  If a name exists already and needs to be corrected, use -f flag.

        Optional Arguments:
            -f
                Force the name change
        """
        opts, args = gnu_parse(args, "f")
        if args is None or (len(args) != 2 and len(args) != 1):
            await ctx.message.delete()
            await ctx.send(content="Error parsing command", delete_after=30)
            return

        force = False
        for o, a, in opts:
            if o == "-f":
                force = True

        with session_scope() as s:
            gym = s.query(Gyms).filter_by(gym_id=args[0]).one_or_none()
            if gym is None:
                await ctx.message.delete()
                content = 'Unrecognized gym ID "{gid}"'
                await ctx.send(content=content.format(gid=args[0]), delete_after=30)
                return

            if gym.name is not None and not force:
                await ctx.message.delete()
                content = 'Gym ID {gid} already has name "{name}".  Use -f to overwrite.'
                await ctx.send(content=content.format(gid=gym.gym_id, name=gym.name), delete_after=30)
                return

            old_name = gym.name
            if len(args) == 1:
                gym.name = None
            else:
                gym.name = args[1]
            content = "Gym ID {gid} has had its name updated to {name} from {old}."
            await ctx.message.delete()
            await ctx.send(content=content.format(gid=gym.gym_id, name=gym.name, old=old_name), delete_after=30)

    @commands.command()
    async def pokestop_missing_name(self, ctx: commands.Context):
        line = "{sid} <https://www.google.com/maps/dir/?api=1&destination={lat},{lng}>"
        content = [line.format(sid=stop.pokestop_id, lat=stop.latitude, lng=stop.longitude)
                   for stop in engine.execute(select([Pokestops]).where(Pokestops.name == None))]
        for i in range(0, len(content), 15):
            await ctx.send(content='\n'.join(content[i:i + 15]))

    @commands.command()
    async def pokestop_name(self, ctx: commands.Context, *, args):
        """
        Add or update name of a pokestop.

        #!pokestop_name [OPTION...] POKESTOP_ID NAME

        POKESTOP_ID is the hexadecimal ID for the pokestop, and NAME is the name to be added.  NAME must be a quoted
        string, using either ' or " as quotes.  If a name exists already and needs to be corrected, use -f flag.

        Optional Arguments:
            -f
                Force the name change
        """
        opts, args = gnu_parse(args, "f")
        if args is None or (len(args) != 2 and len(args) != 1):
            await ctx.message.delete()
            await ctx.send(content="Error parsing command", delete_after=30)
            return

        force = False
        for o, a, in opts:
            if o == "-f":
                force = True

        with session_scope() as s:
            stop = s.query(Pokestops).filter_by(pokestop_id=args[0]).one_or_none()
            if stop is None:
                await ctx.message.delete()
                content = 'Unrecognized pokestop ID "{sid}"'
                await ctx.send(content=content.format(sid=args[0]), delete_after=30)
                return

            if stop.name is not None and not force:
                await ctx.message.delete()
                content = 'Pokestop ID {sid} already has name "{name}".  Use -f to overwrite.'
                await ctx.send(content=content.format(sid=stop.pokestop_id, name=stop.name), delete_after=30)
                return

            old_name = stop.name
            if len(args) == 1:
                stop.name = None
            else:
                stop.name = args[1]
            content = "Pokestop ID {sid} has had its name updated to {name} from {old}."
            await ctx.message.delete()
            await ctx.send(content=content.format(sid=stop.pokestop_id, name=stop.name, old=old_name), delete_after=30)


def setup(bot):
    bot.add_cog(DataEntry(bot))
