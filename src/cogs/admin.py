import logging
from typing import Optional

import discord
from discord.ext import commands

# from sql import engine
# from util import total_size

from bot import PMSFBot
from sql import session_scope
from sql.table import _PokemonFilter

log = logging.getLogger(__name__)


class Admin(commands.Cog):
    """Admin-only commands that make the bot dynamic."""

    def __init__(self, bot: PMSFBot):
        self._bot: PMSFBot = bot
        # self._m: Optional[discord.Message] = None

    async def cog_check(self, ctx: commands.Context) -> bool:
        return await self._bot.is_owner(ctx.author)

    # module loading commands

    @commands.command(hidden=True)
    async def load(self, ctx: commands.Context, *, module: str):
        """Loads a module."""
        if not module.startswith("cogs."):
            module = "cogs." + module

        try:
            self._bot.load_extension(module)
        except commands.ExtensionError as e:
            await ctx.send(f'{e.__class__.__name__}: {e}')
        else:
            await ctx.send('Bzzrt.  Load successful.', delete_after=5)

    @commands.command(hidden=True)
    async def unload(self, ctx: commands.Context, *, module: str):
        """Unloads a module."""
        if not module.startswith("cogs."):
            module = "cogs." + module

        try:
            self._bot.unload_extension(module)
        except commands.ExtensionError as e:
            await ctx.send(f'{e.__class__.__name__}: {e}')
        else:
            await ctx.send('Bzzrt.  Unload successful.', delete_after=5)

    @commands.command(hidden=True)
    async def reload(self, ctx: commands.Context, *, module: str):
        """Reloads a module."""
        if not module.startswith("cogs."):
            module = "cogs." + module

        try:
            self._bot.reload_extension(module)
        except commands.ExtensionError as e:
            await ctx.send(f'{e.__class__.__name__}: {e}')
        else:
            await ctx.send('Bzzrt.  Reload successful.', delete_after=5)

    # channel message commands

    @commands.command(hidden=True)
    async def purge(self, ctx, *, quantity: Optional[int] = None):
        if quantity is None:
            await ctx.channel.purge(limit=1000)
        else:
            await ctx.channel.purge(limit=quantity)

    @commands.command(hidden=True)
    async def purge_unpinned(self, ctx):
        def not_pinned(m: discord.Message):
            return not m.pinned
        await ctx.channel.purge(limit=1000, check=not_pinned)

    @commands.command(hidden=True)
    async def pokedex(self, ctx: commands.Context, *, s: str):
        from sql.table import Pokedex
        if result := Pokedex.search_str(s):
            await ctx.send(result)
        else:
            await ctx.send("no results found")

    @commands.command(hidden=True)
    async def message_edit(self, ctx: commands.Context, m: discord.Message, *, content: str):
        await m.edit(content=content)

    @commands.command(hidden=True)
    async def quit(self, ctx: commands.Context):
        log.info("quit called, shutting down")
        await self._bot.close()

    @commands.command(hidden=True)
    async def sql_reload(self, ctx: commands.Context, name: str):
        import sql.table as table
        from sql.meta import CachedMeta

        t = getattr(table, name, None)
        if t is not None and isinstance(t, CachedMeta):
            t.set_cache_dirty()
            await ctx.send(f"Table {name}'s cache has been deleted and will refresh on next call.")

    @commands.command(hidden=True)
    async def add_filter(self, ctx: commands.Context, always: int, perfect: int, vet: int, *, s: str):
        from sql.table import Pokedex
        dex: Pokedex
        if dex := Pokedex.search_str(s):
            with session_scope() as sql:
                sql.merge(_PokemonFilter(pokemon_id=dex.dex_no,
                                         form=dex.form,
                                         always=always,
                                         perfect=perfect,
                                         verified_end_time=vet))
                if dex.form == "" and Pokedex.contains((dex.dex_no, "Normal")):
                    sql.merge(_PokemonFilter(pokemon_id=dex.dex_no,
                                             form="Normal",
                                             always=always,
                                             perfect=perfect,
                                             verified_end_time=vet))
            await ctx.send(f"Added filter option for {dex} with always={always}, perfect={perfect}, "
                           f"verified_end_time={vet}")
            _PokemonFilter.set_cache_dirty()

    @commands.command(hidden=True)
    async def get_filter(self, ctx: commands.Context, *, s: str):
        from sql.table import Pokedex
        dex: Pokedex
        if dex := Pokedex.search_str(s):
            with session_scope() as sql:
                r = sql.query(_PokemonFilter).filter(_PokemonFilter.pokemon_id == dex.dex_no,
                                                     _PokemonFilter.form == dex.form).all()
                assert len(r) == 1
                r = r[0]

                if r is not None:
                    await ctx.send(f"Filter option for {dex} is always={r.always}, perfect={r.perfect}, "
                                   f"verified_end_time={r.verified_end_time}")

                if dex.form == "":
                    r = sql.query(_PokemonFilter).filter(_PokemonFilter.pokemon_id == dex.dex_no,
                                                         _PokemonFilter.form == "Normal").all()
                    assert len(r) == 1
                    r = r[0]

                    if r is not None:
                        await ctx.send(f"Filter option for Normal {dex} is always={r.always}, perfect={r.perfect}, "
                                       f"verified_end_time={r.verified_end_time}")

    # @commands.command(hidden=True)
    # async def obj_size(self, ctx, *, args):
    #     await ctx.send(f"{args} has size {total_size(eval(args))}")
    #
    # @commands.command()
    # async def test_mention(self, ctx):
    #     await asyncio.sleep(200)
    #     await ctx.send(content=f"{self._bot.get_user(self._bot.owner_id).mention}", delete_after=10)
    #
    # @commands.command()
    # async def test_reactions(self, ctx):
    #     if self._m is None:
    #         self._m = await ctx.send(embed=discord.Embed())
    #         await self._m.add_reaction(self._bot.get_emoji(284614278709903371))
    #     else:
    #         embed = self._m.embeds[0]
    #         print(embed.title)
    #         embed.title = "hello"
    #         x = await self._m.edit(embed=embed)
    #
    # @commands.command(hidden=True)
    # async def ping_test(self, ctx):
    #     for _ in range(3):
    #         await ctx.send(content="<@319557701728469003>".format(i=ctx.bot.owner_id))
    #         await asyncio.sleep(random.randint(60, 120))
    #
    # @commands.command(hidden=True)
    # async def sql(self, ctx, *, query: str):
    #     """Run some SQL."""
    #     if query.startswith('```') and query.endswith('```'):
    #         query = '\n'.join(query.split('\n')[1:-1])
    #     else:
    #         query = query.strip('` \n')
    #
    #     # await ctx.message.delete()
    #     if not query.startswith("select"):
    #         await ctx.send(content="run only select statements to avoid concurrency issues", delete_after=60)
    #         return
    #     results = engine.execute(query).fetchall()
    #     if type(results) is list:
    #         content = '\n'.join('|'.join(str(item) for item in r) for r in results)
    #     else:
    #         content = '|'.join(str(item) for item in results)
    #     await ctx.send(content=content)
    #
    #     # is_multistatement = query.count(';') > 1
    #     # if is_multistatement:
    #     #     # fetch does not support multiple statements
    #     #     strategy = ctx.db.execute
    #     # else:
    #     #     strategy = ctx.db.fetch
    #     #
    #     # try:
    #     #     start = time.perf_counter()
    #     #     results = await strategy(query)
    #     #     dt = (time.perf_counter() - start) * 1000.0
    #     # except Exception:
    #     #     return await ctx.send(f'```py\n{traceback.format_exc()}\n```')
    #     #
    #     # rows = len(results)
    #     # if is_multistatement or rows == 0:
    #     #     return await ctx.send(f'`{dt:.2f}ms: {results}`')
    #     #
    #     # headers = list(results[0].keys())
    #     # table = TabularData()
    #     # table.set_columns(headers)
    #     # table.add_rows(list(r.values()) for r in results)
    #     # render = table.render()
    #     #
    #     # fmt = f'```\n{render}\n```\n*Returned {plural(rows):row} in {dt:.2f}ms*'
    #     # if len(fmt) > 2000:
    #     #     fp = io.BytesIO(fmt.encode('utf-8'))
    #     #     await ctx.send('Too many results...', file=discord.File(fp, 'results.txt'))
    #     # else:
    #     #     await ctx.send(fmt)
    #
    # # @commands.command(hidden=True)
    # # async def sql_table(self, ctx, *, table_name: str):
    # #     """Runs a query describing the table schema."""
    # #     from .utils.formats import TabularData
    # #
    # #     query = """SELECT column_name, data_type, column_default, is_nullable
    # #                FROM INFORMATION_SCHEMA.COLUMNS
    # #                WHERE table_name = $1
    # #             """
    # #
    # #     results = await ctx.db.fetch(query, table_name)
    # #
    # #     headers = list(results[0].keys())
    # #     table = TabularData()
    # #     table.set_columns(headers)
    # #     table.add_rows(list(r.values()) for r in results)
    # #     render = table.render()
    # #
    # #     fmt = f'```\n{render}\n```'
    # #     if len(fmt) > 2000:
    # #         fp = io.BytesIO(fmt.encode('utf-8'))
    # #         await ctx.send('Too many results...', file=discord.File(fp, 'results.txt'))
    # #     else:
    # #         await ctx.send(fmt)


def setup(bot):
    bot.add_cog(Admin(bot))
