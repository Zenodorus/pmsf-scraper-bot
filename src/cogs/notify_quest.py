import datetime
import time
from typing import List, Union, Iterable, Optional

import discord
from discord.ext import commands

from sql import session_scope
from sqlalchemy.orm import joinedload

from sql.table import Items, Pokedex, Quests, QuestTypes
from util.display import or_list, chunk_str
from util.math import midnight
from util.notifier import Notifier, EmbedSet

TYPE_CODES = {0: "Normal", 1: "Fighting", 2: "Flying", 3: "Poison", 4: "Ground", 5: "Rock", 6: "Bug", 7: "Ghost",
              8: "Steel", 9: "Fire", 10: "Water", 11: "Grass", 12: "Electric", 13: "Psychic", 14: "Ice", 15: "Dragon",
              16: "Dark", 17: "Fairy"}
THROW_TYPES = {0: "", 10: "nice", 11: "great", 12: "excellent", 15: "curveball"}

MEGA_ICON = "https://cdn.bulbagarden.net/upload/thumb/3/3f/GO_Venusaur_Mega_Energy.png/40px-GO_Venusaur_Mega_Energy.png"

# TODO remove
ALT_BULBAPEDIA_CDN = True


class QuestEmbedSet(EmbedSet):

    FIELD_LINE = "[{name}](https://www.google.com/maps/dir/?api=1&destination={lat},{lng})"

    def __init__(self, reward_type: Union[Items, Pokedex, None]):
        super().__init__(reward_type)
        self._data: List[Quests]
        self._key: Union[Items, Pokedex, None]

    @property
    def _embed_title(self) -> str:
        if not self._data:
            return f"A quest rewarding {self.reward_name} has not been found."
        else:
            return f"A quest rewarding {self.reward_name} was spotted!"

    @property
    def _embed_description(self) -> str:
        if not self._data:
            return discord.embeds.EmptyEmbed
        else:
            return f"The following quests give out {self.reward_name}:"

    @property
    def _embed_thumbnail(self) -> str:
        if self._key is None:
            if not ALT_BULBAPEDIA_CDN:
                return "https://cdn.bulbagarden.net/upload/7/71/GO_Stardust.png"
            else:
                return "https://archives.bulbagarden.net/media/upload/7/71/GO_Stardust.png"
        elif isinstance(self._key, Pokedex):
            return (self._key.thumbnail if not ALT_BULBAPEDIA_CDN
                    else self._key.thumbnail.replace('cdn.bulbagarden.net', 'archives.bulbagarden.net/media'))
            # if self._data and self._data[0].reward_energy_amount:
            #     return MEGA_ICON
            # else:
            #     return self._key.thumbnail if not ALT_BULBAPEDIA_CDN else self._key.thumbnail.replace('cdn', 'cdn2')
        elif isinstance(self._key, Items):
            return (self._key.icon if not ALT_BULBAPEDIA_CDN
                    else self._key.icon.replace('cdn.bulbagarden.net', 'archives.bulbagarden.net/media'))

    @property
    def reward_name(self):
        if self._key is None:
            return "stardust"
        elif isinstance(self._key, Pokedex):
            if self._data and self._data[0].reward_energy_amount:
                return f"Mega {self._key.name} Energy"
            else:
                return self._key.name
        elif isinstance(self._key, Items):
            return self._key.name

    @staticmethod
    def field_name(quest_type: QuestTypes, num: int, args: dict):
        text = quest_type.quest_text
        fargs = {}
        if "{num}" in text:
            fargs["num"] = num
        if "type_list" in args and "{type_list}" in text:
            type_list = args["type_list"].split(' ')
            fargs["type_list"] = or_list(tuple(map(lambda x: f"{TYPE_CODES[int(x)]}-", type_list)))
        if "poke_list" in args and "{poke_list}" in text:
            poke_list = args["poke_list"].split(' ')
            fargs["poke_list"] = or_list(tuple(Pokedex.search(dex_no=int(dex)).name for dex in poke_list))
        if "throw_type" in args and "{throw_type}" in text:
            fargs["throw_type"] = THROW_TYPES[int(args["throw_type"])]
        if "{win}" in text:
            fargs["win"] = "Win" if "win" in args and args["win"] else "Battle"
        if "{time}" in text:
            fargs["time"] = args["time"]
        text = text.format(**fargs)

        if num > 1:
            text = text.replace("(s)", "s")
            text = text.replace("(ies)", "ies")
        elif num == 1:
            text = text.replace(" 1 time(s)", "")
            text = text.replace("(s)", "")
            text = text.replace("(ies)", "y")
            text = text.replace(" 1 ", " a ")
            for c in "aeiouAEIOU":
                text = text.replace(f" a {c}", f" an {c}")
        return text.replace("  ", " ")

    def _prepare_data(self, quests: Iterable[Quests]) -> Optional[list]:
        if set(q.rowid for q in quests) == set(q.rowid for q in self._data):
            return None

        quests_by_type = {}
        for q in quests:
            k = q.quest_type, q.quantity, q.args, q.reward_stardust or q.reward_item_quantity or q.reward_energy_amount
            if k not in quests_by_type:
                quests_by_type[k] = []
            quests_by_type[k].append(q)
        quests_by_type = list(quests_by_type.items())
        quests_by_type.sort(key=lambda x: tuple(map(int, x[0][0].type_id.split(','))))

        fields = []
        for (qt, num, args, quantity), quests in quests_by_type:
            quests.sort(key=lambda q: q.pokestop_id)
            args = dict(map(lambda kv: kv.split('=', 1), args.split(','))) if args else {}
            name = self.field_name(qt, num, args) + (f"  ({quantity})" if quantity is not None else "")
            value = '\n'.join(QuestEmbedSet.FIELD_LINE.format(lat=q.pokestop.latitude, lng=q.pokestop.longitude,
                                                              name=q.pokestop.name) for q in quests)
            for v in chunk_str(value, chunk_size=1024):
                fields.append((name, v))

        return fields


class QuestNotifier(Notifier):

    _loop_duration = 3600
    _EmbedSetClass = QuestEmbedSet
    _name = "quests"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._reset_time: Optional[int] = None

    @property
    def expired(self):
        return False

    @property
    def reset_time(self):
        t = midnight(time.time())
        if self._reset_time is None:
            return t
        elif self._reset_time < t:
            self._reset_time = None
            return t
        else:
            return self._reset_time

    @reset_time.setter
    def reset_time(self, value: int):
        self._reset_time = midnight(time.time()) + value * 3600
        self._loop.restart()

    def _fetch_data(self):
        with session_scope(expire_on_commit=False) as sql:
            rewards = sql.query(Quests).options(joinedload(Quests.reward_item), joinedload(Quests.reward_dex)). \
                filter(Quests.timestamp > midnight(time.time()) - 24*3600). \
                group_by(Quests.reward_item_id, Quests.reward_pokemon_id, Quests.reward_pokemon_form).all()
            quests = sql.query(Quests).options(joinedload(Quests.pokestop), joinedload(Quests.quest_type),
                                               joinedload(Quests.reward_item), joinedload(Quests.reward_dex)). \
                filter(Quests.timestamp > self.reset_time).all()

        items = filter(None, map(lambda q: q.reward_item, rewards))

        return ([(None, tuple(filter(lambda q: q.reward_stardust is not None, quests)))] +
                [(item, tuple(filter(lambda q: q.reward_item is item, quests))) for item in items])


class QuestPokemonNotifier(QuestNotifier):

    POKEDEX_LIMITS = {"kanto": (1, 151), "johto": (152, 251), "hoenn": (252, 386), "sinnoh": (387, 493),
                      "unova": (494, 649), "kalos": (650, 721), "alola": (722, 809), "galar": (810, 898)}

    def __init__(self, bot, cid, pokedex, *args, **kwargs):
        self._dex = pokedex
        super().__init__(bot, cid, *args, **kwargs)

    def _fetch_data(self):
        dex_min, dex_max = QuestPokemonNotifier.POKEDEX_LIMITS[self._dex]
        with session_scope(expire_on_commit=False) as sql:
            pokemon = sql.query(Quests).options(joinedload(Quests.reward_dex)). \
                filter(Quests.timestamp > midnight(time.time()) - 24*3600,
                       dex_min <= Quests.reward_pokemon_id,
                       Quests.reward_pokemon_id <= dex_max,
                       Quests.reward_energy_amount == None). \
                group_by(Quests.reward_pokemon_id, Quests.reward_pokemon_form).all()
            quests = sql.query(Quests).options(joinedload(Quests.pokestop), joinedload(Quests.quest_type),
                                               joinedload(Quests.reward_dex)). \
                filter(Quests.timestamp > self.reset_time).all()

        pokemon = map(lambda q: q.reward_dex, pokemon)

        return [(dex,  tuple(filter(lambda q: q.reward_dex is dex, quests))) for dex in pokemon]


class QuestMegaCandyNotifier(QuestNotifier):

    def _fetch_data(self):
        with session_scope(expire_on_commit=False) as sql:
            pokemon = sql.query(Quests).options(joinedload(Quests.reward_dex)). \
                filter(Quests.timestamp > midnight(time.time()) - 24*3600, Quests.reward_energy_amount != None). \
                group_by(Quests.reward_pokemon_id, Quests.reward_pokemon_form).all()
            quests = sql.query(Quests).options(joinedload(Quests.pokestop), joinedload(Quests.quest_type),
                                               joinedload(Quests.reward_dex)). \
                filter(Quests.timestamp > self.reset_time, Quests.reward_energy_amount != None).all()

        pokemon = map(lambda q: q.reward_dex, pokemon)

        return [(dex,  tuple(filter(lambda q: q.reward_dex is dex, quests))) for dex in pokemon]


class QuestItemWhitelistNotifier(QuestNotifier):

    def __init__(self, bot, cid, item_ids, *args, **kwargs):
        self._item_ids = item_ids
        super().__init__(bot, cid, *args, **kwargs)

    def _fetch_data(self):
        with session_scope(expire_on_commit=False) as sql:
            items = sql.query(Quests).options(joinedload(Quests.reward_item)). \
                filter(Quests.timestamp > midnight(time.time()) - 24*3600, Quests.reward_pokemon_id == None). \
                group_by(Quests.reward_item_id).all()
            quests = sql.query(Quests).options(joinedload(Quests.pokestop), joinedload(Quests.quest_type),
                                               joinedload(Quests.reward_item)). \
                filter(Quests.timestamp > self.reset_time, Quests.reward_pokemon_id == None).all()

        items = map(lambda item: item.reward_item if item is not None else None, items)
        items = filter(lambda item: getattr(item, "item_id", None) in self._item_ids, items)

        return [(item, tuple(filter(lambda q: q.reward_item is item, quests))) for item in items]


class QuestItemBlacklistNotifier(QuestNotifier):

    def __init__(self, bot, cid, item_ids, *args, **kwargs):
        self._item_ids = item_ids
        super().__init__(bot, cid, *args, **kwargs)

    def _fetch_data(self):
        with session_scope(expire_on_commit=False) as sql:
            items = sql.query(Quests).options(joinedload(Quests.reward_item)). \
                filter(Quests.timestamp > midnight(time.time()) - 24*3600, Quests.reward_pokemon_id == None). \
                group_by(Quests.reward_item_id).all()
            quests = sql.query(Quests).options(joinedload(Quests.pokestop), joinedload(Quests.quest_type),
                                               joinedload(Quests.reward_item)). \
                filter(Quests.timestamp > self.reset_time, Quests.reward_pokemon_id == None).all()

        items = map(lambda item: item.reward_item if item is not None else None, items)
        items = filter(lambda item: getattr(item, "item_id", None) not in self._item_ids, items)

        return [(item, tuple(filter(lambda q: q.reward_item is item, quests))) for item in items]


class QuestCog(commands.Cog):

    CHANNEL_ID = [(645062580993392640, QuestItemWhitelistNotifier, (1, 2, 3)),
                  (811352981622095912, QuestItemWhitelistNotifier, (701, 703, 705, 706, 708, 709)),
                  (811358873814433814, QuestItemBlacklistNotifier, (1, 2, 3, 701, 703, 705, 706, 708, 709)),
                  (811353131299504128, QuestMegaCandyNotifier),
                  (811353168851370024, QuestPokemonNotifier, "kanto"),
                  (811353187054256178, QuestPokemonNotifier, "johto"),
                  (811353205350072360, QuestPokemonNotifier, "hoenn"),
                  (811353222811222078, QuestPokemonNotifier, "sinnoh"),
                  (811353241291587634, QuestPokemonNotifier, "unova"),
                  (811353254616236051, QuestPokemonNotifier, "kalos"),
                  (815331696588226560, QuestPokemonNotifier, "alola"),
                  (815331722832248852, QuestPokemonNotifier, "galar")]

    def __init__(self, bot):
        self._bot = bot
        self._notifiers = [cls(bot, cid, *args) for (cid, cls, *args) in self.CHANNEL_ID]

    async def cog_check(self, ctx):
        return await self._bot.is_owner(ctx.author)

    async def cog_command_error(self, ctx, error):
        # TODO: handle when message is more then 2000 characters
        if isinstance(error, commands.UserInputError):
            await ctx.send(content=f"UserInputError: {error}")
        elif isinstance(error, commands.ConversionError):
            await ctx.send(content=f"ConversionError: {error.original}")
        else:
            raise error

    def cog_unload(self):
        for quest_notify in self._notifiers:
            quest_notify.stop()

    @commands.command()
    async def quest_reset(self, ctx: commands.Context, hours_after_midnight: int):
        if not 0 <= hours_after_midnight < 24:
            raise commands.UserInputError("argument must be an integer in [0,24)")

        await ctx.send(content=f"Quest lookup set to {hours_after_midnight} hours after midnight.  Lookup "
                               f"will revert back to midnight after today.  Restarting loop...")
        for quest_notify in self._notifiers:
            quest_notify.reset_time = hours_after_midnight


def setup(bot):
    bot.add_cog(QuestCog(bot))


def teardown(bot):
    pass
