import asyncio
import logging
import os.path
import signal

import aiohttp
import discord
from discord.ext.commands import Bot, ExtensionAlreadyLoaded

log = logging.getLogger(__name__)


initial_extensions = ["cogs.admin",
                      "cogs.gamemaster",
                      "cogs.notify_gym",
                      "cogs.notify_incident",
                      "cogs.notify_pokemon",
                      "cogs.notify_quest",
                      "cogs.notify_raid",
                      # "cogs.analysis",
                      # "cogs.nests",
                      "cogs.pvp",
                      "cogs.roles",
                      "cogs.scraper",
                      ]


class PMSFBot(Bot):

    BASE_HEADERS = {"accept": "text/html,application/xhtml+xml,application/xml;"
                              "q=0.9,image/webp,image/apng,*/*;"
                              "q=0.8,application/signed-exchange;"
                              "v=b3",
                    "accept-encoding": "gzip, deflate, br",
                    "accept-language": "en-US,en;q=0.9",
                    "dnt": "1",
                    "sec-gpc": "1",
                    "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) "
                                  "Chrome/79.0.3945.130 Safari/537.36"}

    def __init__(self):
        loop = asyncio.get_running_loop()

        cookie_jar = aiohttp.CookieJar(loop=loop)
        if os.path.exists("resources/cookies"):
            cookie_jar.load("resources/cookies")
        self.session: aiohttp.ClientSession = aiohttp.ClientSession(headers=PMSFBot.BASE_HEADERS, cookie_jar=cookie_jar)

        intents = discord.Intents.default()
        intents.members = True

        loop.add_signal_handler(signal.SIGINT, lambda: asyncio.create_task(self.close()))
        loop.add_signal_handler(signal.SIGTERM, lambda: asyncio.create_task(self.close()))
        super().__init__(command_prefix="#!", intents=intents)

    async def close(self):
        await super().close()

        self.session.cookie_jar.save("resources/cookies")
        await self.session.close()

    async def on_ready(self):
        for extension in initial_extensions:
            try:
                self.load_extension(extension)
            except ExtensionAlreadyLoaded:
                pass
            # except Exception as e:
            #     tb = e.__traceback__
            #     stack_str = ''.join(traceback.StackSummary.from_list(traceback.extract_tb(tb)).format())
            #     logger.error("Assertion failed\n%s%s" % (stack_str, str(e)))
        log.info("Finished loading extensions")
