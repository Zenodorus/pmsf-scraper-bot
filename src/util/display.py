from typing import List, Any, Optional, Collection

import discord


def or_list(iterable):
    if len(iterable) == 1:
        return iterable[0]
    elif len(iterable) == 2:
        return iterable[0] + " or " + iterable[1]
    else:
        return ", ".join(iterable[:-1]) + ", or " + iterable[-1]


def chunk_str(s: str, delimiter: str = '\n', chunk_size: int = 2000):
    chunks = []
    while len(s) >= chunk_size:
        i = s.rfind(delimiter, 0, chunk_size)
        if i == -1:
            raise ValueError("delimited message has an item greater than 2000 characters")
        chunks.append(s[:i])
        s = s[i+1:]
    if s:
        chunks.append(s)
    return chunks


async def send_big_delimited_message(channel: discord.TextChannel, content: str, delimiter: str = '\n', **kw) -> None:
    while len(content) >= 2000:
        i = content.rfind(delimiter, 0, 2000)
        if i == -1:
            raise ValueError("delimited message has an item greater than 2000 characters")
        await channel.send(content=content[:i], **kw)
        content = content[i:]
    if content:
        await channel.send(content=content, **kw)


def monospace_tabular(data: List[Collection[Any]], title_row: Optional[Collection[Any]] = None, message: str = ""):
    if not isinstance(data, list) or not data:
        raise ValueError

    if title_row is None:
        title_row = []
    table_width = len(title_row)
    for r in data:
        # if not isinstance(r, list):
        #     raise ValueError
        table_width = max(table_width, len(r))

    cell_widths = [0] * table_width
    for r in [title_row] + data:
        for i, c in enumerate(r):
            cell_widths[i] = max(cell_widths[i], len(str(c)))

    if sum(cell_widths) + (table_width - 1) >= 72:
        raise ValueError("table won't look good on mobile")
    if sum(cell_widths) + 2 * (table_width - 1) >= 72:
        div = " "
    else:
        div = "  "

    title_str = div.join(str(c).ljust(cell_widths[i], ' ') for i, c in enumerate(title_row))
    data_str = '\n'.join(div.join(str(c).ljust(cell_widths[i], ' ') for i, c in enumerate(r)) for r in data)
    table_chunks = chunk_str(data_str, chunk_size=1992 - len(message) - len(title_str))

    return (f"{message}```{title_str}\n{chunk}```" for chunk in table_chunks)
