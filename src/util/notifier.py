import asyncio
import datetime
import logging
from typing import List, Optional, Dict, Any, Iterable, Tuple

import discord
from discord import HTTPException
from discord.ext import tasks

log = logging.getLogger(__name__)


class EmbedSet(object):

    def __init__(self, key):
        self._data: list = []
        self._key = key
        self._embeds: List[discord.Embed] = [self._build_embed()]

    @property
    def expired(self):
        raise NotImplementedError

    @property
    def _embed_title(self) -> str:
        raise NotImplementedError

    @property
    def _embed_description(self) -> str:
        raise NotImplementedError

    @property
    def _embed_thumbnail(self) -> str:
        raise NotImplementedError

    @property
    def _embed_colour(self):
        return discord.embeds.EmptyEmbed

    def _build_embed(self) -> discord.Embed:
        e = discord.Embed(title=self._embed_title, description=self._embed_description,
                          timestamp=datetime.datetime.utcnow(), colour=self._embed_colour)
        e.set_thumbnail(url=self._embed_thumbnail)
        return e

    def _prepare_data(self, data: Iterable) -> Optional[list]:
        # check if data is new, create key-value pairs for each field if new
        # return none if data is not updated, or list of key-value pairs (possibly empty) if different
        raise NotImplementedError

    def update(self, data: Iterable) -> bool:
        fields = self._prepare_data(data)
        if fields is None:
            return False
        self._data = list(data)

        self._embeds = [self._build_embed()]
        for name, value in fields:
            name = name.rstrip().lstrip()
            value = value.rstrip().lstrip()
            if len(self._embeds[-1]) + len(name) + len(value) > 6000 or len(self._embeds[-1].fields) >= 25:
                self._embeds.append(self._build_embed())
            self._embeds[-1].add_field(name=name, value=value, inline=False)
        return True

    def __iter__(self):
        return iter(self._embeds)


class Notifier(object):

    _loop_duration = 15
    _per_message_delay = 1
    _EmbedSetClass = EmbedSet
    _name = ""

    def __init__(self, bot, channel_id):
        self._bot = bot
        self._channel_id = channel_id
        self._channel: Optional[discord.channel.TextChannel] = None
        self._messages: List[discord.Message] = []
        self._embeds: Dict[Any, EmbedSet] = {}
        self._loop = tasks.loop(seconds=self._loop_duration)(self._loop)
        self._loop.before_loop(self._loop_init)
        self._loop.after_loop(self._loop_teardown)
        self._loop.start()

    def _fetch_data(self) -> List[Tuple[Any, Any]]:
        raise NotImplementedError

    def compare_notify_embeds(self, index: int, other: discord.Embed, key: Any) -> bool:
        embed = self._messages[index].embeds[0]
        if isinstance(key, tuple):
            key = tuple(map(str, (k for k in key)))
        if len(embed.fields) != len(other.fields):
            log.debug(f"{self.__class__.__name__} updating message {key} due to embed field "
                      f"length mismatch.")
            return False
        # Embed.thumbnail returns an EmbedProxy object so may not equal
        if len(embed.fields) == 0:
            if not all(getattr(embed, k) == getattr(other, k) for k in ("title", "color")):
                log.debug(f"{self.__class__.__name__} updating message {key} due to embed "
                          f"title or color mismatch.")
                return False
            return True
        else:
            if not all(getattr(embed, k) == getattr(other, k) for k in ("_fields", "title", "description", "color")):
                log.debug(f"{self.__class__.__name__} updating message {key} due to embed "
                          f"title, color, description, or fields mismatch.")
                return False
            return True

    async def _loop(self) -> None:
        ind = 0
        for k, k_data in self._fetch_data():
            if k not in self._embeds:
                self._embeds[k] = self._EmbedSetClass(k)
            self._embeds[k].update(k_data)
            # if self._embeds[k].expired:
            #     del self._embeds[k]
            # else:
            for e in self._embeds[k]:
                try:
                    if ind >= len(self._messages):
                        self._messages.append(await self._channel.send(embed=e))
                        await asyncio.sleep(self._per_message_delay)
                    elif not self._messages[ind].embeds or not self.compare_notify_embeds(ind, e, k):
                        await self._messages[ind].edit(content=None, embed=e)
                        await asyncio.sleep(self._per_message_delay)
                except HTTPException as ex:
                    log.error(f"HTTPException in embed titled: {e.title}")
                    log.error(ex)
                ind += 1

        for i in range(ind, len(self._messages)):
            if self._messages[i].content != "Reserved" or self._messages[i].embeds:
                await self._messages[i].edit(content="Reserved", embed=None)
                await asyncio.sleep(self._per_message_delay)

    async def _loop_init(self) -> None:
        await self._bot.wait_until_ready()
        self._channel = self._bot.get_channel(self._channel_id)
        # await self._channel.edit(name=self._name)
        self._messages.clear()
        async for message in self._channel.history(oldest_first=True):
            try:
                assert message.author == self._bot.user
                assert len(message.embeds) <= 1 and message.content in [None, ""]
            except AssertionError:
                await message.delete()
            else:
                self._messages.append(message)

    async def _loop_teardown(self) -> None:
        if self._loop.is_being_cancelled():
            log.info(f"Repeated task for {self.__class__.__name__} on channel {self._channel.name} cancelled.")
        # await self._channel.edit(name=f"{self._name}-offline")

    def stop(self):
        self._loop.cancel()
