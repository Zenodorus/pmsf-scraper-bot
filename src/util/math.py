def choose(n, k):
    if 0 <= k <= n:
        num = 1
        denom = 1
        for i in range(1, min(k, n - k) +1):
            num *= n - i + 1
            denom *= i
        return num // denom
    else:
        return 0


def binomial_probability(n, k, p):
    if p <= 0:
        return 0
    elif p >= 1:
        return 1
    else:
        return choose(n, k) * p**k * (1-p)**(n-k)


def cumulative_binomial_probability(n, k, p):
    if p <= 0:
        return 0
    elif p >= 1:
        return 1
    else:
        return 1 - sum(binomial_probability(n, i, p) for i in range(k))


def midnight(timestamp: float):
    return (int(timestamp - 3600*4) // 86400) * 86400 + 3600 * 4
