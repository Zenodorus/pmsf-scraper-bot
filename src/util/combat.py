import math
from typing import Union

from sql.table import Pokedex


CPM = [0,
       0.093999996781349, 0.166397869586945, 0.215732470154762, 0.255720049142838, 0.290249884128571,  # 1-5
       0.321087598800659, 0.349212676286697, 0.375235587358475, 0.399567276239395, 0.422500014305115,  # 6-10
       0.443107545375824, 0.462798386812210, 0.481684952974319, 0.499858438968658, 0.517393946647644,  # 11-15
       0.534354329109192, 0.550792694091797, 0.566754519939423, 0.582278907299042, 0.597400009632111,  # 16-20
       0.612157285213470, 0.626567125320435, 0.640652954578400, 0.654435634613037, 0.667934000492095,  # 21-25
       0.681164920330048, 0.694143652915955, 0.706884205341339, 0.719399094581604, 0.731700003147125,  # 26-30
       0.737769484519958, 0.743789434432983, 0.749761044979095, 0.755685508251190, 0.761563837528229,  # 31-35
       0.767397165298462, 0.773186504840851, 0.778932750225067, 0.784636974334717, 0.790300011634826,  # 36-40
       0.795300006866455, 0.800300002098083, 0.805299997329711, 0.810299992561340, 0.815299987792968,  # 41-45
       0.820299983024597, 0.825299978256225, 0.830299973487854, 0.835300028324127, 0.840300023555755,  # 46-50
       0.845300018787384, 0.850300014019012, 0.855300009250640, 0.860300004482269, 0.865299999713897]  # 51-55
CPM_HALF = [math.sqrt((CPM[level] ** 2 + CPM[level + 1] ** 2) / 2) for level in range(55)]


def combat(dex: Pokedex, level: Union[float, int], atk_iv: int, def_iv: int, sta_iv: int) -> int:
    if not (1 <= level <= 51):
        raise ValueError("bad level")

    cpm = CPM[int(level)] if int(level) == level else CPM_HALF[int(level)]
    return int(
        (dex.base_atk + atk_iv) * math.sqrt(dex.base_def + def_iv) * math.sqrt(dex.base_sta + sta_iv) * (cpm**2) / 10)


def ads(dex: Pokedex, level: Union[float, int], atk_iv: int, def_iv: int, sta_iv: int) -> (float, float, int):
    if not (1 <= level <= 51):
        raise ValueError("bad level")

    cpm = CPM[int(level)] if int(level) == level else CPM_HALF[int(level)]
    return (dex.base_atk + atk_iv) * cpm, (dex.base_def + def_iv) * cpm, int((dex.base_sta + sta_iv) * cpm)


def stat_product(dex: Pokedex, level: Union[float, int], atk_iv: int, def_iv: int, sta_iv: int) -> float:
    if not (1 <= level <= 51):
        raise ValueError("bad level")

    cpm = CPM[int(level)] if int(level) == level else CPM_HALF[int(level)]
    return (dex.base_atk + atk_iv) * (dex.base_def + def_iv) * (cpm**2) * int((dex.base_sta + sta_iv) * cpm)


def atk_product(dex: Pokedex, level: Union[float, int], atk_iv: int, def_iv: int, sta_iv: int) -> (float, float):
    if not (1 <= level <= 51):
        raise ValueError("bad level")

    cpm = CPM[int(level)] if int(level) == level else CPM_HALF[int(level)]
    atk, dfn = (dex.base_atk + atk_iv) * cpm, (dex.base_def + def_iv) * cpm
    return atk, atk * dfn * int((dex.base_sta + sta_iv) * cpm)
