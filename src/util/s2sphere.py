from typing import Iterable

import s2sphere
from s2sphere import LatLng, CellId


CAMPUS_S2_CELL_IDS = [9805148532741505024,
                      9805148567101243392,
                      9805148601460981760,
                      9805148635820720128,
                      9805148670180458496,
                      9805148704540196864,
                      9805177738519117824,
                      9805177841598332928,
                      9805177875958071296]
CAMPUS_S2_CELLS = [s2sphere.CellId(i) for i in CAMPUS_S2_CELL_IDS]
SPIRAL = [0, 1, 2, 2, 3, 3, 0, 0, 0, 1, 1, 1, 2, 2, 2, 2]


# print(list(s2sphere.CellId(i).parent(10).to_token() for i in []))

# to get s2cell token:  s2sphere.CellId.from_lat_lng(s2sphere.LatLng.from_degrees(lat, lng)).parent(level).to_token()


def get_s2_token(lat, lng, level):
    return get_s2_cellid(lat, lng, level).to_token()


def get_s2_cellid(lat, lng, level):
    return s2sphere.CellId.from_lat_lng(s2sphere.LatLng.from_degrees(lat, lng)).parent(level)


def same_s2_cell(lat1, lng1, lat2, lng2, level):
    return get_s2_cellid(lat1, lng1, level) == get_s2_cellid(lat2, lng2, level)


def surface_distance(lat1: float, lng1: float, lat2: float, lng2: float):
    return round(LatLng.from_degrees(lat1, lng1).get_distance(LatLng.from_degrees(lat2, lng2)).radians * 6371000, 1)


def is_on_campus(lat, lng):
    p = s2sphere.LatLng.from_degrees(lat, lng).to_point()
    for cell_id in CAMPUS_S2_CELLS:
        if s2sphere.Cell(cell_id).contains(p):
            return True
    return False


def spiral_direction_generator(start_direction: int, n: int):
    d = start_direction
    j = 0
    i = 0
    parity = False
    while n:
        yield d
        n -= 1
        if i == j:
            d = (d + 1) % 4
            i = 0
            if parity:
                j += 1
            parity = not parity
        else:
            i += 1


def s2_cell_neighbor(cid: s2sphere.CellId, index: Iterable[int]):
    for i in index:
        cid = cid.get_edge_neighbors()[i]
    return cid


def nearby_sighting_s2_cells(lat: float, lng: float):
    center = s2sphere.CellId.from_lat_lng(s2sphere.LatLng.from_degrees(lat, lng)).parent(20)
    relative_coords = [(0, 0, 0, 0), (0, 0, 0, 1, 1, 1), (1, 1, 1, 1), (1, 1, 1, 2, 2, 2),
                       (2, 2, 2, 2), (2, 2, 2, 3, 3, 3), (3, 3, 3, 3), (3, 3, 3, 0, 0, 0)]
    return (s2_cell_neighbor(center, coords) for coords in relative_coords)
