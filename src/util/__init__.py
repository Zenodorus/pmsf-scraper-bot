from typing import Callable, Iterable

MAPS_URL = "[Directions](https://www.google.com/maps/dir/?api=1&destination={lat},{lng})"


def apply(func: Callable, iterable: Iterable, tupled=False) -> None:
    if not tupled:
        for i in iterable:
            func(i)
    else:
        for i in iterable:
            func(*i)


def split_once_at(s: str, sep: str, i: int):
    splits = s.split(sep)
    return sep.join(splits[:i]), sep.join(splits[i:])
